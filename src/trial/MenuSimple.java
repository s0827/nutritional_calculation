package trial;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.*;



public class MenuSimple {									//	メニューごとのカロリー、糖質の計算
	String dictionary_file="dictionary.txt";				//	食品名はdictionary.txtに登録されていなければならない
	String menu_dir="./menu/";
	String foodstuff_file="foodstuff.txt";					//	メニューは食材のリストから入手する。将来はメニュー品目を処理するようにする
	String foodproduct_file="foodproducts.xml";				//	個々に栄養価が登録されている加工食品
	TreeMap<String, String> dictionary;
	TreeMap<String, Ingredient> nutrition;
	LinkedHashMap<String, IngredientFoodProduct> foodproducts;
	LinkedHashMap<String, IngredientD> result;				//	MenuDは栄養計算のサマリーを保存
	String symbols="#$%&@";									//	これらのシンボルをメニューの指定に使う
	boolean select_all=false;								//	すべてのメニューを指定する時真
	String select="#";										//	最初は#で指定されメニューを出力
	NutFrame frame;											//	可視化エンジン
	
	public static void main(String args[]){
		MenuSimple ms=new MenuSimple();
	}
	MenuSimple(){
		Pattern pat=Pattern.compile("(["+symbols+"])");		//	メニューの指定に使う記号の識別用
		Calendar calendar=Calendar.getInstance();			//	Java6の実装
		int year=calendar.get(Calendar.YEAR)-2000;
		int month=calendar.get(Calendar.MONTH)+1;
		int day=calendar.get(Calendar.DAY_OF_MONTH);
		
		String dstr=new DecimalFormat("00").format(year)+new DecimalFormat("00").format(month)+new DecimalFormat("00").format(day);
		String src=foodstuff_file;
		String dst=menu_dir+dstr+".txt";
		try {
			Process ps=Runtime.getRuntime().exec("cp "+src+" "+dst);		//	実行時のfoodstuffファイルをセーブする
			ps.waitFor();
		}
		catch(Exception e){
			System.err.println(e);
		}	
		
		System.out.println("メニューのカロリーと糖質を計算しています "+year+"年"+month+"月"+day+"日");
		IngredientReader reader=new IngredientReader();					//	食品成分表の読み込み
		nutrition=reader.getIngredientIdMap();							//	食品成分表のIDをキーとするマップを使う
		read_dictionary();												//	食品名からIDへの参照のマップを得る
		FoodProductXMLreader xmlreader=new FoodProductXMLreader(dictionary,nutrition);	//	加工食品について、すべての項目が提供されていない時に基準となる食品の成分でおぎなう
		foodproducts=xmlreader.read(foodproduct_file);
		
		frame=new NutFrame();									//	可視化を起動する
		
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			String filename=foodstuff_file;
			while(true){												//	最初にデフォルトのファイルを処理、その後ファイル名を与えて処理。できるだけ入力が少なくて済むようにする
				analyze_menu(filename);									//	処理本体
				frame.repaint();
				System.out.print("別のファイルを指定/*#BA該当する,両方,すべての項目を選択/Qで終了 : ");
				String line=in.readLine();
				if (line.length()==0) {
					continue;
				}
				else if (line.length()==1) {
					Matcher mat=pat.matcher(line);
					if (mat.find()){
						select_all=false;
						select=mat.group(1);
					}
					else {
						if (line.equals("0") || line.toUpperCase().equals("O")){		//	同じファイルの再読み込み
							filename=foodstuff_file;
						}
						else if (line.toUpperCase().equals("A")){
							if (select_all) {
								select_all=false;
							}
							else {
								select_all=true;
							}
						}
						else if (line.toUpperCase().equals("B")){
							select_all=false;
							select=symbols;
						}
						else if (line.toUpperCase().equals("Q")){
							System.exit(0);
						}						
					}
				}
				else {
					filename=line+".txt";								//	.txtを追加する
				}
			}					
		}
		catch(Exception e){
			System.err.println(e);
		}
		System.out.println("プログラムを終了します");
	}
	void read_dictionary(){
		 dictionary=new TreeMap<String, String>();
		 try {
			 BufferedReader in=new BufferedReader(new FileReader(dictionary_file));
			 while(true){
				 String line=in.readLine();
				 if (line==null) break;
				 if (line.length()==0) continue;
				 if (line.substring(0,1).equals(":")) continue;			//	コロンでコメント行
				 String[] strs=line.split("[ \t]+");					//	スペースまたはタブ区切り
				 dictionary.put(strs[0],strs[1]);
			 }
			 in.close();
		 }
		 catch(Exception e){
			 System.err.println(e);
		 }
	}
	void analyze_menu(String filename){
		Pattern pat=Pattern.compile("["+select+"]");
		System.out.println("select= "+select);
		try {
			BufferedReader in=new BufferedReader(new FileReader(filename));
			IngredientD grandsummary=new IngredientD();								//	全メニューの合計
			while(true){
				String line=in.readLine();
				if (line==null) break;
				if (line.substring(0,1).equals(":")) continue;			//	コロンでコメント行
				String[] strs=MenuString.split(line);					//	コマンド行を語に分割
				if (strs[1].equals("{")){
					String title=strs[0];
					
					boolean selected=false;
					if (select_all){
						selected=true;
					}
					else {
						Matcher mat=pat.matcher(title);
						if (mat.find()) selected=true;
					}
					Menu menu=new Menu(title,selected,nutrition,dictionary,foodproducts);		//	入力ストリームを渡し、メニューごとに処理。食品成分表のデータ渡す
					TreeMap<String, MenuData> data=menu.analyze(in);					
					
					if (menu.isSelected()) {
						Ingredient summary=get_summary(data);
						System.out.print("このメニューの合計 ");
						summary.output();
						System.out.println();
						grandsummary.add(summary);
					}
				}
			}
			in.close();
			System.out.print("今日のメニューの合計 ");
			grandsummary.output();
			System.out.println();
			
			frame.put_data(grandsummary);
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	Ingredient get_summary(TreeMap<String, MenuData> data){
		Ingredient summary=new Ingredient();
		Iterator<String> it=data.keySet().iterator();
		while(it.hasNext()) summary.add(data.get(it.next()));
		return summary;
	}
}
