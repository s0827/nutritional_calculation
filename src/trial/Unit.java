package trial;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.*;

public class Unit {												//	単位の処理。食材ごとに用いられる単位が使えるようにする
	Pattern upat=Pattern.compile("[0-9\\.]+(.+)");
	Pattern gpat=Pattern.compile("([0-9\\.]+)g");
	
	LinkedHashMap<String, LinkedHashMap<String, Double>> unit_map;
	public Unit(String unit_file){
		unit_map=new LinkedHashMap<String, LinkedHashMap<String, Double>>();
		try {
			BufferedReader in=new BufferedReader(new FileReader(unit_file));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				if (line.substring(0,1).equals(":")) continue;	//	コメント行
				
				String[] strs=line.split("[\t ]+");
				String key=strs[0];
				
				Matcher umat=upat.matcher(strs[1]);
				Matcher gmat=gpat.matcher(strs[2]);
				
				if (umat.find() && gmat.find()){
//					System.out.println(key);
					String unit=umat.group(1);
					double weight=Double.parseDouble(gmat.group(1));
					
					LinkedHashMap<String, Double> U=unit_map.get(key);
					if (U==null){
						U=new LinkedHashMap<String, Double>();
						unit_map.put(key,U);
					}
					U.put(unit,new Double(weight));
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println();
		}
	}
	public double get_weight(String key, String qty){
		double weight=0.0;
		LinkedHashMap<String, Double> U=unit_map.get(key);
		if (U!=null){
			Pattern pat=Pattern.compile("([0-9]+\\.{0,1}[0-9]*)([^0-9\\.]+)");
			Matcher mat=pat.matcher(qty);
			if (mat.find()){
				String unit=mat.group(2);
				double quantity=Double.parseDouble(mat.group(1));
				Double W=U.get(unit);
				if (W!=null) {
					weight=W.doubleValue()*quantity;
				}
				else {
					System.out.println(key+" 指定された単位が登録されていない");
				}
			}
		}
		return weight;
	}
	public static void main(String args[]){					//	開発／デバッグ用テストプログラム
		Unit unit=new Unit("unit.txt");
		unit.disp();
		unit.test();
	}
	void disp(){
		unit_map.forEach((K,V) -> {
			System.out.print(K);
			V.forEach((U,G) -> {
				System.out.print("\t( 1"+U+" = "+new DecimalFormat("0").format(G.doubleValue())+"g )");
			});
			System.out.println();
		});
	}
	void test(){
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			System.out.println("例> \"なす 1本\" の入力で重量に換算する");
			while(true){
				System.out.print("> ");
				String line=in.readLine();
				if (line==null) break;
				String[] strs=line.split("[\t ]+");
				double weight=get_weight(strs[0],strs[1]);
				System.out.println(new DecimalFormat("0.0").format(weight)+"g");
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
}
