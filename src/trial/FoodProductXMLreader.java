package trial;

import java.io.*;
import java.util.*;

import javax.xml.parsers.*;

import org.w3c.dom.*;

import trial.*;

public class FoodProductXMLreader {					//	加工食品のXMLファイルを読む
	TreeMap<String, String> dictionary;				//	すべてのデータがないとき食品成分表のデータで補う。最初にこのデータを設定、XMLの内容で上書きする
	TreeMap<String, Ingredient> nutrition;

	LinkedHashMap<String, IngredientFoodProduct> foodproducts;
/*	public static void main(String args[]){
		FoodProductXMLreader fpx=new FoodProductXMLreader();
	}*/
	public FoodProductXMLreader(TreeMap<String, String> dictionary, TreeMap<String, Ingredient> nutrition){
		this.dictionary=dictionary;
		this.nutrition=nutrition;
	}
	public LinkedHashMap<String, IngredientFoodProduct> read(String xmlfile){
		foodproducts=new LinkedHashMap<String, IngredientFoodProduct>();
		try {
			InputStream is=new FileInputStream(xmlfile);
			DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
			DocumentBuilder builder=factory.newDocumentBuilder();
			Document doc=builder.parse(is);
			Element rootElement=doc.getDocumentElement();
			
			NodeList itemlist=rootElement.getElementsByTagName("item");
			for (int i=0; i<itemlist.getLength(); i++){
				Node itemnode=itemlist.item(i);
				NamedNodeMap itemattr=itemnode.getAttributes();
				String name=itemattr.getNamedItem("keyword").getNodeValue();
				
				IngredientFoodProduct D=new IngredientFoodProduct(name);
				
				Node base_item_node=itemattr.getNamedItem("base_item");
				if (base_item_node!=null){
					String base_item=base_item_node.getNodeValue();
					if (dictionary!=null && nutrition!=null){
						String id=dictionary.get(base_item);
						Ingredient item=nutrition.get(id);
						D.set(item);									//	デフォルト値としてコピーする
					}
				}			
				double calorie=get_attr(itemattr,"calorie");
				double carbohydrate=get_attr(itemattr,"carbohydrate");
				double protein=get_attr(itemattr,"protein");
				double fat=get_attr(itemattr,"fat");
				double salt=get_attr(itemattr,"salt");
				D.overwrite(calorie,carbohydrate,protein,fat,salt);		//	値が0だったら上書きしない
				foodproducts.put(name,D);
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
		return(foodproducts);
	}
	double get_attr(NamedNodeMap attr, String str){			//	レコードがないときは単純に0を返す
		double val=0.0;
		Node node=attr.getNamedItem(str);
		if (node!=null) {
			val=Double.parseDouble(node.getNodeValue());
		}
		return val;
	}
}
