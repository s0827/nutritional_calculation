package trial;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class IngredientReader {									//	食品成分表を解析する
	String filename="ingredient2015.txt";						//	成分表ファイル名
	String line;												//	現在データ行。例外処理に渡すため
	int numlines=1;												//	データライン数
	int numitems=0;												//	食品の項目数
	int maxnum=0;												//	表の上の項目の上限値
	TreeMap<String, Ingredient> IngredientIdMap;				//	食品成分表のIDでレコードを参照するマップ
	TreeMap<String, Ingredient> IngredientIndexMap;				//	食品成分表の索引コードで参照するマップ
	
	LinkedList<Integer> missing_nums=new LinkedList<Integer>();	//	索引番号の欠番
	String group_str="([0-9]{2})";
	String id_str="([0-9]{5})";
	String num_str="([0-9]+)";
	public static void main(String args[]){
		IngredientReader ai=new IngredientReader();
	}
	public IngredientReader(){
		Pattern group_pat=Pattern.compile(group_str);
		Pattern id_pat=Pattern.compile(id_str);
		Pattern num_pat=Pattern.compile(num_str);
		
		IngredientIdMap=new TreeMap<String, Ingredient>();
		IngredientIndexMap=new TreeMap<String, Ingredient>();
		
		try {
			BufferedReader in=new BufferedReader(new FileReader(filename));
			while(true){
				line=in.readLine();
				if (line==null) break;
				numlines++;
				String[] strs=line.split("\t");
//				int numrecords=strs.length;
//				if (numrecords!=68) System.out.println(numlines+"\t"+numrecords+"\t("+line+")");		//	長さが異常なラインの検出（改行の異常）
				if (numlines==5) getnutrition_list(in,line);
				if (strs.length>1){
					Matcher group_mat=group_pat.matcher(strs[0]);
					Matcher id_mat=id_pat.matcher(strs[1]);
					if (group_mat.find() && id_mat.find()){
						Matcher num_mat=num_pat.matcher(strs[2]);
						if (num_mat.find()){
							numitems++;
							maxnum++;
							maxnum=adjust_maxnum(maxnum,num_mat.group(1));								// 索引番号の欠番への対応
						}
						Ingredient ingredient=new Ingredient(strs);
						String id=ingredient.getId();
						String index=ingredient.getIndex();
						IngredientIdMap.put(id,ingredient);
						IngredientIndexMap.put(index,ingredient);
					}					
				}
			}
			in.close();
//			System.out.println("missing nums "+missing_nums);
//			System.out.println("number of lines= "+numlines);
//			System.out.println("number of items= "+numitems);
		}
		catch(Exception e){
			System.out.println("line "+numlines+" ("+line+")");
			System.err.println(e);
		}
	}
	int adjust_maxnum(int maxnum, String str){
		int num=Integer.parseInt(str);
		if (num!=maxnum){
			while(num>maxnum){
				maxnum++;
				missing_nums.add(new Integer(maxnum));
			}
		}
		return maxnum;
	}
	void getnutrition_list(BufferedReader in, String line){
		try {
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter("nutrition_list.txt")));
			String[] strs5=line.split("\t");			//	5行目の内容
			line=in.readLine();
			String[] strs6=line.split("\t");
			String[] nutrition=new String[strs6.length];
			
			for (int i=0; i<strs6.length; i++){
				if (strs6[i].length()>0){
					nutrition[i]=strs6[i];					
				}
				else {
					nutrition[i]=strs5[i];
				}
				out.println(i+"\t"+nutrition[i]);
//				System.out.println(i+"\t"+nutrition[i]);
			}
			out.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	public TreeMap<String, Ingredient> getIngredientIdMap(){ return IngredientIdMap; }
	TreeMap<String, Ingredient> getIngredientIndexMap(){ return IngredientIndexMap; }	
}
