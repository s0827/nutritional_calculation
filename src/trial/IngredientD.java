package trial;

import java.text.*;

public class IngredientD {
	public String name;
	public double calorie;								//	カロリー
	public double carbohydrate;							//	糖質
	public double protein;								//	たんぱく質
	public double fat;									//	脂質
	public double salt;									//	塩分の処理は保留
	public double Na,K,Ca,Mg,P,Fe,Zn,Cu,Mn;				//	ミネラル 食品成分表でmg単位で記述されているもの
	public double VA,VB1,VB2,VB3,VB6,VB12,VC,VD,VE,VK;	//	ビタミン
	public IngredientD(){}
	public void set(IngredientD item){
		set(item, 1.0);
	}
	public void overwrite(IngredientD item){
		overwrite(item, 1.0);
	}
	public void set(IngredientD item,double ratio){
		this.calorie=ratio*item.calorie;
		this.carbohydrate=ratio*item.carbohydrate;
		this.protein=ratio*item.protein;
		this.fat=ratio*item.fat;
		this.salt=ratio*item.salt;
		
		this.Na=ratio*item.Na;
		this.K =ratio*item.K;
		this.Ca=ratio*item.Ca;
		this.Mg=ratio*item.Mg;
		this.P=ratio*item.P;
		this.Fe=ratio*item.Fe;
		this.Zn=ratio*item.Zn;
		this.Cu=ratio*item.Cu;
		this.Mn=ratio*item.Mn;
		
		this.VA =ratio*item.VA;
		this.VB1=ratio*item.VB1;
		this.VB2=ratio*item.VB2;
		this.VB3=ratio*item.VB3;
		this.VC =ratio*item.VC;
		this.VD =ratio*item.VD;
		this.VK =ratio*item.VK;
	}
	public void overwrite(IngredientD item,double ratio){
		if (item.calorie>0.0) 		this.calorie=ratio*item.calorie;
		if (item.carbohydrate>0.0)	this.carbohydrate=ratio*item.carbohydrate;
		if (item.protein>0.0)		this.protein=ratio*item.protein;
		if (item.fat>0.0)			this.fat=ratio*item.fat;
		if (item.salt>0.0)			this.salt=ratio*item.salt;
		
		if (item.Na>0.0)		this.Na=ratio*item.Na;
		if (item.K>0.0)			this.K =ratio*item.K;
		if (item.Ca>0.0)		this.Ca=ratio*item.Ca;
		if (item.Mg>0.0)		this.Mg=ratio*item.Mg;
		if (item.P>0.0)			this.P=ratio*item.P;
		if (item.Fe>0.0)		this.Fe=ratio*item.Fe;
		if (item.Zn>0.0)		this.Zn=ratio*item.Zn;
		if (item.Cu>0.0)		this.Cu=ratio*item.Cu;
		if (item.Mn>0.0)		this.Mn=ratio*item.Mn;
		
		if (item.VA>0.0)		this.VA =ratio*item.VA;
		if (item.VB1>0.0)		this.VB1=ratio*item.VB1;
		if (item.VB2>0.0)		this.VB2=ratio*item.VB2;
		if (item.VB3>0.0)		this.VB3=ratio*item.VB3;
		if (item.VC>0.0)		this.VC =ratio*item.VC;
		if (item.VD>0.0)		this.VD =ratio*item.VD;
		if (item.VK>0.0)		this.VK =ratio*item.VK;
	}
	public void add(IngredientD D){
		calorie+=D.calorie;
		carbohydrate+=D.carbohydrate;
		protein+=D.protein;
		fat+=D.fat;
		salt+=D.salt;
		Na+=D.Na;
		K+=D.K;
		Ca+=D.Ca;
		Mg+=D.Mg;
		P+=D.P;
		Fe+=D.Fe;
		Zn+=D.Zn;
		Cu+=D.Cu;
		Mn+=D.Mn;
		VA+=D.VA;
		VB1+=D.VB1;
		VB2+=D.VB2;
		VB3+=D.VB3;
		VC+=D.VC;
	}
	public void output(){
		System.out.println("エネルギー "+new DecimalFormat("0.0").format(calorie)+"kcal "
				+"糖質 "+new DecimalFormat("0.0").format(carbohydrate)+"g "
				+"たんぱく質 "+new DecimalFormat("0.0").format(protein)+"g "
				+"脂質 "+new DecimalFormat("0.0").format(fat)+"g");
		System.out.println(" 　　ミネラル(mg)"
				+" Na "+new DecimalFormat("0.00").format(Na)
				+" K  "+new DecimalFormat("0.00").format(K)
				+" Ca "+new DecimalFormat("0.00").format(Ca)
				+" P  "+new DecimalFormat("0.00").format(P)
				+" Fe "+new DecimalFormat("0.00").format(Fe));
		System.out.println(" 　　ビタミン(mg)"
				+" A  "+new DecimalFormat("0.00").format(VA)
				+" B1 "+new DecimalFormat("0.00").format(VB1)
				+" B2 "+new DecimalFormat("0.00").format(VB2)
				+" B3 "+new DecimalFormat("0.00").format(VB3)
				+" C  "+new DecimalFormat("0.00").format(VC));
	}
	public String getName(){ return name; }
	public double getCalorie(){ return calorie; }
	public double getCarbohydrate(){ return carbohydrate; }
	public double getProtein(){ return protein; }
	public double getFat(){ return fat; }
	public double getSalt(){ return salt; }
	public double getNa(){ return Na; }				//	ミネラル
	public double getK(){ return K; }
	public double getCa(){ return Ca; }
	public double getMg(){ return Mg; }
	public double getP(){ return P; }
	public double getFe(){ return Fe; }
	public double getZn(){ return Zn; }
	public double getCu(){ return Cu; }
	public double getMn(){ return Mn; }
	public double getVA(){ return VA; }				//	ビタミン
	public double getVB1(){ return VB1; }
	public double getVB2(){ return VB2; }
	public double getVB3(){ return VB3; }
	public double getVC(){ return VC; }
}
