package trial;

import java.io.*;

public class line_ctrl_test {
	enum lct { Cont, Break, Process };
	public static void main(String args[]) {
		line_ctrl_test lct=new line_ctrl_test();
	}
	line_ctrl_test(){
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			boolean stat=true;
			while(stat) {
				System.out.print("> ");
				switch (evaluate(in)) { case Break : stat=false; break; case Cont: break;
				default :
					System.out.println("processing");
				}
			}
			System.out.println("exited");
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	lct evaluate(BufferedReader in) {
		lct val=lct.Process;
		try {
			String line=in.readLine();
			if (line.equals("q")) { val=lct.Break; }
			else if (line.equals(":")) { val=lct.Cont; }
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return val;
	}
}
