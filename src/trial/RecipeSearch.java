package trial;

import java.util.*;
import java.util.regex.*;

import menu.MenuDate;
import menu.MenuReader;

import java.io.*;

import nutrition.*;

public class RecipeSearch extends MenuReader {			//	レシピの内容の検索、解析を提供する
	TreeSet<Integer> fileset;
	RecipeSearch(LinkedHashMap<String, Nutritions> nutrition_table, LinkedHashMap<String, Nutritions> foodproducts, 
			LinkedHashMap<String, String> IDmap, Nutritions standard_nutrition, MenuDate menu_date, Unit2 unit, String select_chars,
			TreeSet<Integer> fileset){
		
		super(nutrition_table,foodproducts,IDmap,standard_nutrition,menu_date,unit,select_chars);			//	主なデータをスーパークラスで保持

		this.fileset=fileset;
	}
	boolean search(BufferedReader in, String[] strs) {
		boolean stat=false;
		if (strs[0].equals("R")) {
			analyze_recipe_file(in);
			stat=true;
		}
		else if (strs[0].equals("r")) {					//	履歴の検索、履歴のデータを再利用するために使う
			analyze_history(in);
			stat=true;
		}
		return stat;
	}
	void analyze_recipe_file(BufferedReader in) {
		boolean exit=false;
		String filename="recipe.txt";
		LinkedList<MenuData> list=read_file(filename);					//	レシピファイルの読み込み
		list.forEach(V -> V.summation());								//	メニュー項目ごとの栄養成分の集計
		if (list.size()==0) return;
		
		int depth=1;
		int width=1;
		boolean quantity_f=true;
		boolean ratio_f=false;											//	一日の必要量に対する比率を出力
		
		while(true) {
			Iterator<MenuData> it=list.iterator();
			while(it.hasNext()) it.next().output(quantity_f, ratio_f, depth, width, standard_nutrition);		//	これではメニューごとに階層的に扱うことはできない
			try {
				System.out.print("R> ");
				String line=in.readLine();
				if (line.length()==0) break;
				if (line.toUpperCase().equals("Q")) {
					break;
				}
				else if (line.equals("+")) {
					depth++;
					if (depth>2) depth=2;
				}
				else if (line.equals("-")) {
					depth--;
					if (depth<1) depth=1;
				}
				else if (line.equals(">")) {
					width++;
					if (width>2) width=2;
				}
				else if (line.equals("<")) {
					width--;
					if (width<0) width=0;
				}
				else if (line.equals("=")) {
					if (quantity_f) {
						quantity_f=false;
					}
					else {
						quantity_f=true;
					}
				}
				else if (line.equals("%")) {
					if (ratio_f) {
						ratio_f=false;
					}
					else {
						ratio_f=true;
					}
				}
			}
			catch(Exception e) {
				System.err.println(e);
			}			
		}
	}
	void analyze_history(BufferedReader in) {
		int istart=0, iend=30;							//	デフォルトで過去30日間のデータを解析する
		String keyword=null;
		String[] findex=new String[fileset.size()];
		Iterator<Integer> it=fileset.iterator();
		Pattern pat=Pattern.compile("([0-9]+)");
		for (int i=0; i<findex.length; i++) findex[i]=new String(it.next().toString());
		if (istart<0) istart=0;
		if (iend>=findex.length) iend=findex.length-1;
	
		while(true) {
			for (int i=istart; i<iend; i++) disp_menu(findex[i],keyword);
			try {
				System.out.print("r> ");
				String line=in.readLine();
				if (line.toUpperCase().equals("Q")) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()) {
					iend=Integer.parseInt(mat.group(1));
					if (iend>=findex.length) iend=findex.length-1;
				}
				else {
					keyword=line;
				}
			}
			catch(Exception e) {
				System.err.println(e);
			}			
		}
	}
	void disp_menu(String findex, String keyword) {					//	%で始まる料理の内容をそのまま表示する
		Pattern pat=null;
		if (keyword!=null) pat=Pattern.compile(keyword);
		String filename=menu_dir+findex+".txt";
		boolean init_f=true;
		try {
			BufferedReader in=new BufferedReader(new FileReader(filename));
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				if (pat!=null) {
					Matcher mat=pat.matcher(line);
					if (!mat.find()) continue;
				}
				if (line.substring(0,1).equals("%")) {
					if (init_f) {
						System.out.println("findex: "+findex);		//	表示内容が存在するときだけファイルのインデックスを表示
						init_f=false;
					}
					System.out.println(line);
					while(true) {
						line=in.readLine();
						System.out.println(line);
						if (line.length()>0) {
							if (line.substring(0,1).equals("}")) break;							
						}
					}
				}
			}
			in.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
}
