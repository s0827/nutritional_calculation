package trial;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.*;

public class Menu {			//	各メニューの内容の処理
	String title;
	boolean selected;
	TreeMap<String, Ingredient> nutrition;
	TreeMap<String, String> dictionary;
	LinkedHashMap<String, IngredientFoodProduct> foodproducts;
	TreeMap<String, MenuData> data;
	Pattern qpat=Pattern.compile("([0-9\\.]+)g");
	Pattern dpat=Pattern.compile("([0-9]+)kcal/([0-9]+)g");
	Pattern npat=Pattern.compile("([0-9]+\\.{0,1}[0-9]*)");
	Menu(String str, boolean selected, TreeMap<String, Ingredient> nutrition, TreeMap<String, String> dictionary, 
			LinkedHashMap<String, IngredientFoodProduct> foodproducts){
		this.nutrition=nutrition;
		this.dictionary=dictionary;								
		this.foodproducts=foodproducts;								//	データベースと辞書をロードする
		this.selected=selected;
		title=str;
		if (selected) System.out.println(str);
	}
	TreeMap<String, MenuData> analyze(BufferedReader in){
		try {
			data=new TreeMap<String, MenuData>();
			while(true){
				String line=in.readLine();
				if (line==null) break;
				String[] strs=MenuString.split(line);				//	行頭などの空レコードをスキップする
				if (strs[0].equals("}")) break;
				if (strs[0].substring(0,1).equals(":")) continue;
				String keyword=strs[0];
				String id=dictionary.get(keyword);
				if (id!=null) {
					Ingredient item=nutrition.get(id);
					if (item!=null){
						Matcher qmat=qpat.matcher(strs[1]);
						if (qmat.find()){
							String comment="";
							for (int i=2; i<strs.length; i++){
								if (i>2) comment+=" ";
								comment+=strs[i];
							}
							double quantity=Double.parseDouble(qmat.group(1));							
							double utilization=item.getUtilization();
							MenuData menudata=new MenuData(keyword,item,quantity,utilization,comment);
							if (selected) output_menu_item(menudata);
//							if (selected) System.out.println("\t"+keyword+"\t"+new DecimalFormat("0.0").format(calory)+"kcal"
//									+"\t"+new DecimalFormat("0.0").format(carbohydrate)+"g"
//									+"\t["+item.getName()+"]"+"\t"+comment);
							data.put(keyword,menudata);
						}
					}
					else {
						System.out.println("　　"+id+" 食品成分データがない (IDに該当する食品がない)");
					}
				}
				else {
					IngredientFoodProduct foodproduct=foodproducts.get(keyword);
					if (foodproduct!=null){
						Matcher qmat=qpat.matcher(strs[1]);
						double quantity=0.0;
						if (qmat.find()) quantity=Double.parseDouble(qmat.group(1));
						MenuData menudata=new MenuData(keyword,foodproduct,quantity,1.0,"");
						if (selected) output_menu_item(menudata);
						data.put(keyword,menudata);					
					}
					else {		//	カロリー、糖質量の直接入力 /区切りで文字列を分解し頭からエネルギー、糖質、たんぱく質、脂質、塩分の順にとる
						if (strs.length>1){
							String[] dstr=strs[1].split("/");
							if (dstr.length==5){											//	直接入力の書式に合わない
								double calorie=get_number(dstr[0]);
								double carbohydrate=0.0;
								double protein=get_number(dstr[2]);
								double fat=get_number(dstr[3]);

								if (dstr[1].length()==0){									//	糖質のデータがない時、エネルギー、たんぱく質、脂質から計算する
									carbohydrate=(calorie-(4.0*protein+9.0*fat))/4.0;		//	【注意】社員食堂用。たんぱく質、脂質のデータがないと異常になる。
								}
								else {
									carbohydrate=get_number(dstr[1]);
								}								
								MenuData menudata=new MenuData(keyword,calorie,carbohydrate,protein,fat);
								if (selected) output_menu_item(menudata);
								data.put(keyword,menudata);									
							}
							else {
								System.out.println("　　"+keyword+" 食品成分データがない (キーワードに該当する項目がない)");		//	ここでエラーをフックする
							}
						}
						else {
							System.out.println("　　"+keyword+" 食品成分データがない (重さのレコードがない)");
						}
					}
				}
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
		return data;
	}
	double get_number(String str){
		double val=0.0;
		Matcher nmat=npat.matcher(str);
		if (nmat.find()){
			val=Double.parseDouble(nmat.group(1));
		}
		return val;
	}
	void output_menu_item(MenuData menudata){							//	フィールド長を揃えて出力したい
		System.out.print("　　");
		System.out.print(fill_full(8,menudata.getKeyword()));			//	全角文字で指定文字数に合わせる
		System.out.print(fill_half(10,new DecimalFormat("0.0").format(menudata.getCalorie())+"kcal"));
		System.out.print(fill_half(7,new DecimalFormat("0.0").format(menudata.getCarbohydrate())+"g"));
		if (menudata.getItem()!=null){
			System.out.print("　　　"+fill_full(30,menudata.getItem().getName()));
			System.out.print("　　"+fill_half(4,new DecimalFormat("0").format(menudata.getQuantity())+"g"));
			System.out.print("("+fill_half(3,new DecimalFormat("0").format(menudata.getUtilization()*100.0))+"%)");
			System.out.print(fill_half(19,
					"["+new DecimalFormat("0.0").format(menudata.getCalorie())+"kcal/"
					+new DecimalFormat("0.0").format(menudata.getCarbohydrate())+"g]")
				);
			System.out.print("　"+menudata.getComment());			
		}
		System.out.println();
	}
	String fill_full(int length, String instr){						//	左詰め
		int strlength=instr.length();
		String outstr="";
		if (strlength>length){
			outstr=instr.substring(0,length);						//	長すぎる時は指定文字数でカット
		}
		else {
			outstr=instr;
			while(outstr.length()<length) outstr+="　";				//	全角スペースを足して指定文字数にする
		}
		return outstr;
	}
	String fill_half(int length, String instr){
		String outstr=instr;
		while (outstr.length()<length) outstr=" "+outstr;
		return outstr;
	}
	boolean isSelected(){ return selected; }
}
