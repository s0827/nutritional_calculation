package trial;

import java.awt.*;

public class NutFrame extends Frame {				//	栄養成分の可視化表示
	int offset=20, margin=0;						//	メニューバーの高さのオフセットが必要？
	int framewidth=500,frameheight=500+offset;
	int panelwidth=500,panelheight=500;
	NutPanel panel;
	
	public NutFrame(){								//	画面の生成
		setSize(framewidth,frameheight);
		setVisible(true);
		setLayout(null);
		panel=new NutPanel(panelwidth,panelheight);
		panel.setLocation(0,offset);
		add(panel);
	}
	public void put_data(IngredientD data){
		panel.put_data(data);
	}
	public void paint(Graphics gc){
		if (panel!=null) panel.repaint();
	}
}
