package trial;

import java.io.*;
import java.util.*;

public class bs {
	public static void main(String args[]) {
		bs bs=new bs();
	}
	bs (){
		LinkedList<Double> list=new LinkedList<Double>();
		try {
			BufferedReader in=new BufferedReader(new FileReader("bs.txt"));
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				String[] strs=line.split("\t");
				if (strs.length>1) list.add(new Double(strs[1]));			
			}
			in.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
		double[] d=new double[list.size()];
		int nn=60;
		int n=0;
		for (Double D: list) d[n++]=D.doubleValue(); 
		for (int i=0; i<d.length; i++) {
			if (i>=nn && i+nn<d.length) {
				double sum=0.0;
				for (int j=i; j<i+nn; j++) {
					sum+=d[j];
				}
				System.out.println(i+"\t"+sum/(double)nn);
			}
		}
	}
}