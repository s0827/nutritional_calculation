package trial;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.*;

public class IngredientSimpleQuery {				//	キーワードに合致する項目を表示
	public static void main(String args[]){
		IngredientSimpleQuery isq=new IngredientSimpleQuery();
	}
	IngredientSimpleQuery(){
		IngredientReader reader=new IngredientReader();	
		TreeMap<String, Ingredient> map=reader.getIngredientIndexMap();
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			while(true){
				System.out.print("Enter keyword ");
				String keyword=in.readLine();
				Pattern pat=Pattern.compile(keyword);
				Iterator<String> it=map.keySet().iterator();
				int count=0;
				while(it.hasNext()){
					Ingredient item=map.get(it.next());
					String name=item.getName();
					Matcher mat=pat.matcher(name);
					if (mat.find()){
						count++;
						System.out.println(item.getId()+" "+item.getName()+" "+new DecimalFormat("0").format(item.getCalorie())
								+"kcal "+new DecimalFormat("0.0").format(item.getCarbohydrate())+"g　/100g");
					}
				}
				System.out.println("total "+count+" matches");
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
}
