package trial;

public class NutStandard extends IngredientD {			//	glicoによる各栄養素の推奨値（暫定版）50歳基準
	NutStandard(){
		salt=8.0;		//	食塩、目標量、女性 7.0、上限値にあたる
		
		K=3000.0;		//	目標値、女性 2600
		Ca=700.0;		//	推奨量、女性 650
		Mg=350.0;		//	推奨量、女性 290
		P=1000.0;		//	目安量、女性 800
		Fe=7.5;			//	推奨量、女性 6.5-10.5
		Zn=10.0;		//	推奨量、女性 8
		Cu=0.9;			//	推奨量、女性 0.8
		
		VA=0.85;		//	推奨量、女性 0.7、レチナール活性当量
		VB1=1.3;		//	推奨量、女性 1.0
		VB2=1.5;		//	推奨量、女性 1.1
		VB3=14.0;		//	推奨量、女性 11、ナイアシン
//		VB6=1.4;		//	推奨量、女性 1.2
//		VB12=0.0024;	//	推奨量
		VC=100.0;		//	推奨量
		VD=0.0055;		//	目安量
//		VE=6.5;			//	目安量、女性 6.0
		VK=0.15;		//	目安量
	}
}
