package trial;

import java.util.regex.*;

public class Ingredient extends IngredientD {
	String id,index;
	double utilization;					//	利用率 (1.0-廃棄率)
	Pattern pat=Pattern.compile("([0-9]+\\.{0,1}[0-9]*)");
	String[] rawdata;					//	食品成分表のフィールドの値。文字列などが含まれる
	public Ingredient(){}
	Ingredient(String[] rawdata){
		this.rawdata=rawdata;
		parse();
	}
	void parse(){
		id=rawdata[1];
		index=rawdata[2];
		name=rawdata[3];
		try {
			utilization=get_utilization(rawdata[4]);
			calorie=get_calorie(rawdata[5]);					//	カロリーのフィールドの値を取る
			carbohydrate=get_carbohydrate(rawdata[16]);			//	炭水化物
			protein=get_protein(rawdata[8]);					//	たんぱく質
			fat=get_fat(rawdata[10]);							//	脂質
			
			Na=get_number(rawdata[22]);
			K=get_number(rawdata[23]);
			Ca=get_number(rawdata[24]);
			Mg=get_number(rawdata[25]);
			P=get_number(rawdata[26]);
			Fe=get_number(rawdata[27]);
			Zn=get_number(rawdata[28]);
			Cu=get_number(rawdata[29]);
			Mn=get_number(rawdata[30]);
			
			VA=get_number(rawdata[40])/1000.0;
			VB1=get_number(rawdata[47]);
			VB2=get_number(rawdata[48]);
			VB3=get_number(rawdata[49]);
			VC=get_number(rawdata[55]);
		}
		catch(Exception e){
			System.out.println("number error id= "+id+" index= "+index+" "+rawdata[17]);
		}
	}
	double get_utilization(String str) throws Exception {			//	量ごとにフィールドのフォーマットが異なっている。
		return 1.0-get_number(str)/100.0;							//	%表示の廃棄率から1で正規化された利用率を求めて返す	
	}
	double get_calorie(String str) throws Exception {
		return get_number(str);
	}
	double get_carbohydrate(String str) throws Exception {
		return get_number(str);
	}
	double get_protein(String str) throws Exception {
		return get_number(str);
	}
	double get_fat(String str) throws Exception {
		return get_number(str);
	}
	double get_number(String str) throws Exception {
		double val=0.0;
		Matcher mat=pat.matcher(str);					//	マッチしないレコードの値は0でよい
		if (mat.find()){
			val=Double.parseDouble(mat.group(1));
		}
		return val;		
	}
	public String getId(){ return id; }
	public String getIndex(){ return index; }
	public String getName(){ return name; }
	public double getUtilization(){ return utilization; }
}
