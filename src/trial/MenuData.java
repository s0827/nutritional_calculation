package trial;

public class MenuData extends IngredientD {
	String keyword;							//	食材ファイルの名前
	IngredientD item;						//	食品成分データ
	String comment="";						//	食材ファイルの各行の残りの内容
	double quantity,utilization;			//	量(g)、利用率(1-廃棄率)

	MenuData(String keyword, IngredientD item, double quantity, double utilization, String comment){
		this.keyword=keyword;
		this.item=item;
		this.quantity=quantity;
		this.utilization=utilization;
		double ratio=quantity/100.0*utilization;
		set(item,ratio);
	}
	MenuData(String keyword, double calorie, double carbohydrate, double protein, double fat){
		this.keyword=keyword;
		this.calorie=calorie;
		this.carbohydrate=carbohydrate;
		this.protein=protein;
		this.fat=fat;
	}
	String getKeyword(){ return keyword; }
	IngredientD getItem(){ return item; }
	String getComment(){ return comment; }
	double getQuantity(){ return quantity; }
	double getUtilization(){ return utilization; }
}
