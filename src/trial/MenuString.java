package trial;

import java.util.LinkedList;

public class MenuString {			//	空レコードを無視してスペース、タブ区切りのデータを配列に分けて返す
	MenuString(){}
	public static String[] split(String instr){
		String[] strs=instr.split("[ \t]+");					//	語と語の間の余計なスペースは除去される
		LinkedList<String> list=new LinkedList<String>();
		for (int i=0; i<strs.length; i++){						//	行の頭の空レコードを除去する
			if (strs[i].length()>0) list.add(strs[i]);
		}
		String[] outstrs=new String[list.size()];
		int is=0;
		for (String s: list) outstrs[is++]=s;
		return outstrs;
	}
}
