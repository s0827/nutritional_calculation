package trial;


public class IngredientFoodProduct extends IngredientD {			//	加工食品用のデータ
	String comment;
	IngredientFoodProduct(){		
	}
	IngredientFoodProduct(String name){		
		this.name=name;
	}
	void overwrite(double calorie, double carbohydrate, double protein, double fat, double salt){
		if (calorie>0.0) 		this.calorie=calorie;
		if (carbohydrate>0.0) 	this.carbohydrate=carbohydrate;
		if (protein>0.0) 		this.protein=protein;
		if (fat>0.0)			this.fat=fat;
		if (salt>0.0)			this.salt=salt;
		comment="";
	}
}
