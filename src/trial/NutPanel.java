package trial;

import java.awt.*;
import java.awt.geom.*;
import java.text.*;

public class NutPanel extends Panel {				//	レンダラー。【注意】栄養の基準値が当面この中に書かれる。
	NutStandard standard;
	double calorie_max=1600.0;
	double carbohydrate_max=200.0;
	double protein_max=80.0;
	double fat_max=53.3;
	double e_carbohydrate=4.0;						//	糖質、たんぱく質、脂質1gあたりのエネルギー
	double e_protein=4.0;
	double e_fat=9.0;
	
	int panelwidth,panelheight;
	IngredientD data;
	int px0=50, py0=50, px1=70, py1=60;				//	エネルギー、糖質、たんぱく質、脂質の棒グラフのパラメータ
	int wx0=300, wy0=40;							//	ボックスのパラメータ
	
	int px2=30, py2=py0+py1*5+50, py3=40;			//	シグナルのパラメータ
	int cx0=15, cy0=15;
	
	NutPanel(int panelwidth, int panelheight){
		standard=new NutStandard();					//	栄養成分の標準値。値はファイルにハードコードされている
		this.panelwidth=panelwidth;
		this.panelheight=panelheight;
		setSize(panelwidth,panelheight);
		setVisible(true);
		
		carbohydrate_max=calorie_max*0.47/4.0;
		protein_max=calorie_max*0.2/4.0;
		fat_max=calorie_max*0.33/9.0;
	}
	void put_data(IngredientD data){
		this.data=data;
	}
	public void paint(Graphics gc){
		if (data==null) return;
		Graphics2D g2=(Graphics2D)gc;
		Rectangle2D background=new Rectangle2D.Double(0,0,panelwidth,panelheight);
		g2.setPaint(new Color(255,255,255));
		g2.fill(background);
		
		int px=px0;
		int py=py0;
		double calorie=data.getCalorie();
		double carbohydrate=data.getCarbohydrate();
		double protein=data.getProtein();
		double fat=data.getFat();
		
		double e0=carbohydrate*e_carbohydrate;
		double e1=protein*e_protein;
		double e2=fat*e_fat;
		double total_calorie=e0+e1+e2;
		int pe0=get_x(e0,calorie_max,wx0);
		int pe1=get_x(e1,calorie_max,wx0);
		int pe2=get_x(e2,calorie_max,wx0);
		
		try {
			px=px0+px1;
			Rectangle2D carb_box=new Rectangle2D.Double(px,py,pe0,wy0);					//	エネルギーと糖質、たんぱく質、脂質の評価
			g2.setPaint(new Color(255,127,127));
			g2.fill(carb_box);
			Rectangle2D prot_box=new Rectangle2D.Double(px+=pe0,py,pe1,wy0);
			g2.setPaint(new Color(127,255,127));
			g2.fill(prot_box);
			Rectangle2D f_box=new Rectangle2D.Double(px+=pe1,py,pe2,wy0);
			g2.setPaint(new Color(127,127,255));
			g2.fill(f_box);
			
			py+=py1;
			double pc0=get_x(carbohydrate,carbohydrate_max*1.5,wx0);
			Rectangle2D carbonhydrate_box=new Rectangle2D.Double(px0+px1,py,pc0,wy0);	//	糖質
			g2.setPaint(new Color(255,127,127));
			g2.fill(carbonhydrate_box);
			double pc1=get_x(carbohydrate_max,carbohydrate_max*1.5,wx0);
			Line2D carbohydrate_line=new Line2D.Double(px0+px1+pc1,py,px0+px1+pc1,py+wy0);
			g2.setPaint(new Color(0,0,0));
			g2.draw(carbohydrate_line);
			
			py+=py1;
			double pp0=get_x(protein,protein_max*1.5,wx0);
			Rectangle2D protein_box=new Rectangle2D.Double(px0+px1,py,pp0,wy0);			//	たんぱく質
			g2.setPaint(new Color(127,255,127));
			g2.fill(protein_box);
			double pp1=get_x(protein_max,protein_max*1.5,wx0);
			Line2D protein_line=new Line2D.Double(px0+px1+pp1,py,px0+px1+pp1,py+wy0);
			g2.setPaint(new Color(0,0,0));
			g2.draw(protein_line);

			
			py+=py1;
			double pf0=get_x(fat,fat_max*1.5,wx0);
			Rectangle2D fat_box=new Rectangle2D.Double(px0+px1,py,pf0,wy0);				//	脂質
			g2.setPaint(new Color(127,127,255));
			g2.fill(fat_box);
			double pf1=get_x(fat_max,fat_max*1.5,wx0);
			Line2D fat_line=new Line2D.Double(px0+px1+pf1,py,px0+px1+pf1,py+wy0);
			g2.setPaint(new Color(0,0,0));
			g2.draw(fat_line);
			
			for (int i=0; i<5; i++){
				Rectangle2D box=new Rectangle2D.Double(px0+px1,py0+i*py1,wx0,wy0);
				g2.setPaint(new Color(0,0,0));
				g2.draw(box);
			}
			px=px0;						//	外枠を描画
			py=py0+wy0-10;;
			g2.drawString("エネルギー",px,py);
			g2.drawString("糖質",px,py+=py1);
			g2.drawString("たんぱく質",px,py+=py1);
			g2.drawString("脂質",px,py+=py1);
			g2.drawString("塩分",px,py+=py1);
			
			px=px0+px1+wx0+10;			//	文字を書く
			py=py0+wy0-10;
			g2.drawString(new DecimalFormat("0").format(total_calorie)+"kcal",px,py);
			g2.drawString(new DecimalFormat("0.0").format(carbohydrate)+"g",px,py+=py1);
			g2.drawString(new DecimalFormat("0.0").format(protein)+"g",px,py+=py1);
			g2.drawString(new DecimalFormat("0.0").format(fat)+"g",px,py+=py1);			
			
			px=px0+px1+wx0;				//	最大値
			py=py0-5;
			g2.drawString(new DecimalFormat("0").format(calorie_max),px,py);
			g2.drawString(new DecimalFormat("0").format(carbohydrate_max),px0+px1+(int)pc1,py+=py1);
			g2.drawString(new DecimalFormat("0").format(protein_max),px0+px1+(int)pp1,py+=py1);
			g2.drawString(new DecimalFormat("0").format(fat_max),px0+px1+(int)pf1,py+=py1);
			
			px=px0;
			py=py2;
			g2.drawString("ミネラル",px,py);
			g2.drawString("ビタミン",px,py2+py3);

			px=px0+px1;
			py=py2-18;
			g2.drawString("K",px,py);				//	対象となる栄養素の名前、表示位置をハードコードしている。改善すべき
			g2.drawString("Ca",px+=px2,py);
			g2.drawString("Mg",px+=px2,py);
			g2.drawString("P",px+=px2,py);
			g2.drawString("Fe",px+=px2,py);
			g2.drawString("Zn",px+=px2,py);
			g2.drawString("Cu",px+=px2,py);
			
			px=px0+px1;
			py=py2+py3-18;
			g2.drawString("A",px,py);				//	対象となる栄養素の名前、表示位置をハードコードしている。改善すべき
			g2.drawString("B1",px+=px2,py);
			g2.drawString("B2",px+=px2,py);
			g2.drawString("Ni",px+=px2,py);
			g2.drawString("C",px+=px2,py);
			g2.drawString("D",px+=px2,py);
			g2.drawString("K",px+=px2,py);

			px=px0+px1;
			py=py2-15;
			Ellipse2D sygnal=new Ellipse2D.Double(px,py,cx0,cy0);
			g2.setPaint(get_color(data.getK(),standard.getK()));
			g2.fill(sygnal);
			sygnal=new Ellipse2D.Double(px+=px2,py,cx0,cy0);
			g2.setPaint(get_color(data.getCa(),standard.getCa()));
			g2.fill(sygnal);
			sygnal=new Ellipse2D.Double(px+=px2,py,cx0,cy0);
			g2.setPaint(get_color(data.getMg(),standard.getMg()));
			g2.fill(sygnal);
			sygnal=new Ellipse2D.Double(px+=px2,py,cx0,cy0);
			g2.setPaint(get_color(data.getP(),standard.getP()));
			g2.fill(sygnal);
			sygnal=new Ellipse2D.Double(px+=px2,py,cx0,cy0);
			g2.setPaint(get_color(data.getFe(),standard.getFe()));
			g2.fill(sygnal);
			sygnal=new Ellipse2D.Double(px+=px2,py,cx0,cy0);
			g2.setPaint(get_color(data.getZn(),standard.getZn()));
			g2.fill(sygnal);
			sygnal=new Ellipse2D.Double(px+=px2,py,cx0,cy0);
			g2.setPaint(get_color(data.getCu(),standard.getCu()));
			g2.fill(sygnal);
			
			px=px0+px1;
			py=py2+py3-15;
			sygnal=new Ellipse2D.Double(px,py,cx0,cy0);
			g2.setPaint(get_color(data.getVA(),standard.getVA()));
			g2.fill(sygnal);
			sygnal=new Ellipse2D.Double(px+=px2,py,cx0,cy0);
			g2.setPaint(get_color(data.getVB1(),standard.getVB1()));
			g2.fill(sygnal);
			sygnal=new Ellipse2D.Double(px+=px2,py,cx0,cy0);
			g2.setPaint(get_color(data.getVB2(),standard.getVB2()));
			g2.fill(sygnal);
			sygnal=new Ellipse2D.Double(px+=px2,py,cx0,cy0);
			g2.setPaint(get_color(data.getVB3(),standard.getVB3()));
			g2.fill(sygnal);
			sygnal=new Ellipse2D.Double(px+=px2,py,cx0,cy0);
			g2.setPaint(get_color(data.getVC(),standard.getVC()));
			g2.fill(sygnal);
/*			sygnal=new Ellipse2D.Double(px+=px2,py,cx0,cy0);
			g2.setPaint(get_color(data.getVD(),standard.getVD()));
			g2.fill(sygnal);			
			sygnal=new Ellipse2D.Double(px+=px2,py,cx0,cy0);
			g2.setPaint(get_color(data.getVK(),standard.getVK()));
			g2.fill(sygnal);*/

			px=px0+px1;
			py=py2-15;
			for (int i=0; i<7; i++){
				Ellipse2D circle=new Ellipse2D.Double(px+i*px2,py,cx0,cy0);
				g2.setPaint(new Color(0,0,0));
				g2.draw(circle);
			}
			for (int i=0; i<7; i++){
				Ellipse2D circle=new Ellipse2D.Double(px+i*px2,py+py3,cx0,cy0);
				g2.setPaint(new Color(0,0,0));
				g2.draw(circle);
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	int get_x(double x, double xmax, double w){
		int ix=(int)(wx0*(x/xmax));
		return ix;
	}
	Color get_color(double x, double xmax){
		double t=x/xmax;
		int r=0, g=0, b=0;
		if (t<0.01){
			r=255;
			g=255;
			b=255;
		}
		else if (t>=0.01 && t<0.5){
			r=255;
			g=(int)(255.0*2.0*t);
			b=0;	
		}
		else if (t>=0.5 && t<1.0){
			r=(int)(255.0*2.0*(1.0-t));
			g=255;
			b=0;
		}
		else if (t>=1.0 && t<1.5){
			r=0;
			g=255;
			b=(int)(255.0*2.0*(t-1.0));
		}
		else if (t>=1.5 && t<1.99){
			r=(int)(255.0*2.0*(t-1.5));
			g=(int)(255.0*2.0*(2.0-t));
			b=255;
		}
//		System.out.println(t+" "+r+" "+g+" "+b);
		Color c=new Color(r,g,b);
		return c;
	}
}
