package nuvis;

import java.util.*;
import java.awt.*;

import nutrition.*;

public class NuvisXTframe extends Frame {
	int offset=20;										//	画面のパラメータ　
	int framewidth=900, frameheight=200+offset;
	int panelwidth=1600, panelheight=1000;
	NuvisXTpanel panel;
	
	NuvisXTframe(){										//	画面の初期化
		setSize(framewidth, frameheight);
		setVisible(true);
		setLayout(null);
		panel=new NuvisXTpanel(panelwidth,panelheight);
		panel=new NuvisXTpanel(panelwidth, panelheight);
		panel.setLocation(0,offset);
		add(panel);
	}
	NuvisXTpanel getPanel() { return panel; }
	public void paint(Graphics gc){
		if (panel!=null) panel.repaint();
	}
}
