package nuvis;

import java.util.*;

import menu.IdReader;
import menu.IndexReader;
import nutrition.*;										//	栄養情報データの読み取り

public class NuvisXTnutrition_reader {
	LinkedHashMap<String, Nutritions> nutrition_table;	//	食品成分表のデータ	
	LinkedHashMap<String, Nutritions> toushitsu_table;	//	糖質事典のデータ
	LinkedHashMap<String, LinkedHashMap<String, Nutritions>> brand_tables;		//	外食のデータ
	String standard_file="mineral-vitamin.xml";			//	標準摂取量のデータ
	String idref_file="dictionary.txt";					//	食品成分表のIDを保存する。旧版と同じファイルを使う
	String brand_file="brands.txt";							//	外食のブランド名のデータ
	Nutritions standard_nutrition;						//	標準栄養必要量
	LinkedHashMap<String, String> IDmap;				//	食品名から食品成分表を参照するマップ

	public NuvisXTnutrition_reader(){
		StandardNutritionsXML snreader=new StandardNutritionsXML(standard_file);		//	標準摂取量を読み込む
		LinkedHashSet<String> mkeys=snreader.get_mkeys();
		LinkedHashSet<String> vkeys=snreader.get_vkeys();
		standard_nutrition=(Nutritions)snreader;

		IdReader idreader=new IdReader(idref_file);										//	食品名から食品成分表を参照するマップの取得
		IDmap=idreader.getIDmap();
		
		NutritionReader nutrition_reader=new NutritionReader();		//	食品成分表を読み込む
		nutrition_table=nutrition_reader.getNutritionIdMap();		//	読み込むファイルはNutritionReader内に記述
	
		Toushitsujiten toushitsujiten=new Toushitsujiten();			//	糖質事典を読み込む
		toushitsu_table=toushitsujiten.read();
		
		brand_tables=new LinkedHashMap<String, LinkedHashMap<String, Nutritions>>();
		IndexReader brands_reader=new IndexReader(brand_file);			//	外食のデータの読み込み
		FoodProductsXML foodprodcuctsxml=new FoodProductsXML();
		LinkedHashMap<String, String> brands=brands_reader.getMap();
		brands.forEach((K,V) ->{
			LinkedHashMap<String, Nutritions>table=foodprodcuctsxml.read(V, nutrition_table, IDmap, mkeys, vkeys);
			brand_tables.put(K,table);
		});
	}
	LinkedHashMap<String, Nutritions>get_items(LinkedHashMap<String, NuvisXTitem> input_items){
		LinkedHashMap<String, Nutritions> items=new LinkedHashMap<String, Nutritions>();
		Set<String> brand_names=brand_tables.keySet();
		Iterator<String> it=input_items.keySet().iterator();
		while(it.hasNext()) {
			String key=it.next();
			NuvisXTitem input_item=input_items.get(key);
			String source=input_item.getSource();
			if (source.toUpperCase().equals("D")) {
				String ID=IDmap.get(key);
				Nutritions item=nutrition_table.get(ID);
				if (item!=null) items.put(key,item);
			}
			else if (source.toUpperCase().equals("T")) {
				Nutritions t=toushitsu_table.get(key);
				if (t!=null) {
					Nutritions item=new Nutritions(t);
					item.mult(t.getWeight()/100.0);
					items.put(key,item);
				}
			}
			else if (brand_names.contains(source)) {
				LinkedHashMap<String, Nutritions> brand_table=brand_tables.get(source);
				Nutritions b=brand_table.get(key);
				if (b!=null) {
					Nutritions item=new Nutritions(b);
					item.mult(b.getWeight()/100.0);
					items.put(key,item);
				}
			}
		}
		return items;
	}
	Nutritions getStandard_nutrition() { return standard_nutrition; }
	public LinkedHashMap<String, LinkedHashMap<String, Nutritions>> getBrand_tables() { return  brand_tables; }
}
