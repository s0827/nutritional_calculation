package nuvis;

import java.util.*;

import menu.IdReader;

import java.io.*;
import java.awt.*;

import nutrition.*;

//	nuvisMain改良のためのテスト
//	食品成分表よりもAnalyzerで扱うアイテムについて栄養の比較ができるようにする

public class NuvisXT {
	NuvisXTnutrition_reader reader;							//	栄養データの読み込み処理
	LinkedHashMap<String, NuvisXTitem> input_items;			//	表示対象のアイテム
	LinkedHashMap<String, Nutritions> items;				//	アイテムのマップ、エネルギーや各栄養素をキーとしてソート可能とする
	String path="./nuvis/";
	String default_infile="NuvisXT(パン).txt";					//	入力ファイル
	String freffile="./nuvis/NuvisXTfref.txt";				//	参照先のファイル
	NuvisXTframe frame;
	NuvisXTpanel panel;
	
	double weight=100.0;									//	基準重量、表示制御デフォルト値
	double total_calorie=2000.0;							//	カロリー1日の基準値
	double ratio=0.333;										//	プロットする時1日の1/3を基準とする

	public static void main(String args[]) {
		NuvisXT nx=new NuvisXT();	
	}
	NuvisXT(){		
		reader=new NuvisXTnutrition_reader();				//	栄養データ読み込み
		TreeMap<Double, NuvisXTitem> keys=read_input_file();
		
		frame=new NuvisXTframe();							//	画面の生成
		panel=frame.getPanel();
		panel.setStd(reader.getStandard_nutrition());
		panel.put_data(items,keys);
		
		process();
	}
	void process() {										//	メインイベントループ
		BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("F ファイル名  : ファイルの読み込み");
			System.out.println("S [P|F|C|E] : PFCとエネルギーの順にソート、なしで入力順");
			System.out.println("R ratio     : 1日摂取量に対する割合を変える");
			System.out.println("C calorie   : 1日摂取エネルギーを変える");
			System.out.println("W weight    : 重量のスケールを変える");
			System.out.println("O           : 表示の昇順、降順の切り替え");
			System.out.println("E           : エネルギー表示のon/off");
			System.out.println("V           : ビタミン、ミネラル表示のon/off");
			System.out.println("D           : 数値表示のon/off");
			System.out.println("H           : ラベルヘッダ表示のon/off");
			System.out.println("Q           : 終了");
			
			while(true) {												//	コマンドの処理
				System.out.print("> ");
				String line=in.readLine();
				StringTokenizer tokens=new StringTokenizer(line," ");
				String C=tokens.nextToken().toUpperCase();
				if (C.equals("F")){
					if (tokens.hasMoreTokens()){
						TreeMap<Double, NuvisXTitem> keys=read_input_file(tokens.nextToken()+".txt");
						panel.put_data(items,keys);
					}
				}
				else if (C.equals("S")){								// PFCそれぞれをキーとする
					TreeMap<Double, NuvisXTitem>keys;
					if (tokens.hasMoreTokens()) {
						keys=get_keys(tokens);
					}
					else {
						keys=get_keys();
					}
					panel.setKeys(keys);
				}
				else if (C.equals("R")){								//	1日摂取量に対する比率を変える
					double r=ratio;
					if (tokens.hasMoreTokens()) r=Double.parseDouble(tokens.nextToken());							
					panel.setRatio(r);
				}
				else if (C.equals("C")){		//	1日摂取カロリーを変える
					double c=total_calorie;
					if (tokens.hasMoreTokens()) c=Double.parseDouble(tokens.nextToken());
					panel.setTotal_calorie(c);
				}
				else if (C.equals("W")){		//	重量のスケールを変える
					double w=weight;
					if (tokens.hasMoreTokens()) w=Double.parseDouble(tokens.nextToken());
					panel.setWeight(w);
				}
				else if (C.equals("O")){
					panel.toggle_sortorder();
				}				
				else if (C.equals("E")){
					panel.toggle_dispengy();
				}
				else if (C.equals("V")){
					panel.toggle_dispvm();
				}
				else if (C.equals("D")){
					panel.toggle_dispnum();
				}
				else if (C.equals("H")) {
					panel.toggle_dispheader();
				}
				frame.repaint();
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void read_nutrition_data() {
	}
	TreeMap<Double, NuvisXTitem> read_input_file(){
		return read_input_file(default_infile);
	}
	TreeMap<Double, NuvisXTitem> read_input_file(String infile){	//	ファイルから項目読み込み、<keyword, <source, weight>>の形でアイテムを指定						
		input_items=read_items(infile);								//	ファイルから読み込んだ食品のリスト(input_items)生成
		items=reader.get_items(input_items);											//	表示すべきデータをテーブルから抽出
		TreeMap<Double, NuvisXTitem> keys=get_keys();
		return keys;
	}
	LinkedHashMap<String, NuvisXTitem> read_items(String infile){
		LinkedHashMap<String, NuvisXTitem>input_items=new LinkedHashMap<String, NuvisXTitem>();
		try {
			BufferedReader in=new BufferedReader(new FileReader(path+infile));
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.substring(0,1).equals(":")) continue;
				String[] strs=line.split("\t");
				String source=strs[0];
				String keyword=strs[1];
				Double weight=null;
				if (strs.length>2) weight=Double.parseDouble(strs[2]);
				NuvisXTitem item=new NuvisXTitem(keyword,source,weight);
				input_items.put(keyword,item);
			}
			in.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return input_items;
	}
	TreeMap<Double, NuvisXTitem>get_keys(){
		return get_keys(null);
	}
	TreeMap<Double, NuvisXTitem>get_keys(StringTokenizer tokens){					//	検索キーの定義
		TreeMap<Double, NuvisXTitem> keys=new TreeMap<Double, NuvisXTitem>();
		if (tokens==null) {																//	キー指定なし
			Iterator<String> it=input_items.keySet().iterator();
			double d=0.0;
			while(it.hasNext()) keys.put(new Double(d=d+1.0),input_items.get(it.next()));
			return keys;			
		}
		String s=tokens.nextToken().toUpperCase();
		String S=s.toUpperCase();
		input_items.forEach((K,V) -> {
			Nutritions N=items.get(K);
			Double D=N.getCalorie();
			panel.setSortkey("E");
			if (S.equals("C")){
				D=N.getCarbohydrate();
				panel.setSortkey(S);
			}
			else if (S.equals("P")){
				D=N.getProtein();
				panel.setSortkey(S);
			}
			else if (S.equals("F")){
				D=N.getFat();
				panel.setSortkey(S);
			}
			else if (S.equals("W")){
				D=N.getWater();
			}
			else {
				LinkedHashMap<String, Double> minerals=N.getMinerals();
				Double Mi=minerals.get(s);
				if (Mi!=null) D=Mi;
				LinkedHashMap<String, Double> vitamins=N.getVitamins();
				Double Vi=vitamins.get(s);
				if (Vi!=null) D=Vi;
			}
			while(true) {							//	キーの重複を避ける
				NuvisXTitem I=keys.get(D);
				if (I==null) {
					break;
				}
				else {
					double d=D.doubleValue()+1.0e-8;
					D=new Double(d);
				}
			}
			keys.put(D,V);
		});
		return keys;
	}
}
