package nuvis;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;
import java.text.*;

import nutrition.Nutritions;

public class nuvisPanel extends Panel {
	int panelwidth,panelheight;
	Nutritions std;											//	標準摂取量
	TreeMap<Double, Nutritions> IDs;
	TreeMap<Double, Nutritions> key;
	boolean descending=false;
	boolean dispnum=true;				//	PFC、カロリーの数値表示
	boolean dispvm=true;				//	ビタミン、ミネラルの表示
	boolean dispdetail=false;			//	項目名の詳細表示
	Graphics2D g2;
	int dx=40;
	int dy=20;
	int wbar=280;
	int wcal=80;
	int wb=5;
	
	double weight,total_calorie,ratio;
	nuvisPanel(int panelwidth, int panelheight){
		this.panelwidth=panelwidth;
		this.panelheight=panelheight;
		setSize(panelwidth,panelheight);
		setVisible(true);
	}

	void set_data(TreeMap<Double, Nutritions> IDs){
		this.IDs=IDs;
		set_data(IDs,false);
	}
	void set_data(TreeMap<Double, Nutritions> IDs, boolean descending){
		key=IDs;
		this.descending=descending;
	}
	void toggle_dispnum(){ dispnum=toggle(dispnum); }
	void toggle_dispdetail(){ dispdetail=toggle(dispdetail); }
	void toggle_dispvm(){ dispvm=toggle(dispvm); }
	boolean toggle(boolean instat){
		boolean outstat=true;
		if (instat) outstat=false;
		return outstat;
	}
	void put_data(double weight, double total_calorie, double ratio, Nutritions std){
		this.weight=weight;
		this.total_calorie=total_calorie;
		this.ratio=ratio;
		this.std=std;
	}
	void setRatio(double ratio){ this.ratio=ratio; }
	void setTotal_calorie(double total_calorie){ this.total_calorie=total_calorie; }
	void setWeight(double weight){ this.weight=weight; }
	public void paint(Graphics gc){
		if (IDs==null || key==null) return;
		g2=(Graphics2D)gc;
		g2.setFont(new Font("Ariel",Font.BOLD,12));
		g2.setColor(Color.white);
		g2.fillRect(0,0,panelwidth,panelheight);
		
		draw_header();
		
		Iterator<Double> it=key.keySet().iterator();
		if (descending) it=key.descendingKeySet().iterator();
		int ix=20;
		int iy=20;
		while(it.hasNext()){
			iy+=dy;
			Nutritions N=key.get(it.next());
			String id=N.getId();
			int ixn=draw_PFC(ix,iy,N);
			ixn=draw_label(ixn,iy,id,N.getName());
			ixn=draw_calorie(ixn,iy,N)+dx;
			if (dispvm){
				ixn=draw_mv(ixn,iy,N.getMinerals(),std.getMinerals())+10;
				ixn=draw_mv(ixn,iy,N.getVitamins(),std.getVitamins())+10;				
			}	
	
			if (iy>600) break;
		}
	}
	void draw_header(){
		g2.setColor(new Color(255,127,127));
		g2.drawString("C",20,15);
		g2.setColor(new Color(127,255,127));
		g2.drawString("P",30,15);
		g2.setColor(new Color(127,127,255));
		g2.drawString("F",40,15);
		g2.setColor(Color.black);
		g2.drawString("W",50,15);
	
		g2.setColor(Color.black);
		g2.drawString("主要栄養素の量[g]",80,15);
		
		String cstr="1日の標準摂取量の"+new DecimalFormat("0").format(ratio*100)+"%を基準としたエネルギー ";
		if (dispvm) cstr+="、ミネラル、ビタミン";
		cstr+="の量";
		g2.drawString(cstr,310,15);
		
		g2.drawString("スケール "+new DecimalFormat("0").format(weight)+"g 食品成分表のデータは100g当たり",20,30);
		
		LinkedHashMap<String, Double> minerals=std.getMinerals();
		LinkedHashMap<String, Double> vitamins=std.getVitamins();
		if (dispvm) g2.drawString(get_item_str(minerals)+" "+get_item_str(vitamins),310,30);
	}
	String get_item_str(LinkedHashMap<String, Double> map){
		String str=map.keySet().toString();
		return str;
	}
	int draw_PFC(int ix, int iy, Nutritions N){
		Color ccarb=new Color(255,127,127);			//	塗りの色指定。糖質
		Color cprot=new Color(127,255,127);			//	たんぱく質
		Color cfat=new Color(127,127,255);			//	脂質

		double c=N.getCarbohydrate();
		double p=N.getProtein();
		double f=N.getFat();
		double w=N.getWater();
		
		double px0=(double)ix;
		double py0=(double)iy;
		double px=px0;
		double h=(double)dy;
		
		double u=N.getUtilization();
		double wu=u*(double)wbar;
		Rectangle2D b=new Rectangle2D.Double(px0,py0,wu,h);
		g2.setPaint(Color.lightGray);
		g2.draw(b);
		
		double wc=c/weight*(double)wbar;
		if (px+wc>px0+wbar) wc=px0+wbar-px;
		Rectangle2D bc=new Rectangle2D.Double(px0,py0,wc,h);
		g2.setPaint(ccarb);
		g2.fill(bc);
		
		px+=wc;
		double wp=p/weight*(double)wbar;
		if (px+wp>px0+wbar) wp=px0+wbar-px;
		Rectangle2D bp=new Rectangle2D.Double(px,py0,wp,h);
		g2.setPaint(cprot);
		g2.fill(bp);

		px+=wp;
		double wf=f/weight*(double)wbar;
		if (px+wf>px0+wbar) wf=px0+wbar-px;
		Rectangle2D bf=new Rectangle2D.Double(px,py0,wf,h);
		g2.setPaint(cfat);
		g2.fill(bf);
		
		double ww=(c+p+f+w)/weight*(double)wbar;
		if (px0+ww>px0+wbar) ww=wbar;
		Rectangle2D bw=new Rectangle2D.Double(px0,py0,ww,h);
		g2.setPaint(Color.black);
		g2.draw(bw);
		
		if (dispnum){
			String str=new DecimalFormat("0").format(c)
					+":"+new DecimalFormat("0").format(p)
					+":"+new DecimalFormat("0").format(f);
			if (w>0.0) str+=":"+new DecimalFormat("0").format(w);
			g2.setPaint(Color.black);
			g2.drawString(str,(int)(px+wf)+5,(int)py0+15);
		}
		return ix+wbar;
	}
	int draw_calorie(int ix, int iy, Nutritions N){
		double px0=(double)ix;
		double py0=(double)iy;
		double px=px0;
		double px1=px0+(double)wcal;
		double h=(double)dy;
				
		double cal=N.getCalorie();
		double c=cal/(total_calorie*ratio);
		double w=Math.min(c,1.0)*(double)wcal;
		Rectangle2D b=new Rectangle2D.Double(px0,py0,w,h);
		g2.setPaint(new Color(255,100,200));
		g2.fill(b);
		g2.setPaint(Color.black);
		g2.draw(b);
		
		if (dispnum){
			g2.setPaint(Color.black);
			g2.drawString(new DecimalFormat("0").format(cal),(int)(px0+w)+5,(int)py0+15);			
		}
		return ix+wcal;
	}
	int draw_mv(int ix, int iy, LinkedHashMap<String, Double> map, LinkedHashMap<String, Double> std_map){
		double px0=(double)ix;
		double py0=(double)iy;
		double px=px0;
		double w=(double)wb;
		double h=(double)dy;
		
		Iterator<String> it=std_map.keySet().iterator();
		while(it.hasNext()){
			String K=it.next();
			double std=std_map.get(K).doubleValue()*ratio;
			double val=0.0;
			Double V=map.get(K);
			if (V!=null) val=V.doubleValue();
			Color c=get_color(val,std);
			Rectangle2D b=new Rectangle2D.Double(px,py0,w,h);
			g2.setColor(c);
			g2.fill(b);
			px+=w;
		}
		return (int)px;
	}
	int draw_label(int ix, int iy, String id, String label){
		g2.setPaint(Color.black);
		if (!dispdetail){
			label=label.replaceAll("＜.+＞","");
			label=label.replaceAll("（.+）","");
			label=label.replaceAll("［.+］","");
		}
		if (dispdetail) label=id+" "+label;
		g2.drawString(label,ix,iy+dy-5);
		return ix+220;
	}
	Color get_color(double x, double xmax){
		double t=x/xmax;
		int r=0, g=0, b=0;
		if (t<0.01){
			r=255;
			g=255;
			b=255;
		}
		else if (t>=0.01 && t<0.5){
			r=255;
			g=(int)(255.0*2.0*t);
			b=0;	
		}
		else if (t>=0.5 && t<1.0){
			r=(int)(255.0*2.0*(1.0-t));
			g=255;
			b=0;
		}
		else if (t>=1.0 && t<1.5){
			r=0;
			g=255;
			b=(int)(255.0*2.0*(t-1.0));
		}
		else if (t>=1.5 && t<1.99){
			r=(int)(255.0*2.0*(t-1.5));
			g=(int)(255.0*2.0*(2.0-t));
			b=255;
		}
//		System.out.println(t+" "+r+" "+g+" "+b);
		Color c=new Color(r,g,b);
		return c;
	}
}
