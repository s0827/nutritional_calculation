package nuvis;

import java.util.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;

import nutrition.*;

public class NuvisXTpanel extends Panel {
	int panelwidth,panelheight;
	Nutritions std;								//	標準摂取量
	Graphics2D g2;
	LinkedHashMap<String, Nutritions> items;
	TreeMap<Double, NuvisXTitem> keys;
	int dx=40;									//	表示制御用パラメータ
	int dy=20;
	int wbar=280;
	int wcal=80;
	int wb=5;

	double weight=100.0;						//	デフォルト重量(100g)
	double ratio=1.0;							//	デフォルト一日必要量に対する割合
	double total_calorie=1600.0;				//	デフォルト必要カロリー(kcal)
	boolean sortorder=true;						//	ソートの方向。真で昇順
	boolean dispengy=true;						//	カロリー表示
	boolean dispvm=true;						//	ビタミン、ミネラル表示
	boolean dispnum=true;						//	PFC、カロリーの数値表示
	boolean dispheader=true;					//	タイトルヘッダの表示
	String sortkey="E";							//	ソートのキー(E,C,P,F)のいずれか
	
	NuvisXTpanel(int panelwidth, int panelheight){
		this.panelwidth=panelwidth;
		this.panelheight=panelheight;
		setSize(panelwidth,panelheight);
		setVisible(true);			
	}
	public void paint(Graphics gc){
		if (items==null) return;
		g2=(Graphics2D)gc;
		g2.setFont(new Font("Ariel",Font.BOLD,12));
		g2.setColor(Color.white);
		g2.fillRect(0,0,panelwidth,panelheight);
	
		draw_header();
		
		Iterator<Double> it=keys.keySet().iterator();
		if (!sortorder) it=keys.descendingKeySet().iterator();
		int ix=20;
		int iy=20;
		while(it.hasNext()){
			iy+=dy;
			NuvisXTitem item=keys.get(it.next());
			String keyword=item.getKeyword();
			String kw=item.get_keyword();			//	sourceの情報も含むキーワードを表示
			if (!dispheader) {
				String[] kws=kw.split(": ");
				kw=kws[1];
			}
			Nutritions N=items.get(keyword);
			String id=N.getId();
			int ixn=draw_PFC(ix,iy,N)+dx;
			ixn=draw_label(ixn,iy,id,kw);
			if (dispengy) {
				ixn=draw_calorie(ixn,iy,N)+dx;
			}
			else {
				ixn+=wcal+dx;
			}
			if (dispvm){
				ixn=draw_mv(ixn,iy,N.getMinerals(),std.getMinerals())+10;
				ixn=draw_mv(ixn,iy,N.getVitamins(),std.getVitamins())+10;				
			}
			if (iy>600) break;
		}
		
	}
	void draw_header(){
		g2.setColor(new Color(255,127,127));
		g2.drawString("C",20,15);
		g2.setColor(new Color(127,255,127));
		g2.drawString("P",30,15);
		g2.setColor(new Color(127,127,255));
		g2.drawString("F",40,15);
		g2.setColor(Color.black);
		g2.drawString("W",50,15);
	
		g2.setColor(Color.black);
		g2.drawString("主要栄養素の量[g]",80,15);
		
		String cstr="1日の標準摂取量の"+new DecimalFormat("0").format(ratio*100)+"%を基準としたエネルギー ";
		if (dispvm) cstr+="、ミネラル、ビタミン";
		cstr+="の量";
		g2.drawString(cstr,340,15);
		
		g2.drawString("スケール "+new DecimalFormat("0").format(weight)+"g 食品成分表のデータは100g当たり",20,30);
		
		LinkedHashMap<String, Double> minerals=std.getMinerals();
		LinkedHashMap<String, Double> vitamins=std.getVitamins();
		if (dispvm) g2.drawString(get_item_str(minerals)+" "+get_item_str(vitamins),340,30);
	}
	String get_item_str(LinkedHashMap<String, Double> map){
		String str=map.keySet().toString();
		return str;
	}
	int draw_PFC(int ix, int iy, Nutritions N){
		Color ccarb=new Color(255,127,127);			//	塗りの色指定。糖質
		Color cprot=new Color(127,255,127);			//	たんぱく質
		Color cfat=new Color(127,127,255);			//	脂質

		double c=N.getCarbohydrate();
		double p=N.getProtein();
		double f=N.getFat();
		double w=N.getWater();
		
		double px0=(double)ix;
		double py0=(double)iy;
		double px=px0;
		double h=(double)dy;
		
		double u=N.getUtilization();
		double wu=u*(double)wbar;
		Rectangle2D b=new Rectangle2D.Double(px0,py0,wu,h);
		g2.setPaint(Color.lightGray);
		g2.draw(b);
		
		if (sortkey.equals("F")) {
			px+=draw_box(f,px0,px,py0,h,cfat);			
			px+=draw_box(c,px0,px,py0,h,ccarb);
			px+=draw_box(p,px0,px,py0,h,cprot);
		}
		else if (sortkey.equals("P")) {
			px+=draw_box(p,px0,px,py0,h,cprot);
			px+=draw_box(f,px0,px,py0,h,cfat);			
			px+=draw_box(c,px0,px,py0,h,ccarb);
		}
		else {
			px+=draw_box(c,px0,px,py0,h,ccarb);
			px+=draw_box(p,px0,px,py0,h,cprot);
			px+=draw_box(f,px0,px,py0,h,cfat);			
		}

		double ww=(c+p+f+w)/weight*(double)wbar;
		if (px0+ww>px0+wbar) ww=wbar;
		Rectangle2D bw=new Rectangle2D.Double(px0,py0,ww,h);
		g2.setPaint(Color.black);
		g2.draw(bw);
		
		if (dispnum){
			String str=new DecimalFormat("0").format(c)
					+":"+new DecimalFormat("0").format(p)
					+":"+new DecimalFormat("0").format(f);
			if (w>0.0) str+=":"+new DecimalFormat("0").format(w);
			g2.setPaint(Color.black);
			g2.drawString(str,(int)px+5,(int)py0+15);
		}
		return ix+wbar;
	}
	double draw_box(double d, double px0, double px, double py0, double h, Color color) {
		double w=d/weight*(double)wbar;
		if (px+w>px0+wbar) w=px0+wbar-px;
		Rectangle2D bc=new Rectangle2D.Double(px,py0,w,h);
		g2.setPaint(color);
		g2.fill(bc);
		return w;
	}
	int draw_calorie(int ix, int iy, Nutritions N){
		double px0=(double)ix;
		double py0=(double)iy;
		double px=px0;
		double px1=px0+(double)wcal;
		double h=(double)dy;
				
		double cal=N.getCalorie();
		double c=cal/(total_calorie*ratio);
		double w=Math.min(c,1.0)*(double)wcal;
		Rectangle2D b=new Rectangle2D.Double(px0,py0,w,h);
		g2.setPaint(new Color(255,100,200));
		g2.fill(b);
		g2.setPaint(Color.black);
		g2.draw(b);
		
		if (dispnum){
			g2.setPaint(Color.black);
			g2.drawString(new DecimalFormat("0").format(cal),(int)(px0+w)+5,(int)py0+15);			
		}
		return ix+wcal;
	}

	int draw_mv(int ix, int iy, LinkedHashMap<String, Double> map, LinkedHashMap<String, Double> std_map){
		double px0=(double)ix;
		double py0=(double)iy;
		double px=px0;
		double w=(double)wb;
		double h=(double)dy;
		
		Iterator<String> it=std_map.keySet().iterator();
		while(it.hasNext()){
			String K=it.next();
			double std=std_map.get(K).doubleValue()*ratio;
			double val=0.0;
			Double V=map.get(K);
			if (V!=null) val=V.doubleValue();
			Color c=get_color(val,std);
			Rectangle2D b=new Rectangle2D.Double(px,py0,w,h);
			g2.setColor(c);
			g2.fill(b);
			px+=w;
		}
		return (int)px;
	}
	int draw_label(int ix, int iy, String id, String label){
		g2.setPaint(Color.black);
/*		if (!dispdetail){
			label=label.replaceAll("＜.+＞","");
			label=label.replaceAll("（.+）","");
			label=label.replaceAll("［.+］","");
		}
		if (dispdetail) label=id+" "+label;*/
		g2.drawString(label,ix,iy+dy-5);
		return ix+220;
	}
	Color get_color(double x, double xmax){
		double t=x/xmax;
		int r=0, g=0, b=0;
		if (t<0.01){
			r=255;
			g=255;
			b=255;
		}
		else if (t>=0.01 && t<0.5){
			r=255;
			g=(int)(255.0*2.0*t);
			b=0;	
		}
		else if (t>=0.5 && t<1.0){
			r=(int)(255.0*2.0*(1.0-t));
			g=255;
			b=0;
		}
		else if (t>=1.0 && t<1.5){
			r=0;
			g=255;
			b=(int)(255.0*2.0*(t-1.0));
		}
		else if (t>=1.5 && t<1.99){
			r=(int)(255.0*2.0*(t-1.5));
			g=(int)(255.0*2.0*(2.0-t));
			b=255;
		}
//		System.out.println(t+" "+r+" "+g+" "+b);
		Color c=new Color(r,g,b);
		return c;
	}
	void toggle_sortorder() { sortorder=toggle(sortorder); }
	void toggle_dispengy() { dispengy=toggle(dispengy); }
	void toggle_dispvm() { dispvm=toggle(dispvm); }
	void toggle_dispnum() { dispnum=toggle(dispnum); }
	void toggle_dispheader() { dispheader=toggle(dispheader); }
	boolean toggle(boolean inflag) {
		boolean outflag=true;
		if (inflag) outflag=false;
		return outflag;
	}
	void setSortkey(String sortkey) { this.sortkey=sortkey; }
	void setStd(Nutritions std) { this.std=std; }
	void setKeys(TreeMap<Double, NuvisXTitem> keys) { this.keys=keys; }
	void setRatio(double ratio) { this.ratio=ratio; }
	void setTotal_calorie(double total_calorie) { this.total_calorie=total_calorie; }
	void setWeight(double weight) { this.weight=weight; }
	void put_data(LinkedHashMap<String, Nutritions> items, TreeMap<Double, NuvisXTitem> keys) { 
		this.items=items;
		this.keys=keys;
	}
}
