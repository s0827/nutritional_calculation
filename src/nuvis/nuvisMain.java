package nuvis;

import java.awt.*;
import java.util.*;
import java.util.regex.*;
import java.io.*;

import menu.IdReader;
import nutrition.*;

public class nuvisMain extends Frame {						//	食品成分表の可視化
	int offset=20;
	int framewidth=900, frameheight=200+offset;
	int panelwidth=1600, panelheight=1000;
	String idref_file="dictionary.txt";						//	食品成分表のIDを保存する。旧版と同じファイルを使う
	String foodproducts_xml="foodproducts.xml";				//	加工食品のデータ
	String standard_file="mineral-vitamin.xml";				//	標準摂取量のデータ
	String dir="./nuvis/";
	nuvisPanel panel;
	LinkedHashMap<String, Nutritions> nutrition_table;		//	食品成分表のデータ
	LinkedHashMap<String, Nutritions> foodproducts;			//	加工食品、外食のデータ
	TreeMap<Double, Nutritions> IDs;						//	選択された項目のidリスト
	double weight=100.0;									//	基準重量
	double total_calorie=2000.0;							//	カロリー1日の基準値
	double ratio=0.333;										//	プロットする時1日の1/3を基準とする
	
	public static void main(String args[]){
		nuvisMain nuvis=new nuvisMain();
	}
	nuvisMain(){
		initgraphics();

		NutritionReader reader=new NutritionReader();		//	食品成分表の読み込み
		nutrition_table=reader.getNutritionIdMap();

		StandardNutritionsXML snreader=new StandardNutritionsXML(standard_file);		//	標準摂取量を読み込む
		Nutritions standard_nutrition=(Nutritions)snreader;
		LinkedHashSet<String> mkeys=snreader.get_mkeys();
		LinkedHashSet<String> vkeys=snreader.get_vkeys();

		IdReader idreader=new IdReader(idref_file);										//	食品名から食品成分表を参照するマップの取得
		LinkedHashMap<String, String> IDmap=idreader.getIDmap();
		FoodProductsXML foodproducts_reader=new FoodProductsXML();
		foodproducts=foodproducts_reader.read(foodproducts_xml, nutrition_table, IDmap, mkeys, vkeys);
		
		panel.put_data(weight,total_calorie,ratio,standard_nutrition);
		
		IDs=read_file("チーズ.txt");
		panel.set_data(IDs);
		repaint();

		process();											//	イベントループ
	}
	TreeMap<Double, Nutritions> read_file(String filename){
		TreeMap<Double, Nutritions>IDs=new TreeMap<Double, Nutritions>();
		double id=0.0;
		try {
			BufferedReader in=new BufferedReader(new FileReader(dir+filename));
			while(true){
				String line=in.readLine();					//	他のキーとの互換性のために番号をDoubleでつける
				if (line==null) break;
				if (line.length()>1) {
					if (line.substring(0,2).equals("//")) continue;
					String[] strs=line.split("\t+");
					Nutritions N=null;
					if (strs[1].toUpperCase().equals("F")){
						Nutritions M=foodproducts.get(strs[0]);
						if (M!=null) {
							N=new Nutritions(M);
							N.calculate(N,N.getWeight());
						}
					}
					else {
						N=nutrition_table.get(strs[1]);
					}
					if (N!=null) {
						IDs.put(new Double(id),N);
						id+=1.0;
					}
				}
			}
			in.close();			
		}
		catch(Exception e){
			System.err.println(e);
		}
		return IDs;
	}
	TreeMap<Double, Nutritions> find_items(StringTokenizer tokens){
		TreeMap<Double, Nutritions>IDs=new TreeMap<Double, Nutritions>();
		String keywords="";
		while(tokens.hasMoreTokens()){
			keywords+="("+tokens.nextToken()+")";
			if (tokens.hasMoreTokens()) keywords+="|";
		}
		Pattern pat=Pattern.compile(keywords);
		int id=0;
		Iterator<String> it=nutrition_table.keySet().iterator();
		while(it.hasNext()){
			Nutritions N=nutrition_table.get(it.next());
			Matcher mat=pat.matcher(N.getName());
			if (mat.find()){
				IDs.put(new Double(id),N);
				id++;
			}
		}
		return IDs;
	}
	void initgraphics(){
		setSize(framewidth, frameheight);
		setVisible(true);
		setLayout(null);
		panel=new nuvisPanel(panelwidth, panelheight);
		panel.setLocation(0,offset);
		add(panel);
	}
	void process(){
		BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("F ファイル名  : ファイルの読み込み");
			System.out.println("N キーワード  : 食品成分表のキーワードで検索");
			System.out.println("S [P|F|C|E] : PFCとエネルギーの順にソート、なしで入力順");
			System.out.println("R ratio     : 1日摂取量に対する割合を変える");
			System.out.println("C calorie   : 1日摂取エネルギーを変える");
			System.out.println("W weight    : 重量のスケールを変える");
			System.out.println("V           : ビタミン、ミネラル表示のon/off");
			System.out.println("D           : 数値表示のon/off");
			System.out.println("L           : 詳細なラベル表示のon/off");
			System.out.println("Q           : 終了");
			
			while(true){
				System.out.print("> ");
				String line=in.readLine();
				StringTokenizer tokens=new StringTokenizer(line," ");
				if (tokens.hasMoreTokens()){
					String C=tokens.nextToken().toUpperCase();
					if (C.equals("F")){
						if (tokens.hasMoreTokens()){
							IDs=read_file(tokens.nextToken()+".txt");
							panel.set_data(IDs);
						}
					}
					else if (C.equals("N")){
						IDs=find_items(tokens);
						panel.set_data(IDs);					
					}
					else if (C.equals("S")){									// PFCそれぞれをキーとする
						if (tokens.hasMoreTokens()) {
							TreeMap<Double, Nutritions>key=get_key(tokens);
							panel.set_data(key,true);							//　PFCが指定された時は降順にソート
						}
						else {
							panel.set_data(IDs,false);
						}
					}
					else if (C.equals("R")){		//	1日摂取量に対する比率を変える
						double r=ratio;
						if (tokens.hasMoreTokens()) r=Double.parseDouble(tokens.nextToken());							
						panel.setRatio(r);
					}
					else if (C.equals("C")){		//	1日摂取カロリーを変える
						double c=total_calorie;
						if (tokens.hasMoreTokens()) c=Double.parseDouble(tokens.nextToken());
						panel.setTotal_calorie(c);
					}
					else if (C.equals("W")){		//	重量のスケールを変える
						double w=weight;
						if (tokens.hasMoreTokens()) w=Double.parseDouble(tokens.nextToken());
						panel.setWeight(w);
					}
					else if (C.equals("V")){
						panel.toggle_dispvm();
					}
					else if (C.equals("D")){		//	数値表示の有無の切り替え
						panel.toggle_dispnum();
					}
					else if (C.equals("L")){		//	ラベルの詳細表示の切り替え
						panel.toggle_dispdetail();
					}
					else if (C.equals("Q")){
						System.exit(0);
					}
				}
				repaint();
			}			
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	TreeMap<Double, Nutritions>get_key(StringTokenizer tokens){
		TreeMap<Double, Nutritions> key=new TreeMap<Double, Nutritions>();
		String item=tokens.nextToken();
		IDs.forEach((K,N) -> {
			Double D=N.getCalorie();
			String I=item.toUpperCase();
			if (I.equals("C")){
				D=N.getCarbohydrate();
			}
			else if (I.equals("P")){
				D=N.getProtein();
			}
			else if (I.equals("F")){
				D=N.getFat();
			}
			else if (I.equals("W")){
				D=N.getWater();
			}
			else {
				LinkedHashMap<String, Double> minerals=N.getMinerals();
				Double Mi=minerals.get(item);
				if (Mi!=null) D=Mi;
				LinkedHashMap<String, Double> vitamins=N.getVitamins();
				Double Vi=vitamins.get(item);
				if (Vi!=null) D=Vi;
			}
			key.put(D,N);
		});
		return key;
	}
	public void paint(Graphics gc){
		if (panel!=null) panel.repaint();
	}
}
