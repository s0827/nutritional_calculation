package nuvis;

public class NuvisXTitem {
	String keyword,source;
	Double weight;
	NuvisXTitem(String keyword, String source, Double weight) {
		this.keyword=keyword;
		this.source=source;
		this.weight=weight;
	}
	String get_keyword() { return source+": "+keyword; }
	String getKeyword( ) { return keyword; }
	String getSource() { return source; }
	Double getWeight() { return weight; }
}
