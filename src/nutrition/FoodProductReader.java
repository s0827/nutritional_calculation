package nutrition;

import java.io.*;
import java.util.*;

import menu.*;

public class FoodProductReader {									//	食品成分表以外の栄養データの読み込み。この中で色々なソースからデータを取り込む
	LinkedHashMap<String, Nutritions> foodproducts;					//	栄養データのテーブル。これを構築して返却する
	NutritionSearch nutsearch;											//	検索器をこの中で取得する
	LinkedHashMap<String, String> IDmap;							//	食品名から食品成分表を参照するマップ
	
	public FoodProductReader(LinkedHashMap<String, Nutritions> nutrition_table, LinkedHashMap<String, String> IDmap,
			LinkedHashSet<String> mkeys, LinkedHashSet<String> vkeys){	
		foodproducts=new LinkedHashMap<String, Nutritions>();
		
		FoodProductsXML foodproductsxml=new FoodProductsXML();		//	加工食品のデータの読み込み(初期の実装によるものを最初に読み込む）
		
		LinkedHashMap<String, Nutritions> old_table=foodproductsxml.read("foodproducts.xml", nutrition_table, IDmap, mkeys, vkeys);

		Toushitsujiten toushitsujiten=new Toushitsujiten();			//	糖質事典
		LinkedHashMap<String, Nutritions> toushitsu_table=toushitsujiten.read();
		
		SFoodProducts simple=new SFoodProducts();					//	簡易リスト読み込み(直接入力の形式)。今後変更の可能性がある
		LinkedHashMap<String, Nutritions> simple_table=simple.read();
		
		foodproducts.putAll(old_table);
		foodproducts.putAll(toushitsu_table);
		foodproducts.putAll(simple_table);
			
		nutsearch=new NutritionSearch(nutrition_table,IDmap,old_table,toushitsu_table,simple_table);
	}
	public LinkedHashMap<String, Nutritions> getFoodproducts(){ return foodproducts; }
	public NutritionSearch getNutsearch(){ return nutsearch; }
}
