package nutrition;

public class PFC {						//	３大栄養素
	public double calorie;				//	カロリー
	public double carbohydrate;			//	糖質
	public double protein;				//	たんぱく質
	public double fat;					//	脂質
	public double fiber;				//	食物繊維
	
	double ec=4.0,ep=4.0,ef=9.0,ed=2.0;	//	糖質、たんぱく質、脂質、食物繊維1gあたりのエネルギー
	public PFC(){}
	public PFC(double calorie, double carbohydrate, double protein, double fat, double fiber){
		this.calorie=calorie;
		this.carbohydrate=carbohydrate;
		this.protein=protein;
		this.fat=fat;
		this.fiber=fiber;
	}
	public PFC(Nutritions N){
		this.calorie=N.calorie;
		this.carbohydrate=N.carbohydrate;
		this.protein=N.protein;
		this.fat=N.fat;
		this.fiber=N.fiber;		
	}
	public PFC(MenuData D){
		this.calorie=D.calorie;
		this.carbohydrate=D.carbohydrate;
		this.protein=D.protein;
		this.fat=D.fat;
		this.fiber=D.fiber;
	}
	public void substitute(){
		if (carbohydrate==0.0){										//	糖質量=0のとき、エネルギー、たんぱく質、脂質から推定する
			carbohydrate=(calorie-(ep*protein+ef*fat))/ec;
		}
		else if (fiber==0.0 && fat!=0.0){							//	食物繊維の量の推定									
			double cal_fiber=calorie-(ec*carbohydrate+ep*protein+ef*fat);
			if (cal_fiber>0.0){										//	炭水化物が糖質の意味と推定されるとき
				fiber=cal_fiber/ed;
			}
			else {													//	炭水化物が食物繊維も含むと推定されるとき
				double cal_carb=calorie-(ep*protein+ef*fat);		//	エネルギー、たんぱく質、脂質から糖質を推定し、残りの炭水化物は食物繊維と考える
				double carb=(cal_carb-ed*carbohydrate)/(ec-ed);
				fiber=carbohydrate-carb;
				if (fiber>0.0){
					carbohydrate=carb;
				}
				else {
					fiber=0.0;
				}				
			}
		}
		if (fat==0.0){												//	脂質=0のとき
			fat=(calorie-(ec*carbohydrate+ep*protein+ed*fiber))/ef;
		}
	}
	void copyPFC(PFC pfc){
		calorie=pfc.calorie;
		carbohydrate=pfc.carbohydrate;
		protein=pfc.protein;
		fat=pfc.fat;
		fiber=pfc.fiber;
	}
	public double getCalorie(){ return calorie; }
	public double getCarbohydrate(){ return carbohydrate; }
	public double getProtein(){ return protein; }
	public double getFat(){ return fat; }
	public double getFiber(){ return fiber; }
}
