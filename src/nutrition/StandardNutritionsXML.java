package nutrition;

import java.io.*;
import java.util.*;
import java.text.*;

import javax.xml.parsers.*;

import org.w3c.dom.*;

public class StandardNutritionsXML extends Nutritions {		//	データは親クラスに保存。このクラスに保持される栄養素が
	StandardNutritionsXML(){}
	public StandardNutritionsXML(String xmlfile){			//	ファイルからミネラルとビタミンの標準摂取量を読み込む
		try {
			InputStream is=new FileInputStream(xmlfile);
			DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
			DocumentBuilder builder=factory.newDocumentBuilder();
			Document doc=builder.parse(is);
			Element rootElement=doc.getDocumentElement();
			
			NodeList mineral_list=rootElement.getElementsByTagName("minerals");
			minerals=read_items(mineral_list);
			NodeList vitamin_list=rootElement.getElementsByTagName("vitamins");
			vitamins=read_items(vitamin_list);
			
			is.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	LinkedHashMap<String, Double> read_items(NodeList nodelist){		//	ミネラルとビタミンのデータ構造は同じなので一つの関数で読み込む
		LinkedHashMap<String, Double> map=new LinkedHashMap<String, Double>();
		try {
			Node minerals_node=nodelist.item(0);
			NodeList mineral_items=((Element)minerals_node).getElementsByTagName("item");
			for (int i=0; i<mineral_items.getLength(); i++){
				Node itemnode=mineral_items.item(i);
				NamedNodeMap itemattr=itemnode.getAttributes();
				String name=itemattr.getNamedItem("name").getNodeValue();
				double std_intake=0.0;
				Node std_intake_node=itemattr.getNamedItem("std_intake");
				if (std_intake_node!=null){
					std_intake=Double.parseDouble(std_intake_node.getNodeValue());
				}
				map.put(name,new Double(std_intake));
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
		return map;
	}
	public LinkedHashSet<String> get_mkeys(){
		return get_keys(minerals);
	}
	public LinkedHashSet<String> get_vkeys(){
		return get_keys(vitamins);
	}
	LinkedHashSet<String> get_keys(LinkedHashMap<String, Double> map){
		LinkedHashSet<String> keys=new LinkedHashSet<String>();
		map.forEach((K,V) -> keys.add(K));
		return keys;
	}
	public void output(){		//	デバッグ用出力
		output(minerals);
		output(vitamins);
	}
	void output(LinkedHashMap<String, Double> map){
		map.forEach((name,Std_intake) -> 
			System.out.println(name+"\t"+new DecimalFormat("0.00000").format(Std_intake.doubleValue()))
		);
		System.out.println();
	}
	public static void main(String args[]){
		StandardNutritionsXML sn=new StandardNutritionsXML("mineral-vitamin.xml");
		sn.output();
	}
}
