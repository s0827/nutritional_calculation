package nutrition;

import java.util.*;
import java.util.regex.*;
import java.text.*;

public class MenuData extends Nutritions {				//	重量をかけたその材料の栄養の値を保持する
	String id;											//	ID番号
	String keywords;									//	キーワード、最初の行の内容	
	String selected;									//	選択フラグ(#@など)。項目ごとに指定できるようにしたい。タイトル毎に設定するが項目の設定でoverrideできるとよい
	boolean select_f=true;								//	選択フラグの処理をする
	String comment;										//	コメント
	LinkedList<MenuData> menuitems;						//	このメニュー項目はタイトルの場合があり、その場合はメニュー項目のマップを下位に持つ。２層以上の階層を持つことは想定しない
	MenuServing mserv;									//	食数の情報
	
	public MenuData(String id, String keywords, Nutritions standard_nutrition){		//	メタメニュー、selectedの処理をしない（すべての項目を受理する）
		super();
		menuitems=null;
		set_id(id,keywords,"");
		select_f=false;
		initialize_keys(standard_nutrition);
	}
	public MenuData(String Mid,String keywords,String selected,Nutritions standard_nutrition, MenuServing mserv, String comment){
		super();
		menuitems=null;
		set_id(id,keywords,selected);
		this.mserv=mserv;
		this.comment=comment;
		initialize_keys(standard_nutrition);	
	}
	public MenuData(String id, String keywords, String selected, Nutritions standard_nutrition, String comment){
		super();
		menuitems=null;
		set_id(id,keywords,selected);
		this.comment=comment;
		initialize_keys(standard_nutrition);
	}
	public MenuData(String id, String keywords, String selected, Nutritions N, double quantity, String comment){
		super();										//	ここで初期化する										
		set_id(id,keywords,selected);
		calculate(N,quantity);							//	親クラスを辿って栄養を計算する
		this.comment=comment;
	}
	public MenuData(String id, String keywords, String selected,Nutritions standard_nutrition, 
			String comment, double calorie, double carbohydrate, double protein, double fat, double salt, double fiber){
		super(calorie,carbohydrate,protein,fat,salt,fiber);
		initialize(standard_nutrition);
		substitute();
		set_id(id,keywords,selected);
		this.comment=comment;
	}
	public MenuData(MenuData D){						//	コピーコンストラクタ
		super(D);
		this.id=D.id;
		this.keywords=D.keywords;
		this.selected=D.selected;
		this.comment=D.comment;
		if (D.mserv!=null) this.mserv=copy(D.mserv);
		if (D.menuitems!=null) this.menuitems=copy(D.menuitems);
	}
	LinkedList<MenuData> copy(LinkedList<MenuData> src){
		LinkedList<MenuData> dst=new LinkedList<MenuData>();
		for (MenuData D: src){
			MenuData M=new MenuData(D);
			dst.add(M);
		}
		return dst;
	}
	MenuServing copy(MenuServing src){
		return new MenuServing(src);
	}
	public void set_id(String id, String selected){
		this.id=id;
		this.selected=selected;
	}
	public void set_id(String id, String keywords, String selected){
		this.id=id;
		this.keywords=keywords;
		this.selected=selected;
	}
	public void add_item(MenuData item){
		if (menuitems==null) menuitems=new LinkedList<MenuData>();
		menuitems.add(item);
	}
	public void summation() {										//	条件によらず下位の項目を集計する
		if (menuitems==null) return;
		menuitems.forEach(V -> {
			V.summation();
			add_nutritions(V);
		});
	}
	public void summation(String select){
		if (menuitems==null) return;								//	下位の項目がないときは処理をしない
		menuitems.forEach(V -> {
			V.summation(select);									//	下位の項目の計算をしたあとで足しあげる
			if (select_f){
				Pattern pat=Pattern.compile(select);
				Matcher mat=pat.matcher(V.getSelected());
				if (mat.find()) add_nutritions(V);
			}
			else {
				add_nutritions(V);									//	選択操作がないときは単純に足しあげる				
			}
		});
	}
	public void output(String select, int depth, int ilevel, int width){			//	ここでも選択操作が必要
		int d=depth-1;
		int i=ilevel+1;
		boolean output_f=true;
		if (select==null) {
			output_f=true;
		}
		else {
			if (select_f) {
				Pattern pat=Pattern.compile(select);						//	選択されているかチェック
				Matcher mat=pat.matcher(selected);
				if (mat.find()){
					output_f=true;
				}
				else {
					output_f=false;
				}
			}			
		}
		if (output_f) {
			output_major_nutritions(keywords,ilevel,width,menuitems);	//	主要栄養素の出力。menuitemsがnull(最下層)の時に重量を出力することを考える
			if (width==2) output_mv(ilevel);							//	ミネラル、ビタミンの出力
			if (d>0 && menuitems!=null){								//	深さを判断して、下の階層のデータを出力する
				menuitems.forEach(V -> V.output(select,d,i,width));
			}	
		}
	}
	public void output(){												//	メニュー内容の出力
		System.out.println("\t"+keywords+"\t"+format_quantity(quantity)+"g");
	}
	String format_quantity(double quantity) {
		String str=new DecimalFormat("0.0").format(quantity);
		if (str.substring(str.length()-2).equals(".0")) str=str.substring(0,str.length()-2);
		return str;
	}
	public void output_summary(){
		System.out.println("\t"+new DecimalFormat("0").format(calorie)
				+"\t"+new DecimalFormat("0.0").format(carbohydrate)
				+"\t"+new DecimalFormat("0.0").format(protein)
				+"\t"+new DecimalFormat("0.0").format(fat)
				+"\t"+new DecimalFormat("0.0").format(salt)
				+"\t"+new DecimalFormat("0.0").format(fiber));
	}
	public void output(boolean quantity_f, boolean ratio_f, int depth, int width, Nutritions standard_nutrition) {		//	レシピ検索出力用に新しく作成。階層はタイトルと素材の項目のみとする
//		System.out.println(quantity_f+","+depth+","+width);
		if (depth==1) {
			if (menuitems!=null) {
				if (quantity_f) {
					System.out.println(keywords);								
				}
				else {
					if (width==2) System.out.println();
					output_major_nutritions(keywords,0,width,ratio_f,standard_nutrition);				//	主要栄養素の出力
					if (width==2) output_mv(1,ratio_f,standard_nutrition);								//	ミネラル、ビタミンの出力						
				}
			}
		}
		else if (depth>1) {
			if (quantity_f) {
				if (menuitems!=null) {
					System.out.println();
					System.out.println(keywords);
				}
				else {
					String qty="";
					if (quantity>0.0) qty+="\t"+format_quantity(quantity)+"g";
					System.out.println(" "+keywords+qty);						
				}
			}		
			else {
				if (menuitems!=null) {
					System.out.println();
					output_major_nutritions(keywords,0,width,ratio_f,standard_nutrition);		//	主要栄養素の出力
					if (width==2) output_mv(1,ratio_f,standard_nutrition);						//	ミネラル、ビタミンの出力								
				}
				else {
					output_major_nutritions(keywords,1,width,ratio_f,standard_nutrition);		//	主要栄養素の出力
					if (width==2) output_mv(1,ratio_f,standard_nutrition);						//	ミネラル、ビタミンの出力								
				}
			}
		}
	}
	public void setId(String id) { this.id=id; }
	public void setMenuitems(LinkedList<MenuData> menuitems ) { this.menuitems=menuitems; }
	public void setMserv(MenuServing mserv) { this.mserv=mserv; }
	public LinkedList<MenuData> getMenuitems(){ return menuitems; }
	public String getSelected(){ return selected; }
	public String getId(){ return id; }
	public String getKeywords(){ return keywords; }
	public MenuServing getMserv(){ return mserv; }
}
