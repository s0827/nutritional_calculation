package nutrition;

import java.io.*;
import java.util.*;

public class Toushitsujiten extends NutritionParserUtil {
	String filename="toushitsujiten.txt";
	public Toushitsujiten(){
	}
	public LinkedHashMap<String, Nutritions>read(){				//	糖質事典のデータを読み込む。テキストファイルに保存されているのは一部のデータ。
		LinkedHashMap<String, Nutritions> toushitsujiten=new LinkedHashMap<String, Nutritions>();
		try {
			BufferedReader in=new BufferedReader(new FileReader(filename));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				String[] strs=parse(line);
				if (strs==null) continue;
				String name=strs[0];
				Nutritions nutritions=new Nutritions(name);
				
				double calorie=Double.parseDouble(strs[1]);					//	糖質事典には脂質の情報はない
				double carbohydrate=Double.parseDouble(strs[2]);			//	ビタミン、ミネラルの情報はない
				double protein=Double.parseDouble(strs[5]);
				double salt=Double.parseDouble(strs[3]);
				double fiber=Double.parseDouble(strs[4]);
				double weight=Double.parseDouble(strs[6]);

				PFC pfc=new PFC(calorie,carbohydrate,protein,0.0,fiber);	//	糖質値、食物繊維についてデータを補完する
				pfc.substitute();											//	脂質の値を推定する
			
				nutritions.copyPFC(pfc);									//	３大栄養素の値のコピー。Nutritionsの親クラスで処理されている
				nutritions.setSalt(salt);									//	糖質辞典のレコードは常に重量を含む
				nutritions.set_weight(weight,true);							//	加工食品の与えられた重量をそのまま使うときのために保存する

				nutritions.normalize_nutritions(weight);					//	100gあたりの値に直す
				toushitsujiten.put(name,nutritions);
//				nutritions.output_major_nutritions(0);
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
		return toushitsujiten;
	}
}
