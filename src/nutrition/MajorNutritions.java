package nutrition;

public class MajorNutritions extends PFC {
	String group_id,id,index,name;		//	グループid(idの上位2桁)、id、食品成分表の索引番号、食品名
	String brand;						//	ブランド名、foodproducts.xmlから読み込む加工食品、外食で使用
	public double salt;					//	塩分の処理は保留
	public double water;
	public double utilization;			//	利用率(1.0-廃棄率)
	MajorNutritions(){}
	MajorNutritions(String name){ this.name=name; }
	public MajorNutritions(Nutritions N){				//	コピーコンストラクタ
		super(N);
		this. group_id=N.group_id;
		this.id=N.id;
		this.index=N.index;
		this.name=N.name;
		this.brand=N.brand;
		this.salt=N.salt;
		this.water=N.water;
		this.utilization=N.utilization;
	}
	public MajorNutritions(MenuData D){
		super(D);
		this.group_id=D.group_id;
		this.id=D.id;
		this.index=D.index;
		this.name=D.name;
		this.brand=D.brand;
		this.salt=D.salt;
		this.water=D.water;
		this.utilization=D.utilization;
	}
	void set_id(String group_id, String id, String index, String name){
		this.group_id=group_id;
		this.id=id;
		this.index=index;
		this.name=name;
	}
	public MajorNutritions(double calorie, double carbohydrate, double protein, double fat, double salt, double fiber){
			setMajorNutritions(calorie,carbohydrate,protein,fat,salt,fiber,water,1.0);
	}
	public void setName(String name){ this.name=name; }
	void setMajorNutritions(double calorie, double carbohydrate, double protein, double fat, 
			double salt, double fiber, double water, double utilization){
		this.calorie=calorie;
		this.carbohydrate=carbohydrate;
		this.protein=protein;
		this.fat=fat;
		this.salt=salt;
		this.fiber=fiber;
		this.water=water;
		this.utilization=utilization;
	}
	void copyMajorNutritions(MajorNutritions M){			//	コメントなどはコピーしない
		salt=M.salt;
		utilization=M.utilization;
		copyPFC(M);
	}
	void calculate_major_nutritions(Nutritions N, double quantity){		//	この中で100g単位に換算
		calorie=N.calorie*quantity/100.0;
		carbohydrate=N.carbohydrate*quantity/100.0;
		protein=N.protein*quantity/100.0;
		fat=N.fat*quantity/100.0;
		salt=N.salt*quantity/100.0;
		fiber=N.fiber*quantity/100.0;
		water=N.water*quantity/100.0;
		utilization=N.utilization/100.0;		
	}
	void add_major_nutritions(Nutritions N){
		calorie+=N.calorie;
		carbohydrate+=N.carbohydrate;
		protein+=N.protein;
		fat+=N.fat;
		salt+=N.salt;
		fiber+=N.fiber;
		water+=N.water;
	}
	public void normalize_nutritions(double weight){
		calorie=calorie*100.0/weight;
		carbohydrate=carbohydrate*100.0/weight;
		protein=protein*100.0/weight;
		fat=fat*100.0/weight;
		salt=salt*100.0/weight;
		fiber=fiber*100.0/weight;
		water=water*100.0/weight;
	}
	public void mult_major_nutritions(double mult){
		calorie=calorie*mult;
		carbohydrate=carbohydrate*mult;
		protein=protein*mult;
		fat=fat*mult;
		salt=salt*mult;
		fiber=fiber*mult;
		water=water*mult;
	}
	void setSalt(double salt){ this.salt=salt; }
	public String getId(){ return id; }
	public String getName(){ return name; }
	public String getBrand() { return brand; }
	public double getSalt(){ return salt; }
	public double getWater() { return water; }
	public double getUtilization() { return utilization; }
}
