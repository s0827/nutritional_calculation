package nutrition;

import java.util.regex.*;
import java.text.*;

public class MenuServing {
	String keywords="";
	double weight=0.0;
	double serving=1.0;
	public MenuServing(String instr){
		String[] strs=instr.split(" +");
		keywords=strs[0];
		Pattern npat=Pattern.compile("([0-9\\.]+)g");
		Pattern spat=Pattern.compile("([0-9\\.]+)");
		if (strs.length>1){
			for (int i=1; i<strs.length; i++){
				boolean stat=true;							//	数量データでなければキーワードに追加
				Matcher nmat=npat.matcher(strs[i]);			//	食数と重量を同時に認識できるようになっている
				if (nmat.find()){
					weight=Double.parseDouble(nmat.group(1));
					stat=false;
				}
				else {
					Matcher smat=spat.matcher(strs[i]);
					if (smat.find()){
						serving=Double.parseDouble(smat.group(1));
						stat=false;
					}
				}
				if (stat) keywords+=" "+strs[i];
			}
		}
	}
	public void copy(MenuServing M) {
		keywords=M.keywords;
		weight=M.weight;
		serving=M.serving;
	}
	public double get_quantity(MenuServing M){
		double quantity=1.0;
		boolean stat=true;
		if (weight!=0.0){
			if (M.getWeight()!=0.0) {
				quantity=weight/M.getWeight();
				stat=false;
			}
		}
		if (stat){
			if (serving!=0.0){
				if (M.getServing()!=0.0) {
					quantity=serving/M.getServing();
				}
			}
		}
		return quantity;
	}
	MenuServing(MenuServing M){
		keywords=M.keywords;
		weight=M.weight;
		serving=M.serving;
	}
	public void setWeight(double weight) { this.weight=weight; }
	public void setServing(double serving) { this.serving=serving; }
	public String getKeywords(){ return keywords; }
	public double getWeight(){ return weight; }
	public double getServing(){ return serving; }
	
	public String output() {
		String str="("+keywords+" "+new DecimalFormat("0.00").format(weight)
				+" "+new DecimalFormat("0.00").format(serving)+")";
		return str;
	}
}
