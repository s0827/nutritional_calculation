package nutrition;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class NutritionReader {									//	食品成分表のデータの読み取り。IngredientReaderを改良。成分表の分析はこのクラスで行う。
	String filename="ingredient2015.txt";						//	食品成分表のファイル名
	String line;												//	現在データ行。例外処理に渡すため
	int numlines=1;												//	データライン数
	int numitems=0;												//	食品の項目数
	int maxnum=0;												//	表の上の項目の上限値
	LinkedHashMap<String, Nutritions> NutritionIdMap;			//	食品成分表のIDでレコードを参照するマップ
	LinkedHashMap<String, Nutritions> NutritionIndexMap;		//	食品成分表の索引番号で参照するマップ
	LinkedList<Integer> missing_nums=new LinkedList<Integer>();	//	索引番号の欠番

	String group_str="([0-9]{2})";
	String id_str="([0-9]{5})";
	String num_str="([0-9]+)";
	Pattern pat=Pattern.compile("([0-9]+\\.{0,1}[0-9]*)");		//	食品成分表から数値を抽出するためのパターン

	public NutritionReader(){
		Pattern group_pat=Pattern.compile(group_str);
		Pattern id_pat=Pattern.compile(id_str);
		Pattern num_pat=Pattern.compile(num_str);
		
		NutritionIdMap=new LinkedHashMap<String, Nutritions>();
		NutritionIndexMap=new LinkedHashMap<String, Nutritions>();

		try {
			BufferedReader in=new BufferedReader(new FileReader(filename));
			while(true){
				line=in.readLine();
				if (line==null) break;
				numlines++;
				String[] strs=line.split("\t");

//				int numrecords=strs.length;
//				if (numrecords!=68) System.out.println(numlines+"\t"+numrecords+"\t("+line+")");		//	長さが異常なラインの検出（改行の異常）
				if (numlines==5) getnutrition_list(in,line);
				if (strs.length>1){
					Matcher group_mat=group_pat.matcher(strs[0]);
					Matcher id_mat=id_pat.matcher(strs[1]);
					if (group_mat.find() && id_mat.find()){
						Matcher num_mat=num_pat.matcher(strs[2]);
						if (num_mat.find()){
							numitems++;
							maxnum++;
							maxnum=adjust_maxnum(maxnum,num_mat.group(1));								// 索引番号の欠番への対応
						}
						Nutritions nutritions=get_nutritions(strs);
						String group_id=strs[0];
						String id=strs[1];
						String index=strs[2];
						String name=strs[3];
						nutritions.set_id(group_id,id,index,name);
						
						NutritionIdMap.put(id,nutritions);
						NutritionIndexMap.put(index,nutritions);
					}
				}
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void getnutrition_list(BufferedReader in, String line){		//	成分のリストをファイルに出力。デバッグ用
		try {
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter("nutrition_list.txt")));
			String[] strs5=line.split("\t");					//	5行目の内容
			line=in.readLine();
			String[] strs6=line.split("\t");
			String[] nutrition=new String[strs6.length];
			
			for (int i=0; i<strs6.length; i++){
				if (strs6[i].length()>0){
					nutrition[i]=strs6[i];					
				}
				else {
					nutrition[i]=strs5[i];
				}
				out.println(i+"\t"+nutrition[i]);
//				System.out.println(i+"\t"+nutrition[i]);
			}
			out.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
	}	
	int adjust_maxnum(int maxnum, String str){
		int num=Integer.parseInt(str);
		if (num!=maxnum){
			while(num>maxnum){
				maxnum++;
				missing_nums.add(new Integer(maxnum));
			}
		}
		return maxnum;
	}
	Nutritions get_nutritions(String[] strs){
		Nutritions nutritions=new Nutritions();
		LinkedHashMap<String, Double> minerals=new LinkedHashMap<String, Double>();
		LinkedHashMap<String, Double> vitamins=new LinkedHashMap<String, Double>();
	
		try {													//	フィールドのインデックスはハードコードであることに注意
			double calorie=get_number(strs[5]);					//	カロリー
			double carbohydrate=get_number(strs[16]);			//	炭水化物
			double protein=get_number(strs[8]);					//	たんぱく質
			double fat=get_number(strs[10]);					//	脂質
			double salt=get_number(strs[56]);					//	食塩相当量
			double fiber=get_number(strs[20]);					//	食物繊維総量
			double water=get_number(strs[7]);					//	水分
			double utilization=1.0-get_number(strs[4])/100.0;	//	利用率。廃棄率から計算
			
			nutritions.setMajorNutritions(calorie, carbohydrate, protein, fat, salt, fiber, water, utilization);
			
			minerals.put("Na",get_number(strs[22]));			//	ミネラルをマップに入力
			minerals.put("K",get_number(strs[23]));
			minerals.put("Ca",get_number(strs[24]));
			minerals.put("Mg",get_number(strs[25]));
			minerals.put("P",get_number(strs[26]));
			minerals.put("Fe",get_number(strs[27]));
			minerals.put("Zn",get_number(strs[28]));
			minerals.put("Cu",get_number(strs[29]));
			minerals.put("Mn",get_number(strs[30]));
			
			nutritions.setMinerals(minerals);
			
			vitamins.put("VA",get_number(strs[40])/1000.0);		//	ビタミンA,B12,C,D,Kはug単位
			vitamins.put("VB1",get_number(strs[47]));
			vitamins.put("VB2",get_number(strs[48]));
			vitamins.put("VB3",get_number(strs[49]));
			vitamins.put("VB6",get_number(strs[50]));
			vitamins.put("VB12",get_number(strs[51])/1000.0);
			vitamins.put("VC",get_number(strs[55]));
			vitamins.put("VD",get_number(strs[41])/1000.0);
			vitamins.put("VK",get_number(strs[46])/1000.0);
			
			nutritions.setVitamins(vitamins);
		}
		catch(Exception e){
			System.out.println("number error id= "+strs[1]+" "+strs[3]);
		}
		return nutritions;
	}
	double get_utilization(String str) throws Exception {			//	量ごとにフィールドのフォーマットが異なっている。
		return 1.0-get_number(str)/100.0;							//	%表示の廃棄率から1で正規化された利用率を求めて返す	
	}
	double get_number(String str) throws Exception {
		double val=0.0;
		Matcher mat=pat.matcher(str);								//	マッチしないレコードの値は0でよい
		if (mat.find()){
			val=Double.parseDouble(mat.group(1));
		}
		return val;		
	}
	public LinkedHashMap<String, Nutritions> getNutritionIdMap(){ return NutritionIdMap; } 
	LinkedHashMap<String, Nutritions> getNutritionIndexMap(){ return NutritionIndexMap; } 
}
