package nutrition;

import java.util.*;
import java.util.regex.*;

public class NutritionParserUtil {
	NutritionParserUtil(){
	}
	Nutritions parse_nutrition(String name, String str){
		Nutritions nutritions=new Nutritions(name);
		double weight=100.0;							//	デフォルト重量100gを設定
		boolean weight_f=false;
		Pattern wpat=Pattern.compile("\\(([0-9]+)g\\)");
		Matcher wmat=wpat.matcher(str);
		if (wmat.find()){
			weight=get_number(wmat.group(1));			//	重量指定を取り出す
			weight_f=true;
		}
		String[] strs=str.split("/");
		if (strs.length>=5){							//	直接入力の書式に合わない
			double calorie=get_number(strs[0]);
			double carbohydrate=get_number(strs[1]);
			double protein=get_number(strs[2]);
			double fat=get_number(strs[3]);
			double salt=get_number(strs[4]);
			double fiber=0.0;
			if (strs.length==6) fiber=get_number(strs[5]); 
			
			PFC pfc=new PFC(calorie,carbohydrate,protein,fat,fiber);	//	糖質値、食物繊維についてデータを補完する
			pfc.substitute();											//	脂質の値を推定する
		
			nutritions.copyPFC(pfc);									//	３大栄養素の値のコピー。Nutritionsの親クラスで処理されている
			nutritions.setSalt(salt);
			nutritions.set_weight(weight, weight_f);					//	加工食品の与えられた重量をそのまま使うときのために保存する

			nutritions.normalize_nutritions(weight);					//	100gあたりの値に直す
		}
		else {
			System.out.println(name+" 栄養情報読み込みエラー");
		}
		return nutritions;
	}
	double get_number(String str){
		double val=0.0;
		Pattern npat=Pattern.compile("([0-9]+\\.{0,1}[0-9]*)");
		Matcher nmat=npat.matcher(str);
		if (nmat.find()){
			val=Double.parseDouble(nmat.group(1));
		}
		return val;
	}
	String[] split(String line){							//	タブで分解する。空レコードを詰める
		if (line.length()==0) return null;					//	空行を無視
		if (line.substring(0,1).equals(":")) return null;	//	コメント行を無視
		String[] strs=line.split("\t");
		LinkedList<String> list=new LinkedList<String>();
		for(String str: strs){
			if (str.length()>0) list.add(str);
		}
		String[] result=new String[list.size()];
		int i=0;
		for(String str: list) result[i++]=str;
		return result;
	}
	String[] parse(String line){
		String[] result=split(line);
		if (result==null) return null;
		if (result.length<7) return null;
		return result;
	}
}
