package nutrition;

import java.util.*;
import java.io.*;

public class SFoodProducts extends NutritionParserUtil {
	String filename="simplefoodproducts.txt";
	SFoodProducts(){
	}
	LinkedHashMap<String, Nutritions>read(){
		LinkedHashMap<String, Nutritions> simplefoodproducts=new LinkedHashMap<String, Nutritions>();
		try {
			BufferedReader in=new BufferedReader(new FileReader(filename));
			while(true){
				String line=in.readLine();
				if (line==null) break;
				String[] strs=split(line);			//	テキストをタブで分解する
				if (strs==null) continue;
				String name=strs[0];
				Nutritions nutritions=parse_nutrition(name,strs[1]);
				simplefoodproducts.put(name,nutritions);
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
		return simplefoodproducts;
	}
}
