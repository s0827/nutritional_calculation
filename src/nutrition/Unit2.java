package nutrition;

import java.io.*;			//	単位の変換
import java.util.*;			//	Unitの改良版だが、処理の干渉を防ぐために新しく作り直すことにする
import java.util.regex.*;
import java.text.*;

public class Unit2 {													//	入力ファイルのパス名はUnitReaderに内蔵する	
	LinkedHashMap<String, LinkedHashMap<String, Double>> unit_map;		//	単位マップ
	LinkedHashMap<String, Double> specific_gravity_map;					//	比重マップ、容量から重量を換算するために使う
	LinkedHashMap<String, Double> measure_map;							//	計量マップ、計量器具の容量

	String mpstr;
	
	public Unit2() {
		UnitReader reader=new UnitReader();
		unit_map=reader.getUnit_map();
		specific_gravity_map=reader.getSpecific_gravity_map();
		measure_map=reader.getMeasure_map();
	
		mpstr="";
		Iterator<String> it=measure_map.keySet().iterator();
		while(it.hasNext()) {
			mpstr+=it.next();
			if (it.hasNext()) mpstr+="|";
		}
	}
	public double get_weight(String[] strs, double weight, boolean weight_f) {
		Pattern pat=Pattern.compile("([\\-]{0,1}[0-9/\\.]+)([g]{0,1})");
		Matcher mat=pat.matcher(strs[1]);
		double w=0.0;
		if (mat.find()) {
			if (mat.group(2).equals("g")) {
				w=Double.parseDouble(mat.group(1));
			}
			else {
				if (!weight_f) {									//	1個あたりの重量が指定されていないのに個数で計算しようとした時に警告を出す
					System.out.println(strs[0]+" 1個あたりの重量が指定されていない(100gとして計算)");	
				}
				w=weight*process_number(mat.group(1));				//	数字部分を取り出す。単位部分はなんでも良い				
			}
		}
		return w;
	}
	public double get_weight(String item, String rec) {
		return get_weight(item,rec,true);							//	以前との互換性
	}
	public double get_weight(String item, String rec, boolean weight_f) {
		double weight=0.0;
		double sign=1.0;
		Pattern npat=Pattern.compile("(なし|無)");					//	なしの処理
		Matcher nmat=npat.matcher(rec);
		if (nmat.find()) return 0.0;

		Pattern pat=Pattern.compile("("+mpstr+"){0,1}([\\-]{0,1})([0-9/\\.]+)(.*)");
		Matcher mat=pat.matcher(rec);
		if (mat.find()) {
			if (mat.group(2)!=null) {
				if (mat.group(2).equals("-")) sign=-1.0;			//	マイナス符号が付いている時
			}
			double w=process_number(mat.group(3));
			if (mat.group(4).toLowerCase().equals("g")) {			//	重量がgで直接表されている時
				weight=w;
			}
			else if (mat.group(4).toLowerCase().equals("cc")) {		//	容積がccで表された時
				weight=get_specific_gravity(item)*w;
			}
			else {
				if (!weight_f) {									//	1個あたりの重量が指定されていないのに個数で計算しようとした時に警告を出す
					System.out.println(item+" 1個あたりの重量が指定されていない(100gとして計算)");	
				}
				if (measure_map.containsKey(mat.group(1))) {			//	器具で計量される材料
					double specific_gravity=get_specific_gravity(item);	//	比重の換算
					weight=measure_map.get(mat.group(1)).doubleValue()*specific_gravity*w;
				}					
				else {
					if (unit_map.containsKey(item)) {					//	野菜などそれぞれの単位で測られるもの
						LinkedHashMap<String, Double>umap=unit_map.get(item);
						double unit=0.0;								//	単位指定の不正の時はその材料の量を0とする
						Double U=umap.get(mat.group(4));
						if (U!=null) {
							unit=U.doubleValue();
						}
						else {
							System.out.println(item+" "+mat.group(4)+" 指定された単位がない");							
						}
						weight=unit*w;
					}
				}
			}
//			System.out.println(mat.group(1)+" "+new DecimalFormat("0.00").format(w)+" "+mat.group(3));
		}
		else {
			System.out.println(item+" "+rec+" 重量の認識に失敗");
		}
//		System.out.println(item+" "+new DecimalFormat("0.00").format(weight)+"g");
		return weight*sign;
	}
	double get_specific_gravity(String item) {
		double specific_gravity=1.0;
		Double SG=specific_gravity_map.get(item);
		if (SG!=null) {
			specific_gravity=SG.doubleValue();
		}
		else {
			System.out.println(item+" 比重データがない");
		}
		return specific_gravity;
	}
	double process_number(String str) {				//	分数を実数に直す
		double d=0.0;
		Pattern pat=Pattern.compile("([0-9]+)/([0-9]+)");
		Matcher mat=pat.matcher(str);
		if (mat.find()) {
			d=Double.parseDouble(mat.group(1))/Double.parseDouble(mat.group(2));
		}
		else {
			d=Double.parseDouble(str);
		}
		return d;
	}
	public static void main(String args[]) {							//	単独テストのためのコード
		Unit2 unit2=new Unit2();
		unit2.test();
	}
	void test() {
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			while(true) {
				System.out.print("> ");
				String line=in.readLine();
				String[] strs=line.split(" ");
				get_weight(strs[0],strs[1]);
			}
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
}
