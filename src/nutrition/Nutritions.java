package nutrition;

import java.text.DecimalFormat;
import java.util.*;

public class Nutritions extends MajorNutritions {			//	三大栄養素はMajorNutritionsに保存される
	LinkedHashMap<String, Double> minerals;					//	値がnullのものは指定されていないとみなす。0とnullで区別される
	LinkedHashMap<String, Double> vitamins;
	double quantity,weight;
	boolean weight_f=false;									//	個あたりの重量が定義されていればtrue
	boolean suggest_item=false;
	double price;
	String category;
	public Nutritions(){
		initialize();
	}
	public Nutritions(Nutritions N){						//	コピーコンストラクタ
		super(N);
		this.quantity=N.quantity;
		this.weight=N.weight;
		this.weight_f=N.weight_f;
		this.suggest_item=N.suggest_item;
		this.price=N.price;
		minerals=copy(N.minerals);
		vitamins=copy(N.vitamins);
	}
	Nutritions(String name){
		super(name);
		initialize();
	}
	public Nutritions(double calorie, double carbohydrate, double protein, double fat, double salt, double fiber){
		super(calorie,carbohydrate,protein,fat,salt,fiber);
	}
	public Nutritions(MenuData D){
		super(D);
		this.quantity=D.quantity;
		minerals=copy(D.minerals);
		vitamins=copy(D.vitamins);
	}
	void copy(Nutritions N){
		minerals.putAll(N.minerals);						//	すべての要素をコピー
		vitamins.putAll(N.vitamins);
		copyMajorNutritions(N);								//	親クラスMajorNutritionsの内容のコピー処理。id、コメント等はコピーしない
	}
	LinkedHashMap<String, Double> copy(LinkedHashMap<String, Double> src){
		LinkedHashMap<String, Double> dst=new LinkedHashMap<String, Double>();
		src.forEach((K,V) -> {
			double value=V.doubleValue();
			dst.put(K,new Double(value));	
		} );
		return dst;
	}
	void initialize(Nutritions n){
		initialize();
		initialize_keys(n);
	}
	void initialize(){
		minerals=new LinkedHashMap<String, Double>();
		vitamins=new LinkedHashMap<String, Double>();
	}
	void initialize_keys(Nutritions n){
		LinkedHashMap<String, Double> m=n.getMinerals();
		m.forEach((K,V) -> {
			minerals.put(K,new Double(0.0));
		});
		LinkedHashMap<String, Double> v=n.getVitamins();
		v.forEach((K,V) -> {
			vitamins.put(K,new Double(0.0));
		});
	}
	void setBrand(String brand) { this.brand=brand; }
	public void calculate(Nutritions N, double quantity){
		this.quantity=quantity;
		calculate(minerals,N.getMinerals(),quantity);		//	ミネラルの量を計算
		calculate(vitamins,N.getVitamins(),quantity);		//	ビタミンの量を計算
		calculate_major_nutritions(N,quantity);				//	主要栄養素を親クラスで計算
	}
	public void mult(double mult){
		mult(minerals,mult);
		mult(vitamins,mult);
		mult_major_nutritions(mult);
	}
	void calculate(LinkedHashMap<String, Double> dst, LinkedHashMap<String, Double> src, double quantity){	//	dstはこのクラス。srcは食品成分表の100gあたりの基準値
		src.forEach((K,V) -> {
			double val=quantity*V.doubleValue()/100.0;	//	この中で100g単位に換算
			dst.put(K,new Double(val));
		});
	}
	void mult(LinkedHashMap<String, Double> map, double quantity){
		map.forEach((K,V) -> {
			double val=quantity*V.doubleValue();
			map.put(K,new Double(val));
		});
	}
	public void add_nutritions(Nutritions N){
		minerals.forEach((K,V) -> {
			Double X=N.minerals.get(K);
			if (X!=null){
				double val=V.doubleValue()+X.doubleValue();
				minerals.put(K,new Double (val));				
			}
		});
		vitamins.forEach((K,V) -> {
			Double X=N.vitamins.get(K);
			if (X!=null){
				double val=V.doubleValue()+X.doubleValue();
				vitamins.put(K,new Double (val));
			}
		});
		add_major_nutritions(N);
	}
	public void add_price(Nutritions N) {			//	Nutritionsのレベルで加算すべきもの
		price+=N.price;
	}
	void mult(Nutritions N){		
	}
	public String get_idstr() {
		return 	id+" "+name;
	}
	public void output(String key){
		System.out.println(key+" "+id+" "+name);
	}
	public void output_major_nutritions(String str){
		output_major_nutritions(str,0,1);
	}
	public void output_major_nutritions(int width){
		output_major_nutritions(name,0,1);
	}
	void output_major_nutritions(String keyword, int ilevel, int width, boolean ratio_f, Nutritions standard_nutrition){
		output_major_nutritions(keyword,ilevel,width);			//	主要栄養素については標準値が定義されていない（今後の課題）
	}
	void output_major_nutritions(String keyword, int ilevel, int width){
		output_major_nutritions(keyword,ilevel,width,null);
	}
	void output_major_nutritions(String keyword, int ilevel, int width, LinkedList<MenuData> menuitems){
		for (int i=0; i<ilevel; i++) System.out.print("　");	
		if (width==0){
			System.out.println(keyword
					+" "+new DecimalFormat("0.0").format(calorie)+"kcal"
					+"/"+new DecimalFormat("0.0").format(carbohydrate)+"g"
					+"/"+new DecimalFormat("0.0").format(protein)+"g"
					+"/"+new DecimalFormat("0.0").format(fat)+"g"
					+"/"+new DecimalFormat("0.0").format(salt)+"g"	
					+"/"+new DecimalFormat("0.0").format(fiber)+"g"
						);
		}			
		else {
			System.out.print(keyword);
			if (menuitems==null) System.out.print(" ("+new DecimalFormat("0.0").format(quantity)+"g)");
			System.out.println(
				" エネルギー "+new DecimalFormat("0.0").format(calorie)+"kcal"
				+" 糖質 "+new DecimalFormat("0.0").format(carbohydrate)+"g"
				+" たんぱく質 "+new DecimalFormat("0.0").format(protein)+"g"
				+" 脂質 "+new DecimalFormat("0.0").format(fat)+"g"
				+" 食塩 "+new DecimalFormat("0.0").format(salt)+"g"
				+" 食物繊維 "+new DecimalFormat("0.0").format(fiber)+"g"
				); 
		}
	}
	public void output_mv(int ilevel, boolean ratio_f, Nutritions standard_nutrition) {
		if (ratio_f) {
			if (minerals!=null) {
				for (int i=0; i<ilevel; i++) System.out.print("　");	
				System.out.print("　　ミネラル");
				output_mv(minerals,ilevel,standard_nutrition.getMinerals());				
			}
			if (vitamins!=null) {
				for (int i=0; i<ilevel; i++) System.out.print("　");	
				System.out.print("　　ビタミン");
				output_mv(vitamins,ilevel,standard_nutrition.getVitamins());	
			}			
		}
		else {
			output_mv(ilevel);
		}
	}
	public void output_mv(int ilevel){			//	ミネラル、ビタミンを出力する
		for (int i=0; i<ilevel; i++) System.out.print("　");	
		System.out.print("　　ミネラル");
		output_mv(minerals,ilevel);
		
		for (int i=0; i<ilevel; i++) System.out.print("　");	
		System.out.print("　　ビタミン");
		output_mv(vitamins,ilevel);
	}
	public void output_mv(LinkedHashMap<String, Double> map, int ilevel, LinkedHashMap<String, Double> stdmap){
		map.forEach((K,V) -> {
			Double S=stdmap.get(K);
			if (S!=null) {
				double ratio=V.doubleValue()/S.doubleValue()*100.0;	
				System.out.print(" "+K+" "+new DecimalFormat("0.0").format(ratio)+"%");
			}
		});
		System.out.println();
	}
	public void output_mv(LinkedHashMap<String, Double> map, int ilevel){
		map.forEach((K,V) -> {
			System.out.print(" "+K+" "+new DecimalFormat("0.00").format(V.doubleValue()));
		});
		System.out.println();
	}
	void setWeight(double weight){ this.weight=weight; }
	void set_weight(double weight, boolean weight_f) {
		this.weight=weight;
		this.weight_f=weight_f;
	}
	void setMinerals(LinkedHashMap<String, Double> minerals){ this.minerals=minerals; }
	void setVitamins(LinkedHashMap<String, Double> vitamins){ this.vitamins=vitamins; }
	
	public void setSuggest_item(boolean suggest_item) { this.suggest_item=suggest_item; }
	public void setPrice(double price) { this.price=price; }
	public void setCategory(String category) { this.category=category; }
	public boolean isSuggest_item() { return suggest_item; }
	public double getWeight(){ return weight; }
	public boolean getWeight_f() { return weight_f; }
	public double getPrice() { return price; }
	public String getCategory() { return category; }
	public LinkedHashMap<String, Double> getMinerals(){ return minerals; }
	public LinkedHashMap<String, Double> getVitamins(){ return vitamins; }
	public LinkedHashSet<String> get_mkeys(){ 
		LinkedHashSet<String> keys=new LinkedHashSet<String>();
		minerals.forEach((K,V) -> keys.add(K));
		return keys;
	}
	public LinkedHashSet<String> get_vkeys(){ 
		LinkedHashSet<String> keys=new LinkedHashSet<String>();
		vitamins.forEach((K,V) -> keys.add(K));
		return keys;
	}
}

