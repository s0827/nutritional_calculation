package nutrition;

import java.io.*;
import java.util.*;

import javax.xml.parsers.*;

import org.w3c.dom.*;

public class FoodProductsXML {			//	加工食品、外食の情報の取得
	public FoodProductsXML(){;			//	すべてのデータがないとき食品成分表のデータで補う。最初にこのデータを設定、XMLの内容で上書きする
	}
	public LinkedHashMap<String, Nutritions>read(String brand, String xmlfile, LinkedHashMap<String, Nutritions> nutrition_table, 
			LinkedHashMap<String, String> IDmap, LinkedHashSet<String> mkeys, LinkedHashSet<String> vkeys){
		LinkedHashMap<String, Nutritions> foodproducts=read(xmlfile, nutrition_table, IDmap, mkeys, vkeys);
		foodproducts.forEach((K,V) -> V.setBrand(brand));
		return foodproducts;
	}
	public LinkedHashMap<String, Nutritions>read(String xmlfile, LinkedHashMap<String, Nutritions> nutrition_table, 
			LinkedHashMap<String, String> IDmap, LinkedHashSet<String> mkeys, LinkedHashSet<String> vkeys){
		LinkedHashMap<String, Nutritions> foodproducts=new LinkedHashMap<String, Nutritions>();
		
		try {
			InputStream is=new FileInputStream(xmlfile);
			DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
			DocumentBuilder builder=factory.newDocumentBuilder();
			Document doc=builder.parse(is);
			Element rootElement=doc.getDocumentElement();
			
			NodeList itemlist=rootElement.getElementsByTagName("item");
			for (int i=0; i<itemlist.getLength(); i++){
				double weight=100.0;
				boolean weight_f=false;
				
				Node itemnode=itemlist.item(i);
				NamedNodeMap itemattr=itemnode.getAttributes();
				String name=itemattr.getNamedItem("keyword").getNodeValue();
				
				Nutritions nutritions=new Nutritions(name);					//	base_itemが指定されている時はデータをコピーする
				foodproducts.put(name,nutritions);
				
				Node weight_node=itemattr.getNamedItem("weight");			//	栄養成分が100g以外の重量で定義されているとき
				if (weight_node!=null) {
					weight=Double.parseDouble(weight_node.getNodeValue());
					weight_f=true;
				}
				
				Node base_node=itemattr.getNamedItem("base");
				if (base_node!=null){
					String base=base_node.getNodeValue();
					String id=IDmap.get(base);
					if (id!=null){						
						Nutritions item=nutrition_table.get(id);
						nutritions.copy(item);								//	デフォルト値としてコピーする
						if (weight!=0.0) nutritions.mult(weight/100.0);		//	重量が100gではないときの調整。Major Nutritionはまだ読み込んでいないので影響ない
					}
				}

				double calorie=get_attr(itemattr,"calorie");
				double carbohydrate=get_attr(itemattr,"carbohydrate");
				double protein=get_attr(itemattr,"protein");
				double fat=get_attr(itemattr,"fat");
				double salt=get_attr(itemattr,"salt");
				double fiber=get_attr(itemattr,"fiber");
				boolean suggest_item=get_flag(itemattr,"suggest_item");
				double price=get_attr(itemattr,"price");
				String category=get_str_attr(itemattr,"category");
				
				PFC pfc=new PFC(calorie,carbohydrate,protein,fat,fiber);	//	糖質値、食物繊維についてデータを補完する
				pfc.substitute();
			
				nutritions.copyPFC(pfc);									//	３大栄養素の値のコピー。Nutritionsの親クラスで処理されている
				nutritions.setSalt(salt);
				nutritions.set_weight(weight,weight_f);						//	加工食品の与えられた重量をそのまま使うときのために保存する
				nutritions.setSuggest_item(suggest_item);
				nutritions.setPrice(price);
				nutritions.setCategory(category);
				
				LinkedHashMap<String, Double> minerals=nutritions.getMinerals();
				put_values(itemattr,minerals,mkeys);
				LinkedHashMap<String, Double> vitamins=nutritions.getVitamins();
				put_values(itemattr,vitamins,vkeys);
				
				if (salt==0.0){												//	塩分をNaから計算する
					Double Na=vitamins.get("Na");
					if (Na!=null) salt=Na.doubleValue()*2.5;				//	Na 1gが食塩2.5gに対応
				}
				
				if (weight!=0.0) {
					nutritions.normalize_nutritions(weight);				//	100gあたりの値に直す
					normalize(nutritions.getMinerals(),weight);
					normalize(nutritions.getVitamins(),weight);
				}
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
		return foodproducts;
	}
	void put_values(NamedNodeMap itemattr, LinkedHashMap<String, Double> map, LinkedHashSet<String> keys){
		keys.forEach((K) -> {
			Node node=itemattr.getNamedItem(K);								//	各々の栄養素がXMLで定義されているか調べ、定義されていればマップにロードする
			if (node!=null) {
				double val=Double.parseDouble(node.getNodeValue());
				map.put(K,new Double(val));
			}
		});
	}
	void normalize(LinkedHashMap<String, Double> map, double weight){
		map.forEach((K,V) -> {
			double x=V.doubleValue()*100.0/weight;
			map.put(K,new Double(x));
		});
	}
	String get_str_attr(NamedNodeMap attr, String str) {
		String val=null;
		Node node=attr.getNamedItem(str);
		if (node!=null) val=node.getNodeValue();
		return val;		
	}
	double get_attr(NamedNodeMap attr, String str){							//	レコードがないときは0を返す
		double val=0.0;
		Node node=attr.getNamedItem(str);
		if (node!=null) {
			val=Double.parseDouble(node.getNodeValue());
		}
		return val;
	}
	boolean get_flag(NamedNodeMap attr, String str) {						//	レコードがないときはfalseを返す
		boolean flag=false;
		Node node=attr.getNamedItem(str);
		if (node!=null) {
			String val=node.getNodeValue();
			if (val.toLowerCase().equals("true")) flag=true;
		}
		return flag;
	}
}
