package nutrition;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UnitReader {
	String unit_file="unit.txt";
	String specific_gravity_file="specific_gravity.txt";
	String measure_file="measure.txt";

	LinkedHashMap<String, LinkedHashMap<String, Double>> unit_map;		//	単位マップ
	LinkedHashMap<String, Double> specific_gravity_map;					//	比重マップ、容量から重量を換算するために使う
	LinkedHashMap<String, Double> measure_map;							//	計量マップ、計量器具の容量

	UnitReader(){
		unit_map=new LinkedHashMap<String, LinkedHashMap<String, Double>>();
		specific_gravity_map=new LinkedHashMap<String, Double>();
		measure_map=new LinkedHashMap<String, Double>();
		
		read_unit();
		read_specific_gravity();
		read_measure();
	}
	void read_unit(){
		Pattern upat=Pattern.compile("[0-9\\.]+(.+)");
		Pattern gpat=Pattern.compile("([0-9\\.]+)g");
		try {
			BufferedReader in=new BufferedReader(new FileReader(unit_file));
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.substring(0,1).equals(":")) continue;	//	コメント行
				String[] strs=line.split("[\t ]+");
				String key=strs[0];
				
				Matcher umat=upat.matcher(strs[1]);
				Matcher gmat=gpat.matcher(strs[2]);
				
				if (umat.find() && gmat.find()){
					String unit=umat.group(1);
					double weight=Double.parseDouble(gmat.group(1));
					
					LinkedHashMap<String, Double> U=unit_map.get(key);
					if (U==null){
						U=new LinkedHashMap<String, Double>();
						unit_map.put(key,U);
					}
					U.put(unit,new Double(weight));
				}
			}
			in.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void read_specific_gravity() {								//	比重と計量のデータファイルは形式が同じ
		specific_gravity_map=read_map(specific_gravity_file);
	}
	void read_measure() {
		measure_map=read_map(measure_file);
	}
	LinkedHashMap<String, Double> read_map(String file){
		LinkedHashMap<String, Double> map=new LinkedHashMap<String, Double>();
		try {
			BufferedReader in=new BufferedReader(new FileReader(file));
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.substring(0,1).equals(":")) continue;	//	コメント行			
				String[] strs=line.split("[\t ]+");
//				System.out.println(line);
				map.put(strs[0],new Double(strs[1]));
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return map;
	}
	LinkedHashMap<String, LinkedHashMap<String, Double>> getUnit_map(){ return unit_map; }
	LinkedHashMap<String, Double> getSpecific_gravity_map(){ return specific_gravity_map; }
	LinkedHashMap<String, Double> getMeasure_map(){ return measure_map; }
}
