package suggestion;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.text.*;

import nutrition.*;
import nuvis.*;

public class MenuSuggest extends Frame {					//	カロリー、糖質量の目標値に近いメニューの組み合わせを検索する
	NuvisXTnutrition_reader reader;							//	栄養データの読み込み処理
	double target_cal=550.0;								//	カロリー、糖質量の目標値
	double target_carb=25.0;
	double fact=target_cal/target_carb;
	double range=0.05;										//	糖質の重みがratio倍大きいとする。
	double min_cal,max_cal,min_carb,max_carb;
	double pf_min=0.0,pf_max=5.0;							//	たんぱく質と脂質の比
	double pr_min=0.0,pr_max=2000.0;						//	値段の最小最大値

	int num=20,disp_mode=1;									//	表示データ点数、モード切り替え
	int max_depth=3;

	int offset=20;
	int framewidth=550, frameheight=560+offset;
	int panelwidth=500, panelheight=500;
	MenuSuggestPanel panel;
	
	LinkedHashMap<String, LinkedHashMap<String, Nutritions>> brand_tables;
	Nutritions[] array;										//	参照用の栄養情報を配列に取得する

	public static void main(String[] args) {
		MenuSuggest ms=new MenuSuggest();
	}
	MenuSuggest(){
		initgraphics();
		set_range();																				//	検索、表示の範囲、重みの設定
		reader=new NuvisXTnutrition_reader();														//	栄養データ読み込み
		brand_tables=reader.getBrand_tables();
		LinkedHashMap<String, Nutritions> brand_table=brand_tables.get("大戸屋");
		LinkedHashMap<String, String> rcat_table=get_rcat_table(brand_table);						//	品名からカテゴリを引くテーブルを生成
		LinkedHashMap<String, LinkedList<String>> category_table=get_category_table(rcat_table);
		LinkedHashMap<String, Nutritions>table=get_valid_items(brand_table);						//	有効な要素を抽出
		array=get_array(table);
		TreeMap<Double, Nutritions> menus=get_menus(array,new Nutritions(),category_table,false);	//	初期出力
		panel.setMenus(menus);
		output(menus);
		repaint();
		process(menus,category_table);																				//	コマンド処理
	}
	void process(TreeMap<Double, Nutritions> menus,LinkedHashMap<String, LinkedList<String>> category_table) {
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			while(true) {
				boolean output_f=false;
				boolean calc_f=false;
				boolean breakfast_f=false;
				System.out.println("T :目標値 R :範囲 F :重み P :pf比 V :価格 B: 朝食指定 N :表示点数 S :データ点表示切替 Q :終了");
				System.out.print("> ");
				String line=in.readLine();
				StringTokenizer tokens=new StringTokenizer(line," ");
				if (tokens.hasMoreTokens()) {
					String C=tokens.nextToken().toUpperCase();
					if (C.equals("D")) {
						String brand=tokens.nextToken();
						LinkedHashMap<String, Nutritions> brand_table=brand_tables.get(brand);
						if (brand_table!=null) {
							LinkedHashMap<String, Nutritions>table=get_valid_items(brand_table);	//	有効な要素を抽出
							array=get_array(table);
							menus=get_menus(array,new Nutritions(),category_table,breakfast_f);		//	初期出力
							output_f=true;
						}
					}
					else if (C.equals("R")) {
						range=Double.parseDouble(tokens.nextToken());
						set_range();
						calc_f=true;
						output_f=true;
					}
					else if (C.equals("F")) {
						fact=Double.parseDouble(tokens.nextToken());
						set_range();
						calc_f=true;
						output_f=true;
					}
					else if (C.equals("P")) {
						if (tokens.hasMoreTokens()) {
							pf_min=Double.parseDouble(tokens.nextToken());
							pf_max=Double.parseDouble(tokens.nextToken());
							panel.set_pf(pf_min,pf_max);
							panel.set_flags(true,false);
						}
						else {
							panel.toggle_pf();							
						}
						panel.repaint();
					}
					else if (C.equals("V")) {
						if (tokens.hasMoreTokens()) {
							pr_min=Double.parseDouble(tokens.nextToken());
							pr_max=Double.parseDouble(tokens.nextToken());
							panel.set_pr(pr_min,pr_max);
							panel.set_flags(false,true);
						}
						else {
							panel.toggle_pr();							
						}
						panel.repaint();
					}
					else if (C.equals("B")) {
						breakfast_f=toggle(breakfast_f);
						calc_f=true;
						output_f=true;
					}
					else if (C.equals("N")) {
						num=Integer.parseInt(tokens.nextToken());
						panel.setNum(num);
						calc_f=true;
						output_f=true;
					}
					else if (C.equals("S")) {
						disp_mode++;
						if (disp_mode>=5) disp_mode=0;
						panel.setDisp_mode(disp_mode);
						panel.repaint();
					}
					else if (C.equals("T")) {
						target_carb=Double.parseDouble(tokens.nextToken());
						target_cal=Double.parseDouble(tokens.nextToken());
						set_range();
						calc_f=true;
						output_f=true;
					}
					else if (C.equals("Q")) {
						System.exit(0);
					}
				}
				if (calc_f) menus=get_menus(array,new Nutritions(),category_table,breakfast_f);
				if (output_f) {
					panel.setMenus(menus);
					output(menus);
					repaint();
				}
			}			
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void output(TreeMap<Double, Nutritions> menus) {
		Iterator<Double> it=menus.keySet().iterator();
		int i=0;
		while(it.hasNext()) {
			if (i>=num) break;
			i++;
			Nutritions N=menus.get(it.next());
			System.out.println(i+"> "+N.getName()+
					" 糖質"+new DecimalFormat("0.0").format(N.getCarbohydrate())+"g"+
					" カロリー"+new DecimalFormat("0.0").format(N.getCalorie())+"kcal"+
					" P/F比"+new DecimalFormat("0.00").format(N.getProtein()/N.getFat())+
					" "+new DecimalFormat("0").format(N.getPrice())+"円");
		}
	}
	boolean toggle(boolean inflag) {
		boolean outflag=false;
		if (!inflag) outflag=true;
		return outflag;
	}
	void set_range() {
		min_cal=target_cal*(1.0-range);
		max_cal=target_cal*(1.0+range);
		min_carb=target_carb*(1.0-range);
		max_carb=target_carb*(1.0+range);
		fact=target_cal/target_carb;
		panel.set_limits(min_cal,min_carb,max_cal,max_carb);
		panel.setNum(num);
	}
	LinkedHashMap<String, String> get_rcat_table(LinkedHashMap<String, Nutritions> brand_table){
		LinkedHashMap<String, String> rcat_table=new LinkedHashMap<String, String>();
		brand_table.forEach((K,V) -> {
			String name=V.getName();
			String category=V.getCategory();
			if (category!=null) rcat_table.put(name, category);
		});
		return rcat_table;
	}
	LinkedHashMap<String, LinkedList<String>> get_category_table(LinkedHashMap<String, String> rcat_table){
		LinkedHashMap<String, LinkedList<String>> category_table=new LinkedHashMap<String, LinkedList<String>>();
		rcat_table.forEach((K,V) -> {
			String name=K;
			String category=V;
			LinkedList<String> list=category_table.get(V);
			if (list==null) {
				list=new LinkedList<String>();
				category_table.put(category, list);
			}
			list.add(K);
		});
		return category_table;
	}
	LinkedHashMap<String, Nutritions> get_valid_items(LinkedHashMap<String, Nutritions> in_table) {
		LinkedHashMap<String, Nutritions> out_table=new LinkedHashMap<String, Nutritions>();
		in_table.forEach((K,V) -> {
			if (V.isSuggest_item()) out_table.put(K,V);
		});
		return out_table;
	}
	Nutritions[] get_array(LinkedHashMap<String, Nutritions>table) {
		Nutritions[] array=new Nutritions[table.size()];
		int i=0;
		Iterator<String> it=table.keySet().iterator();
		while(it.hasNext()) array[i++]=table.get(it.next());
		return array;
	}
	Nutritions[] get_sub_array(int i, Nutritions[] array) {
		int length=array.length-i-1;
		if (length<=0) return null;
		Nutritions[] sub_array=new Nutritions[length];
		for (int j=0; j<length; j++) sub_array[j]=array[i+1+j];
		return sub_array;
	}
	TreeMap<Double, Nutritions> get_menus(Nutritions[] array, Nutritions N,
			LinkedHashMap<String, LinkedList<String>> category_table, boolean breakfast_f){
		return get_menus(1,array,N,category_table, breakfast_f);
	}
	TreeMap<Double, Nutritions> get_menus(int din, Nutritions[] array, Nutritions N,
			LinkedHashMap<String, LinkedList<String>> category_table, boolean breakfast_f){
		int depth=din+1;
		TreeMap<Double, Nutritions> menus=new TreeMap<Double, Nutritions>();
		if (din>max_depth) return menus;
		for (int i=0; i<array.length; i++) {
			Nutritions A=array[i];
			Nutritions M=new Nutritions(N);
			String name=N.getName();
			if (!checkmenu(N,A,category_table,breakfast_f)) continue;		//	メニューの制約条件をチェックする
			if (name==null) { 
				name="";
			}
			else {
				name+=" ";
			}
			name+=A.getName();
			M.setName(name);
			Nutritions B=new Nutritions();
			double quantity=100.0;
			if (A.getWeight_f()) quantity=A.getWeight();
			B.calculate(A,quantity);
			M.add_nutritions(B);
			M.add_price(A);
			double cal=M.getCalorie();
			double carb=M.getCarbohydrate();
			double score=Math.sqrt(Math.pow((cal-target_cal)/fact,2.0)+Math.pow(carb-target_carb,2.0));
			Nutritions[] sub_array=get_sub_array(i,array);
			if (sub_array!=null) menus.putAll(get_menus(depth,sub_array,M,category_table,breakfast_f));
			Double S=new Double(score);
			while(menus.containsKey(S)) S=new Double(S.doubleValue()*1.0001);			//	キーの重複を回避
			menus.put(S,M);				
		}
		return menus;
	}
	boolean checkmenu(Nutritions N, Nutritions X, LinkedHashMap<String, LinkedList<String>> category_table, boolean breakfast_f) {
		LinkedList<String> drinks=category_table.get("ドリンク");							//	制約条件がここにハードコードされている
		LinkedList<String> breakfast=category_table.get("朝マック");
		LinkedList<String> burgers=category_table.get("バーガー");
		if (drinks==null || breakfast==null || burgers==null) return true;
		String menu_str=N.getName();
		String name=X.getName();
		if (menu_str!=null) {
			String[] strs=menu_str.split(" ");
			if (drinks.contains(name)) {
				for(String str: strs) {
					if (drinks.contains(str)) return false;		//	ドリンクの重複を回避
				}
			}
		}
		if (breakfast_f) {
			if (burgers.contains(name)) return false;								//	朝食の時はバーガーを除外
		}
		else {
			if (breakfast.contains(name)) return false;								//	朝食でない時は朝マックを除外
		}
		return true;
	}
	void initgraphics() {
		setSize(framewidth, frameheight);
		setVisible(true);
		setLayout(null);
		panel=new MenuSuggestPanel(panelwidth,panelheight);
		panel.setLocation(40,offset+20);
		add(panel);
	}
	public void paint(Graphics gc) {
		if (panel!=null) panel.repaint();
		
		gc.setColor(Color.LIGHT_GRAY);
		gc.fillRect(0, 0, framewidth, frameheight);
		
		gc.setColor(Color.BLACK);
		gc.drawString("カロリー[kcal]",10,offset+18);
		gc.drawString(new DecimalFormat("0").format(max_cal),10,offset+40);
		gc.drawString(new DecimalFormat("0").format((min_cal+max_cal)/2.0),10,offset+panelheight/2+30);
		gc.drawString(new DecimalFormat("0").format(min_cal),10,offset+panelheight+20);
		
		gc.drawString("糖質[g]", panelwidth, panelheight+offset+56);
		gc.drawString(new DecimalFormat("0.0").format(min_carb), 40, panelheight+offset+36);
		gc.drawString(new DecimalFormat("0.0").format((min_carb+max_carb)/2.0), panelwidth/2+20, panelheight+offset+36);
		gc.drawString(new DecimalFormat("0.0").format(max_carb), panelwidth+10, panelheight+offset+36);	
	}
}
