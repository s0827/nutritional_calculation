package suggestion;

import java.awt.*;
import java.util.*;
import java.text.*;

import nutrition.Nutritions;

public class MenuSuggestPanel extends Panel {		//	カロリーと糖質量の2次元表示
	int panelwidth,panelheight;
	TreeMap<Double, Nutritions> menus;
	double min_cal,min_carb,max_cal,max_carb;
	double pf_min=0.0,pf_max=2.0;					//	たんぱく質と脂質の比
	double pr_min=0.0,pr_max=1500.0;				//	値段の最小最大値
	boolean pf_flag=true, pr_flag=false;			//	pf比、値段表示切り替え
	int num=500,disp_mode=1;
	
	MenuSuggestPanel(int panelwidth, int panelheight){
		this.panelwidth=panelwidth;
		this.panelheight=panelheight;
		setSize(panelwidth,panelheight);
		setVisible(true);
	}
	public void paint(Graphics gc) {
		gc.setColor(Color.WHITE);
		gc.fillRect(0, 0, panelwidth, panelheight);
		if (menus==null) return;
	
		draw_axis(gc);
		
		gc.setColor(Color.BLACK);
		Iterator<Double> it=menus.keySet().iterator();
		for (int n=0; n<num; n++) {
			if (!it.hasNext()) break;
			Double D=it.next();
			Nutritions N=menus.get(D);
			double cal=N.getCalorie();
			double carb=N.getCarbohydrate();
			double px=(carb-min_carb)/(max_carb-min_carb);
			double py=(cal-min_cal)/(max_cal-min_cal);
			int x=(int)(px*(double)panelwidth);
			int y=(int)((1.0-py)*(double)panelheight);
			if (pr_flag) {
				double ratio=(N.getPrice()-pr_min)/(pr_max-pr_min);
				if (ratio<0.0) ratio=0.0;
				if (ratio>=1.0) ratio=1.0;
				int r=127;
				int g=127;
				int b=255;
				if (ratio<0.333) {
					double d=ratio/0.333;
					r=127+(int)((1.0-d)*127.0);
					g=127+(int)(d*127.0);
					r=127+(int)((1.0-d)*127.0);
				}
				else if (ratio<0.667) {
					double d=(ratio-0.333)/0.334;
					r=127+(int)(d*127.0);
					g=255;
					b=127;
				}
				else if (ratio>=0.667){
					double d=(ratio-0.667)/0.333;
					r=255;
					g=127+(int)((1.0-d)*127.0);
					b=127;
				}
				gc.setColor(new Color(r,g,b));
				gc.fillOval(x, y, 10, 10);
				gc.setColor(Color.BLACK);
				gc.drawOval(x, y, 10, 10);
			}
			else if (pf_flag) {
				int r=127;
				double g0=127.0,g1=255.0;
				double b0=255.0,b1=127.0;
				double wratio=N.getProtein()/N.getFat();
				double ratio=(wratio-pf_min)/(pf_max-pf_min);
				if (ratio<0.0) ratio=0.0;
				if (ratio>=1.0) ratio=1.0;
				int g=(int)(g0*(1.0-ratio)+g1*ratio);
				int b=(int)(b0*(1.0-ratio)+b1*ratio);
//				System.out.println(r+" "+g+" "+b+" "+ratio);
				gc.setColor(new Color(r,g,b));
				gc.fillOval(x, y, 10, 10);
				gc.setColor(Color.BLACK);
				gc.drawOval(x, y, 10, 10);
			}
			else {
				gc.setColor(Color.BLACK);
				gc.drawOval(x, y, 10, 10);				
			}
			int m=n+1;
			String str="";
			String strm="("+new DecimalFormat("0").format(m)+")";
			switch (disp_mode) {
			case 0: break;
			case 1: str=strm; break;
			case 2: str=strm+new DecimalFormat("0.0").format(N.getCarbohydrate())+", "+new DecimalFormat("0").format(N.getCalorie());
					break;
			case 3: str=strm;
					if (pf_flag) str+=new DecimalFormat("0.00").format(N.getProtein()/N.getFat());
					else if (pr_flag) str+=new DecimalFormat("0").format(N.getPrice())+"円";
					else str+=new DecimalFormat("0.0").format(D.doubleValue());				
					break;
			case 4: 
				str=strm+N.getName();
				break;
			default: break;
			}
			gc.drawString(str, x+4, y-2);
		}
	}
	void draw_axis(Graphics gc) {
		gc.setColor(Color.LIGHT_GRAY);
		gc.fillRect(0,panelheight/2-1,panelwidth,3);
		gc.fillRect(panelwidth/2-1,0,3,panelheight);
	}
	void set_limits(double min_cal, double min_carb, double max_cal, double max_carb) {
		this.min_cal=min_cal;
		this.min_carb=min_carb;
		this.max_cal=max_cal;
		this.max_carb=max_carb;
	}
	void set_pf(double pf_min, double pf_max) {
		this.pf_min=pf_min;
		this.pf_max=pf_max;
	}
	void set_pr(double pr_min, double pr_max) {
		this.pr_min=pr_min;
		this.pr_max=pr_max;
	}
	void set_flags(boolean pf_flag,boolean pr_flag) { 
		this.pf_flag=pf_flag;
		this.pr_flag=pr_flag;
	}
	void toggle_pf() {
		if (pf_flag) {
			pf_flag=false;
		}
		else {
			pf_flag=true;
			pr_flag=false;
		}
	}
	void toggle_pr() {
		if (pr_flag) {
			pr_flag=false;
		}
		else {
			pr_flag=true;
			pf_flag=false;
		}
	}
	void setMenus(TreeMap<Double, Nutritions> menus) { this.menus=menus; }
	void setNum(int num) { this.num=num; }
	void setDisp_mode(int disp_mode) { this.disp_mode=disp_mode; }
}
