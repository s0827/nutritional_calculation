package menu;

public class DailyDietItem {						//	朝食、昼食、夕食別の栄養成分。棒グラフ表示のため積算成分を出力する
	double total=0.0, breakfast=0.0, lunch=0.0, dinner=0.0;
	double chicken=0.0, meat=0.0,  fish=0.0, dairy=0.0;
	DailyDietItem(){}
	
	void setTotal(double total){ this.total=total; }
	void setBreakfast(double breakfast){ this.breakfast=breakfast; }
	void setLunch(double lunch){ this.lunch=lunch; }
	void setDinner(double dinner){ this.dinner=dinner; }
	
	void setChicken(double chicken){ this.chicken=chicken; }
	void setMeat(double meat){ this.meat=meat; }
	void setFish(double fish){ this.fish=fish; }
	void setDairy(double dairy){ this.dairy=dairy; }
	
	double getBreakfast(){ return breakfast; }
	double getLunch(){ return lunch; }
	double getDinner(){ return dinner; }
	
	double getChicken(){ return chicken; }
	double getMeat(){ return meat; }
	double getFish(){ return fish; }
	double getDairy(){ return dairy; }
	
	double getTotal(){ return total; }
	double get_sum_breakfast(){ return breakfast; }
	double get_sum_lunch(){ return breakfast+lunch; }
	double get_sum_dinner(){ return breakfast+lunch+dinner; }

	double get_sum_chicken(){ return chicken; }
	double get_sum_meat(){ return chicken+meat; }
	double get_sum_fish(){ return chicken+meat+fish; }
	double get_sum_dairy(){ return chicken+meat+fish+dairy; }
}
