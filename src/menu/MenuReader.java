package menu;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.*;

import nutrition.*;

public class MenuReader extends Reader {								//	RecipeSearchに継承される
	MenuDate menu_date;

	String menu_dir="./menu2/";											//	このレベルでメニューファイルの所在がわかっていないといけない

	LinkedHashMap<String, String> books;
	
//	Pattern pat=Pattern.compile("(["+select_chars+"]+)(.+)\\{(.*)");
//	Pattern ipat=Pattern.compile("(["+select_chars+"]+)([^ ]+)( *([0-9]{0,6}))( *([^ ]+))( *([0-9\\.]*)).*;.*");		//	ファイル名は半角スペースで区切る
//	Pattern rpat=Pattern.compile("(\\[.+\\])([^ ]*) *(.*)([;\\{])");			//	レシピファイル読み込みはここで判別必要
	Pattern spat=Pattern.compile("^(["+select_chars+"]+)(.*)$");	
	
	MenuReader(LinkedHashMap<String, String> brands, LinkedHashMap<String, String> books, MenuDate menu_date, String select_chars, ReaderData reader_data){
		super(reader_data);	
		this.brands=brands;
		this.books=books;
		this.menu_date=menu_date;
		this.select_chars=select_chars;
		
		datestring=menu_date.getDatestring();
		
//		spat=Pattern.compile("^(["+select_chars+"]+)(.*)$");
	}
	MenuData read(String filename){
		LinkedList<MenuData> list=read_file(filename);
		return get_daily_menus(list);
	}
	LinkedList<MenuData> read_file(String filename){
		return  read_file(filename,true);
	}
	LinkedList<MenuData> read_file(String filename, boolean output_error_f){
		Pattern pat=Pattern.compile("(["+select_chars+"]+)(.+)\\{(.*)");				//	直接入力の場合
		Pattern ipat=Pattern.compile("(["+select_chars+"]+)([^ ]+) *([^;]+).*");		//	参照入力の場合（データ1行のみ）
		Pattern rpat=Pattern.compile("(\\[.+\\])([^ ]*) *(.*)");						//	レシピブック読み込み判別

		this.output_error_f=output_error_f;
		Pattern fpat=Pattern.compile("[0-9]{6}");
		
		Matcher fmat=fpat.matcher(filename);
		if (fmat.find()) datestring=fmat.group(0);
		
		LinkedList<MenuData> list=new LinkedList<MenuData>();
		int num=0;														//	項目のID番号
		try {
			BufferedReader in=new BufferedReader(new FileReader(filename));
			boolean stat=true;
			while(true){
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;							//	空行は無視
				if (line.substring(0,1).equals(":")) continue;			//	: で始まる行はコメント(無視)
	
				Matcher mat=pat.matcher(line);							//	ケースの切り分け
				if (mat.find()){										//	マッチしない行は無視される。メニュー項目の直接入力は不可
//					System.out.println(mat.group(2));
					Matcher rmat=rpat.matcher(line);
					if (rmat.find()) {
						num=analyze_book_import(in,rmat,list,num,true);	//	レシピブック読み込み(上書きあり,あり合わせ機能)
					}
					else {
						num=analyze_menu_item(in,mat,list,num);			//	直接記述されているメニュー項目をデータに追加する	。brandの処理はこの中で行う							
					}
					continue;
				}
				Matcher imat=ipat.matcher(line);
				if (imat.find()){
					Matcher rmat=rpat.matcher(line);
					if (rmat.find()) {
						num=analyze_book_import(in,rmat,list,num,false);//	レシピブック読み込み(上書きなし)
					}
					else {
						num=analyze_recipe_import(in,imat,list,num);	//	インポートする場合（インラインのレシピ読み込み）
					}
				}
			}
			in.close();
		}
		catch(Exception e){
			System.err.println(e);
		}
		return list;
	}
	int analyze_menu_item(BufferedReader in, Matcher mat, LinkedList<MenuData> list, int num) {		
		String default_selected=mat.group(1);
		MenuServing mserv=new MenuServing(mat.group(2));	//	食数の分析
		String keywords=mserv.getKeywords();
		String comment=mat.group(3);
		String[] strs=keywords.split("[ \t]");				//	ブランド名の分離
		String brand=null;
		if (strs.length>1) brand=strs[1];					//	matで分離すべきであろう
		
		LinkedHashMap<String, Nutritions> brand_products=new LinkedHashMap<String, Nutritions>();		//	brandsデータへの対応
		FoodProductsXML foodproductsxml=new FoodProductsXML();
		LinkedHashMap<String,Nutritions> fp=foodproducts;
		if (brand!=null) {
			String file=brands.get(brand);
			if (file!=null) {
				LinkedHashSet<String> mkeys=standard_nutrition.get_mkeys();
				LinkedHashSet<String> vkeys=standard_nutrition.get_vkeys();
				brand_products.putAll(foodproductsxml.read(brand, file, nutrition_table, IDmap, mkeys, vkeys));
				fp=brand_products;				
			}
		}		
		
		String Mid=get_Mid(num++);
		MenuData M=new MenuData(Mid,keywords,default_selected,standard_nutrition,mserv,comment);	//	メニュー項目を作る
		list.add(M);
		
		try {
			while(true){
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;				//	空行を無視
				String selected=default_selected;			//	選択のオバーライト
				Matcher smat=spat.matcher(line);			//	選択文字を取り除いて改めて処理する
				if (smat.find()) {
					selected=smat.group(1);
					line=smat.group(2);
				}
				strs=split(line);
				if (strs[0].equals("}")) break ;
				if (strs[0].substring(0,1).equals(":")) continue ;
				
				MenuData Mitem=analyze_line(strs, fp, selected, num++);		//	メニュー項目に素材を追加する
				if (Mitem!=null) {
					list.add(Mitem);
					M.add_item(Mitem);
				}
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
		return num;
	}
	int analyze_recipe_import(BufferedReader in, Matcher imat, LinkedList<MenuData> list, int num) {
		String selected=imat.group(1);
		String meal=imat.group(2);
		
		
		MenuImport mi=new MenuImport(imat.group(3),menu_date,datestring);
		String ifilename=menu_dir+mi.getFdatestring();
		String ikeyword=mi.getIkeyword();
		String keywords=meal+" "+ikeyword;

		LinkedList<MenuData> ilist=list;

		MenuReader ireader=new MenuReader(brands, books, menu_date, "%", reader_data);
		ilist=ireader.read_file(ifilename+".txt");
		
		MenuData M=find_menu(ilist,ikeyword);							//	メニューファイルに直接書かれているレシピの参照
		
		double quantity= 1.0;
		if (M!=null) {
			quantity=mi.get_quantity(M.getMserv());						//	メニューの数量情報から分量情報を得る
			num=import_menu(M,num,quantity,keywords,selected);			//	IDをふりなおし、分量を再計算する。
			list.add(M);						
		}
		else {
			if (output_error_f) System.out.println("   "+ikeyword+"　インポートするメニューデータが見つからない");
		}
		return num;
	}
	int analyze_book_import(BufferedReader in, Matcher rmat, LinkedList<MenuData> list, int num, boolean stat) {
		String book=books.get(rmat.group(1));
		String ikeyword=rmat.group(2);
		String qstr=rmat.group(3);
		RecipeParser rp=new RecipeParser(book, reader_data);		//	呼ばれるたびにレシピ本ファイルをオープンして解析する
		MenuData M=rp.find_recipe(ikeyword);						//	レシピ本ファイルの参照。先頭から1レコードごとに参照される
		
		RecipeImport ri=new RecipeImport(ikeyword,qstr);
		double quantity= 1.0;
		if (M!=null) {
			quantity=1.0/ri.get_quantity(M.getMserv());				//	メニューの数量情報から分量情報を得る
			ri.update_serving(M.getMserv());
			System.out.println(quantity+" "+M.getMserv().output());
			String keywords=ri.getIkeyword();
			num=import_menu(M,num,quantity,keywords,"%");			//	IDをふりなおし、分量を再計算する。
			list.add(M);
			if (stat) num=analyze_replace_item(in,num,list,M);		//	あり合わせ機能、statで上書きする要素があるかどうかを知る
		}
		else {
			if (output_error_f) System.out.println("   "+ikeyword+"　インポートするメニューデータが見つからない");
		}
		rp.close();													//	レシピ本ファイルをクローズ
		return num;
	}
	int analyze_replace_item(BufferedReader in, int num, LinkedList<MenuData> menu, MenuData M){
		LinkedHashMap<String, MenuData> menu_map=create_map(M);			//	キーワードで置換できるようにマップを生成
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;							//	空行を無視
				String[] strs=split(line);
				if (strs[0].equals("}")) break ;
				if (strs[0].substring(0,1).equals(":")) continue ;		//	選択指示があってはいけない
				MenuData Mitem=parse(strs,num);
				if (Mitem!=null) {
					String keywords=Mitem.getKeywords();
					MenuData m=menu_map.get(keywords);
					if (m!=null) {
						Mitem.setId(m.getId());
						menu_map.replace(keywords,Mitem);				//	同一のキーワードのレコードがあるとき置換する
					}
					else {
						Mitem.setId(get_Mid(num++));					//	同一のキーワードのレコードがないとき追加する
						menu_map.put(keywords,Mitem);
						menu.add(Mitem);
					}
				}
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
		M.setMenuitems(create_list(menu_map));
		return num;
	}
	LinkedHashMap<String, MenuData> create_map(MenuData M){
		LinkedHashMap<String, MenuData> map=new LinkedHashMap<String, MenuData>();
		LinkedList<MenuData> list=M.getMenuitems();
		list.forEach(V -> map.put(V.getKeywords(), V));
		return map;
	}
	LinkedList<MenuData> create_list(LinkedHashMap<String, MenuData> map){
		LinkedList<MenuData> list=new LinkedList<MenuData>();
		map.forEach((K,V) -> list.add(V));
		return list;
	}
	MenuData find_menu(LinkedList<MenuData>ilist, String ikeyword){
		Pattern pat=Pattern.compile(ikeyword);
		Pattern rpat=Pattern.compile("(\\[.+\\])");
		MenuData M=null;
		for (MenuData D: ilist){
			Matcher mat=pat.matcher(D.getKeywords());
			if (mat.find()){
				M=new MenuData(D);					//	当日のデータを参照するときは、コピーしなければならない
				break;
			}
		}
		return M;
	}
	int import_menu(MenuData M, int num, double quantity, String keywords, String selected){
		M.set_id(get_Mid(num++),keywords,selected);
		for (MenuData D: M.getMenuitems()){
			D.set_id(get_Mid(num++),selected);
			D.mult(quantity);
		}
		return num;
	}
	String get_Mid(int num){
		return datestring+new DecimalFormat("000").format(num++);
	}
	MenuData get_daily_menus(LinkedList<MenuData> list){
		Pattern bpat=Pattern.compile("朝食");
		Pattern lpat=Pattern.compile("昼食");
		Pattern dpat=Pattern.compile("夕食");
		Pattern spat=Pattern.compile("おやつ|間食|夜食|スナック|その他");				//	毎日のメニュー項目ではこのキーワードが必須となった
		int num=list.size()+1;														//	リストの要素数がnumに対応するはず
		MenuData breakfast=new MenuData(get_Mid(num++),"朝食",standard_nutrition);	//	それぞれの要素の名前がハードコードされている
		MenuData lunch=new MenuData(get_Mid(num++),"昼食",standard_nutrition);
		MenuData dinner=new MenuData(get_Mid(num++),"夕食",standard_nutrition);
		MenuData snack=new MenuData(get_Mid(num++),"間食",standard_nutrition);
		MenuData menus=new MenuData(get_Mid(num++),"全食事",standard_nutrition);
		list.forEach(V -> {
			 String keywords=V.getKeywords();
			 Matcher bmat=bpat.matcher(keywords);
			 Matcher lmat=lpat.matcher(keywords);
			 Matcher dmat=dpat.matcher(keywords);
			 Matcher smat=spat.matcher(keywords);
			 if (bmat.find()){
				 breakfast.add_item(V);
			 }
			 else if (lmat.find()){
				lunch.add_item(V);
			 }
			 else if (dmat.find()){
				 dinner.add_item(V);
			 }
			 else if (smat.find()){
				 snack.add_item(V);
			 }
		});
		if (breakfast.getMenuitems()!=null) menus.add_item(breakfast);		//	朝食、昼食、夕食の項目を抽出する
		if (lunch.getMenuitems()!=null) menus.add_item(lunch);
		if (dinner.getMenuitems()!=null) menus.add_item(dinner);
		if (snack.getMenuitems()!=null) menus.add_item(snack);
		list.add(menus);
		list.add(breakfast);
		list.add(lunch);
		list.add(dinner);
		list.add(snack);
		return menus;
	}
}
