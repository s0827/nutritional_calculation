package menu;

import java.util.*;
import java.util.regex.*;

import nutrition.*;

public class NutritionSearch {			//	栄養成分の検索機能を提供する
	LinkedHashMap<String, Nutritions> nutrition_table;
	LinkedHashMap<String, Nutritions> old_table;
	LinkedHashMap<String, Nutritions> toushitsu_table;
	LinkedHashMap<String, Nutritions> simple_table;
	LinkedHashMap<String, String> IDmap;							//	食品名から食品成分表を参照するマップ
	
	public NutritionSearch(LinkedHashMap<String, Nutritions> nutrition_table, LinkedHashMap<String, String> IDmap,
			LinkedHashMap<String, Nutritions> old_table, LinkedHashMap<String, Nutritions> toushitsu_table, LinkedHashMap<String, Nutritions> simple_table){
		this.nutrition_table=nutrition_table;
		this.IDmap=IDmap;
		this.old_table=old_table;
		this.toushitsu_table=toushitsu_table;
		this.simple_table=simple_table;
	}
	public boolean search(String[] strs) {							//	キーワード解釈を横取りして処理する
		Pattern npat=Pattern.compile("([0-9]{5})");
		boolean status=false;

		if (strs[0].equals("f")) {
			search("X",strs[1],old_table);							//	３種類のテーブルを臨時に頭文字で区別
			search("T",strs[1],toushitsu_table);
			search("S",strs[1],simple_table);
			status=true;
		}
/*		else if (strs[0].equals("F")){
			status=true;
		}*/
		else if (strs[0].equals("n")){								//	キーワードで食品成分表を検索する
			System.out.println("食品成分表に登録されている項目");
			nutrition_table.forEach((K,V) -> {
				Pattern pat=Pattern.compile(strs[1]);
				Matcher mat=pat.matcher(V.getName());
				if (mat.find()){
					V.output(strs[1]);
				}
			});
			status=true;
		}
		else if (strs[0].equals("N")){								//	キーワードで食品成分表を参照し、100gあたりの栄養成分を表示
			Matcher nmat=npat.matcher(strs[1]);
			Nutritions N=null;
			if (nmat.find()) {
				N=nutrition_table.get(nmat.group(1));				//	idから直接取得
				System.out.println("食品成分表データ(100g当り)(ID検索)");
				String str=N.get_idstr();
				N.output_major_nutritions(str);
			}
			else {
				String id=IDmap.get(strs[1]);									//	辞書のキーワードからidを取得
				if (id!=null){
					N=nutrition_table.get(id);
				}
				if (N!=null) {
					System.out.println("辞書キーワードと食品成分表データ(100g当り)(完全一致)");		//	完全一致と部分一致の両方を表示する
					String str=N.get_idstr();
					N.output_major_nutritions("("+strs[1]+") "+str);
				}
				System.out.println("辞書キーワードと食品成分表データ(100g当り)(部分一致)");
				Iterator<String> it=IDmap.keySet().iterator();								//	辞書の部分一致の再検索
				Pattern ipat=Pattern.compile(strs[1]);
				IDmap.forEach((K,V) -> {
					Matcher imat=ipat.matcher(K);
					if (imat.find()) {
						Nutritions n=nutrition_table.get(V);
						String str=n.get_idstr();
						n.output_major_nutritions("("+K+") "+str);						
					}
				});
			}
			status=true;
		}
		return status;
	}
	void search(String hdr, String keyword, LinkedHashMap<String, Nutritions> table) {
		Pattern pat=Pattern.compile(keyword);
		Iterator<String> it=table.keySet().iterator();
		while(it.hasNext()) {
			String id=it.next();
			Matcher mat=pat.matcher(id);
			if (mat.find()) {
				Nutritions N=table.get(id);
				System.out.print(hdr+": ");
				N.output_major_nutritions(1);
			}
		}
	}
}
