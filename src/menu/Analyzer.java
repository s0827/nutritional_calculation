package menu;

import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.nio.file.*;

import nutrition.*;

public class Analyzer {
	String menu_dir="./menufiles/";							//	相対日付のファイルで絶対日付のファイルのリンクを保持する
	String archive_dir="./menu2/";							//	絶対日付を名前とするファイルでデータが保存される
	String relative_arcdir="../menu2/";						//	シンボリックリンク作成のための相対パス
	String brand_file="brands.txt";							//	外食のブランド名のデータ
	String books_file="books.txt";							//	レシピ本のデータ
	String foodproducts_xml="foodproducts.xml";				//	加工食品のデータ
//	String unit_file="unit.txt";							//	単位換算用のデータ(Unit2ではハードコードになる)
	String template="template.txt";
	String menufile="";										//	メニューファイル名は日付を評価して決まる
	
	ReaderData reader_data;									//	メニュー読み込みのための各種データ
	MenuDate menu_date;
	MenuReader menureader;									//	メニューファイル読み込みエンジン
	MenuFinder menufinder;									//	メニュー検索エンジン
	
	NutritionSearch nutsearch;								//	栄養成分あるいは登録項目を検索して計算の懸賞を行う
//	RecipeSearch recsearch;									//	レシピの検索を提供
	
	MenuFrame frame;										//	可視化エンジン
		
	TreeSet<Integer> rfileset,fileset;						//	これまでに作られたメニューファイル。メニューの検索のために生成、昇順、降順両方を保持
	
//	String select_chars="[#%&@\\$]";
	String select_chars="[#&@\\$]";

	String datestring;										//	YYMMDDの日付情報
	String formatted_date;									//	表示用に年月日で表示された日付
	String select="#";										//	選択された項目
	
	int maxdepth=4;											//	表示の深さと幅をクラス変数とする
	int maxwidth=2;
	int depth=2;											//	表示の深さと幅
	int width=1;
	
	public static void main(String args[]){					//	プログラムを実行することでファイルへの参照が変わるので、毎朝先にプログラムを実行しなければならない
		Analyzer analyzer=new Analyzer();
	}
	Analyzer(){
		create_fileset();									//	履歴のセットを作成
		initialize_datastructure();							//	栄養素のデータの取得
//		save();												//	ファイルがリンクになればコピーはいらない
		initialize_graphics();
		update_file_references();							//	ファイルのリンクのアップデート
		process();											//	各種処理
	}
	void create_fileset(){
		rfileset=new TreeSet<Integer>();
		Pattern pat=Pattern.compile("([0-9]{6})\\.txt");
		File dir=new File(archive_dir);
		File[] files=dir.listFiles();
		for (int i=0; i<files.length; i++){
			String filename=files[i].getName();
			Matcher mat=pat.matcher(filename);
			if (mat.find()) rfileset.add(Integer.parseInt(mat.group(1)));
		}
		fileset=(TreeSet)rfileset.descendingSet();					//	最近のデータから遡及するように参照できるセットを取得
//		fileset.forEach(V -> System.out.println(V));
	}
	void initialize_datastructure(){
		menu_date=new MenuDate();
		menu_date.setDirectory(archive_dir);						//	メニューファイルを得るためにはディレクトリを教えておく必要がある。
		datestring=menu_date.getDatestring();
		formatted_date=menu_date.getFormatted_date();
		menufile=menu_date.get_default_menufile();					//	日付を評価してメニューファイルを取得
		
		
		IndexReader brands_reader=new IndexReader(brand_file);
		LinkedHashMap<String, String> brands=brands_reader.getMap();
		IndexReader books_reader=new IndexReader(books_file);
		LinkedHashMap<String, String> books=books_reader.getMap();
		
		reader_data=new ReaderData();

		nutsearch=reader_data.getNutsearch();
					
		menureader=new MenuReader(brands, books, menu_date, select_chars, reader_data);
		menufinder=new MenuFinder(menureader);
	}
	void initialize_graphics(){
		frame=new MenuFrame();
		frame.put_standard_nutrition(reader_data.getStandard_nutrition());
	}
	void update_file_references(){
		LinkedHashMap<String, String> datestrings=menu_date.getDatestrings();
		String src=template;
		datestrings.forEach((K,V) -> {
			Integer I=new Integer(Integer.parseInt(V));
			
			String dst=archive_dir+V+".txt";		//	パスの指定の確認必要
			String rlink=relative_arcdir+V+".txt";
			try {
				if (!fileset.contains(I)){
					Files.copy(Paths.get(src),Paths.get(dst));
				}
				String link=menu_dir+K+".txt";
				Files.delete(Paths.get(link));		//	前日に作ったファイル（シンボリックリンク）を消去しないと再設定できない
				Files.createSymbolicLink(Paths.get(link),Paths.get(rlink));
			}
			catch(Exception e){}
		});
	}
	void process(){									//	コマンドを実行するループ。GUIのメインイベントループに対応
		MenuData menus=read_menus(menufile);
		frame.put_title(formatted_date);
		menus.output(select,depth,0,width);			//	表示の深さ(depth)とインデントレベルを指定
		frame.repaint();
//		output_map(map);
		
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			while(true){		
				boolean output_f=false;
				
				System.out.print("> ");
				String line=in.readLine();
				check_date();																//	日付が変わっていれば終了
				if (line.length()>0) {
					String[] strs=line.split("[ \t]");										//	各コマンドが受け取るデータの書式が違うので対応しなければならない
					
					if (strs[0].length()==1){												//	一文字のコマンドを受け取った時
						output_f=process_commands(in,strs);
					}
					else {																	//	選択項目の指定
						String fdstring=menu_date.get_file(strs[0]);
						if (fdstring==null) {
							Pattern fpat=Pattern.compile("[0-9]{6}");						//	日付でファイルが指定されたとき
							Matcher fmat=fpat.matcher(strs[0]);
							if (fmat.find()) fdstring=fmat.group(0);
						}
						if (fdstring!=null) {
							menufile=archive_dir+fdstring+".txt";
							formatted_date=menu_date.get_formatted_date(fdstring);
							output_f=true;
						}
					}
				}
				else {
					output_f=true;					//	リターンだけのとき同じ表示を繰り返す
				}
				if (output_f) {						//	出力が指定されるたびにメニューファイルを読み直す
					menus=read_menus(menufile);
					frame.put_data(menus);
					frame.put_title(formatted_date);
					frame.repaint();
					menus.output(select,depth,0,width);
				}
			}			
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	boolean process_commands(BufferedReader in, String[] strs){
		boolean output_f=false;
		String str="";
		for (int i=1; i<strs.length; i++) str+=strs[i];
		
		if (nutsearch.search(strs)) {}
//		else if (recsearch.search(in,strs)){}
		
		else if (strs[0].toUpperCase().equals("O") || strs[0].equals("0")){		//	メニューファイルをデフォルトに戻す
			menufile=menu_date.get_default_menufile();
			formatted_date=menu_date.getFormatted_date();
			output_f=true;
		}
		else if (strs[0].toUpperCase().equals("T")){							//	データベースのエントリをテストする
			MenuData M=menureader.analyze_line(str);							//	MenuReaderの各行の読み込みの処理
			if (M!=null) System.out.println(M.getId()+" "+M.getKeywords());
		}
		else if (strs[0].toUpperCase().equals("M")){
			if (strs.length>1){
				menufinder.find_menu(strs[1],fileset);
			}
			else {
				menufinder.list_menu(fileset);
			}
		}
		else if (strs[0].toUpperCase().equals("S")){
			output_summary();
			
			menufile=menu_date.get_default_menufile();		//	表示終了後はデフォルトの表示に戻る
			formatted_date=menu_date.getFormatted_date();
			output_f=true;
		}
		else if (strs[0].equals("+")) {
			depth++;
			if (depth>maxdepth) depth=maxdepth;
			output_f=true;
		}
		else if (strs[0].equals("-")) {
			depth--;
			if (depth<0) depth=0;
			output_f=true;
		}
		else if (strs[0].equals(">")) {
			width++;
			if (width>maxwidth) width=maxwidth;
			output_f=true;
		}
		else if (strs[0].equals("<")) {
			width--;
			if (width<0) width=0;
			output_f=true;
		}
		else {
			Pattern pat=Pattern.compile(select_chars);
			Matcher mat=pat.matcher(strs[0]);
			if (mat.find()) {
				select=mat.group(0);
				if (select.equals("$")) select="\\$";
				output_f=true;
			}
			output_f=true;
		}
		return output_f;
	}
	void check_date(){											//	日付が変わっていると、ファイル構造の再構築が必要なので終了し再起動させる
		MenuDate md=new MenuDate();								//	MenuDateを取り直して調べる
		if (!datestring.equals(md.getDatestring())){
			System.out.println("日付が変わりました。プログラムを終了する");
			System.exit(0);
		}
	}
	MenuData read_menus(String filename){								
		MenuData menus=menureader.read(filename);				//	メニュー項目の読み取り、メニューの階層構造を作る
		menus.summation(select);								//	階層構造に従って栄養素の和を計算する		
		frame.put_data(menus);									//	可視化にデータを渡す		
		return menus;
	}
	String get_Mid(int num){
		return datestring+new DecimalFormat("000").format(num++);
	}
	void output_map(LinkedHashMap<String, MenuData> menumap){							//	項目をそのまま出力(確認用)
		menumap.forEach((K,V) -> {
			System.out.println(K+" "+V.getKeywords());
		});
	}
	void output_summary(){
		System.out.println("日付\tエネルギー\t糖質\tたんぱく質\t脂質\t塩分\t食物繊維");
		int i=20;
		for (Integer I: rfileset){
			String filename=I+".txt";
			MenuData menus=menureader.read(archive_dir+filename);
			menus.summation(select);
			System.out.print(i+"\t"+I);
			menus.output_summary();
			i++;
		}
		System.out.println();
	}
}
