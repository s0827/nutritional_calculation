package menu;

import java.text.DecimalFormat;
import java.util.*;
import java.awt.*;
import java.awt.geom.*;

import nutrition.*;

public class MenuPanel extends Panel {
	int panelwidth,panelheight;
	MenuData data;
	String title;								//	プロットのタイトル(日付)
	Nutritions standard_nutrition;				//	栄養素基準値
	
	double calorie_max=1600.0;					//	３大栄養素については基準値は簡単に決まらないのでここに置く。
	double calorie_disp_max=2000.0;				//	カロリーの表示上の最大値
	double e_carbohydrate=4.0;					//	糖質、たんぱく質、脂質1gあたりのエネルギー
	double e_protein=4.0;
	double e_fat=9.0; 
	double carbohydrate_max=calorie_max*0.47/e_carbohydrate;
	double protein_max=calorie_max*0.2/e_protein;
	double fat_max=calorie_max*0.33/e_fat;
	
	double salt_max=8.0;						//	食塩摂取量 8g
	double fiber_max=19.0;						//	食物繊維摂取量 19g
	
	int px0=25, py0=50;							//	３大栄養素のデータを表示するボックスの開始位置
	int px1=110;								//	文字フィールドの巾。この値シフトしてボックスを置く				
	int py1=55;									//	次のボックスを置く位置
	int wx0=300, wy0=35;						//	ボックスの幅と高さ
	
	int px2=30, py2=py0+py1*6+25, py3=35;		//	シグナルのパラメータ
	int cx0=15, cy0=15;
	
	int py4=py2+2*py3-15;							//	カラーバー
	int cbx=300, cby=10;

	Color ccarb=new Color(255,127,127);			//	塗りの色指定。糖質
	Color cprot=new Color(127,255,127);			//	たんぱく質
	Color cfat=new Color(127,127,255);			//	脂質
	Color csalt=new Color(127,127,127);			//	塩分
	Color cfiber=new Color(180,140,50);			//	食物繊維
	
	
	String[] labels={"エネルギー(kcal)","糖質(g)","たんぱく質(g)","脂質(g)","塩分(g)","食物繊維(g)"};

	MenuPanel(int panelwidth, int panelheight){
		this.panelwidth=panelwidth;
		this.panelheight=panelheight;
		setSize(panelwidth,panelheight);
		setVisible(true);
	}
	void setData(MenuData data){ this.data=data; }
	void setStandard_nutrion(Nutritions standard_nutrition){ this.standard_nutrition=standard_nutrition; }
	void setTitle(String title){ this.title=title; }
	
	public void paint(Graphics gc){
		if (data==null) return;
		Graphics2D g2=(Graphics2D)gc;
		Rectangle2D background=new Rectangle2D.Double(0,0,panelwidth,panelheight);
		g2.setPaint(new Color(255,255,255));
		g2.fill(background);
		
		g2.setPaint(new Color(0,0,0));
		g2.drawString(title+"の栄養",px0,py0-20);

		double calorie=data.getCalorie();
		double carbohydrate=data.getCarbohydrate();
		double protein=data.getProtein();
		double fat=data.getFat();

		int pe0=get_x(carbohydrate*e_carbohydrate,calorie_disp_max,wx0);
		int pe1=get_x(protein*e_protein,calorie_disp_max,wx0);
		int pe2=get_x(fat*e_fat,calorie_disp_max,wx0);
		
		double rc=carbohydrate*e_carbohydrate/calorie*100.0;					// PFCの割合を表示する
		double rp=protein*e_protein/calorie*100.0;
		double rf=fat*e_fat/calorie*100.0;
		
		int px=px0;
		int py=py0;
		px=px0+px1;
		int pc=px;
		int pp=pc+pe0;
		int pf=pp+pe1;
		Rectangle2D carb_box=new Rectangle2D.Double(pc,py,pe0,wy0);				//	エネルギーの表示
		g2.setPaint(ccarb);														//	糖質、たんぱく質、脂質成分の表示
		g2.fill(carb_box);
		Rectangle2D prot_box=new Rectangle2D.Double(pp,py,pe1,wy0);
		g2.setPaint(cprot);
		g2.fill(prot_box);
		Rectangle2D f_box=new Rectangle2D.Double(pf,py,pe2,wy0);
		g2.setPaint(cfat);
		g2.fill(f_box);
		g2.setPaint(Color.black);
		g2.drawString(new DecimalFormat("0").format(calorie_max)+" kcal diet",px0+px1+wx0-50,py0-20);
		g2.drawString(new DecimalFormat("0").format(calorie), px0+px1+wx0+5,py+wy0-10);
		g2.setPaint(Color.white);
		g2.drawString(new DecimalFormat("0").format(rc)+"%",pc+5,py+wy0-10);
		g2.drawString(new DecimalFormat("0").format(rp)+"%",pp+5,py+wy0-10);
		g2.drawString(new DecimalFormat("0").format(rf)+"%",pf+5,py+wy0-10);
		
		DailyDiet Daily=new DailyDiet(data);
		DailyDietItem Cal=Daily.getCal();										//	朝昼夕食別のデータを取得
		
		int cb=px0+px1+get_x(Cal.get_sum_breakfast(),calorie_disp_max,wx0);
		int cl=px0+px1+get_x(Cal.get_sum_lunch(),calorie_disp_max,wx0);
		int cd=px0+px1+get_x(Cal.get_sum_dinner(),calorie_disp_max,wx0);
		g2.setPaint(Color.black);
		Line2D lcb=new Line2D.Double(cb,py0-4,cb,py0);
		g2.draw(lcb);
		if (Cal.getBreakfast()>0.0) g2.drawString("朝 "+new DecimalFormat("0").format(Cal.getBreakfast()),cb+2,py0-5);		
		Line2D lcl=new Line2D.Double(cl,py0-4,cl,py0);
		g2.draw(lcl);
		if (Cal.getLunch()>0.0) g2.drawString("昼 "+new DecimalFormat("0").format(Cal.getLunch()),cl+2,py0-5);
		Line2D lcd=new Line2D.Double(cd,py0-4,cd,py0);
		g2.draw(lcd);
		if (Cal.getDinner()>0.0) g2.drawString("晩 "+new DecimalFormat("0").format(Cal.getDinner()),cd+2,py0-5);
		
		double pc1=get_x(calorie_max,calorie_disp_max,wx0);							//	カロリー最大値の線を引く
		Line2D line=new Line2D.Double(px0+px1+pc1,py,px0+px1+pc1,py+wy0);
		g2.setPaint(new Color(0,0,0));
		g2.draw(line);
		
		major_nutritions(g2,Daily.getCarb(),carbohydrate_max,py+=py1,ccarb);		//	糖質
		major_nutritions(g2,Daily.getProt(),protein_max,py+=py1,cprot);
		major_nutritions(g2,Daily.getFat(),fat_max,py+=py1,cfat);
		major_nutritions(g2,Daily.getSalt(),salt_max,py+=py1,csalt);
		major_nutritions(g2,Daily.getFiber(),fiber_max,py+=py1,cfiber);
		
		g2.setPaint(new Color(0,0,0));
		for (int i=0; i<6; i++){													//	外枠の描画
			Rectangle2D box=new Rectangle2D.Double(px0+px1,py0+i*py1,wx0,wy0);
			g2.draw(box);
		}
		px=px0;
		py=py0+wy0-10;
		for (int i=0; i<6; i++) g2.drawString(labels[i],px,py+i*py1);				//	タイトルの表示
		
		px=px0;
		py=py2-5;
		g2.drawString("ミネラル",px,py-5);				//	ミネラルとビタミンのシグナルの表示
		mv(g2,data.getMinerals(),standard_nutrition.getMinerals(),px,py,false);
		py+=py3;
		g2.drawString("ビタミン",px,py-5);				//	ミネラルとビタミンのシグナルの表示
		mv(g2,data.getVitamins(),standard_nutrition.getVitamins(),px,py,true);
		
		px=px0+px1;										//	カラーバー
		py=py4;
		for (int i=0; i<wx0; i++){
			double y=(double)i/(double)cbx*2.0;
			Color c=get_color(y,1.0);
			line=new Line2D.Double(px+i,py,px+i,py+cby);
			g2.setPaint(c);
			g2.draw(line);
		}
		Rectangle2D box=new Rectangle2D.Double(px,py,cbx,cby);
		g2.setPaint(Color.black);
		g2.draw(box);

		g2.setPaint(Color.black);
		g2.drawString("推奨値に対して",px0,py+10);
		int num=4;
		for (int i=0; i<=num; i++){
			int ps=px+i*cbx/num;
			double r=(double)i/(double)num*2.0;
			g2.drawString(new DecimalFormat("0").format(r*100.0)+"%",ps-5,py-5);
			line=new Line2D.Double(ps,py,ps,py+cby);
			g2.draw(line);
		}
	}
	void major_nutritions(Graphics2D g2, DailyDietItem item, double max, int py, Color color){
		double pc0=get_x(item.getTotal(),max*1.5,wx0);
		Rectangle2D box=new Rectangle2D.Double(px0+px1,py,pc0,wy0);					//	糖質
		g2.setPaint(color);
		g2.fill(box);
				
		double pc1=get_x(max,max*1.5,wx0);
		Line2D line=new Line2D.Double(px0+px1+pc1,py,px0+px1+pc1,py+wy0);
		g2.setPaint(new Color(0,0,0));
		g2.draw(line);

		g2.setPaint(new Color(255,255,255));										//	朝食、昼食、夕食で区切り線を入れる
//		System.out.println(item.getTotal()+" "+item.get_sum_breakfast()+" "+item.get_sum_lunch()+" "+item.get_sum_dinner());
		double pb=get_x(item.get_sum_breakfast(),max*1.5,wx0);
		double pl=get_x(item.get_sum_lunch(),max*1.5,wx0);
		double pd=get_x(item.get_sum_dinner(),max*1.5,wx0);
		line=new Line2D.Double(px0+px1+pb,py,px0+px1+pb,py+wy0);
		g2.draw(line);
		line=new Line2D.Double(px0+px1+pl,py,px0+px1+pl,py+wy0);
		g2.draw(line);
		line=new Line2D.Double(px0+px1+pd,py,px0+px1+pd,py+wy0);
		g2.draw(line);
		
		g2.setPaint(Color.black);
		g2.drawString(new DecimalFormat("0").format(max),px0+px1+(int)pc1,py-4);
		g2.drawString(new DecimalFormat("0.0").format(item.getTotal()), px0+px1+wx0+5,py+wy0-10);		
	}
	void mv(Graphics2D g2, LinkedHashMap<String, Double> map, LinkedHashMap<String, Double> std, int px, int py, boolean vitamin_f){
		g2.setPaint(new Color(0,0,0));
		px+=px1;
		py-=18;
		Iterator<String> it=map.keySet().iterator();
		while(it.hasNext()){
			String str=it.next();
			if (vitamin_f) str=str.substring(1);
			g2.drawString(str,px,py);
			px+=px2;
		}
		px=px0+px1;
		py+=3;
		it=map.keySet().iterator();
		while(it.hasNext()){
			String key=it.next();
			Ellipse2D signal=new Ellipse2D.Double(px,py,cx0,cy0);
			g2.setPaint(get_color(map.get(key),std.get(key)));
			g2.fill(signal);
			
			g2.setPaint(new Color(0,0,0));				//	輪郭を描画
			g2.draw(signal);
			px+=px2;
		}
	}
	int get_x(double x, double xmax, double w){
		int ix=(int)(wx0*(x/xmax));
		return ix;
	}
	Color get_color(double x, double xmax){
		double t=x/xmax;
		int r=0, g=0, b=0;
		if (t<0.01){
			r=255;
			g=255;
			b=255;
		}
		else if (t>=0.01 && t<0.5){
			r=255;
			g=(int)(255.0*2.0*t);
			b=0;	
		}
		else if (t>=0.5 && t<1.0){
			r=(int)(255.0*2.0*(1.0-t));
			g=255;
			b=0;
		}
		else if (t>=1.0 && t<1.5){
			r=0;
			g=255;
			b=(int)(255.0*2.0*(t-1.0));
		}
		else if (t>=1.5 && t<1.99){
			r=(int)(255.0*2.0*(t-1.5));
			g=(int)(255.0*2.0*(2.0-t));
			b=255;
		}
//		System.out.println(t+" "+r+" "+g+" "+b);
		Color c=new Color(r,g,b);
		return c;
	}
}
