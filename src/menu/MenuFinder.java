package menu;

import java.util.*;
import java.util.regex.*;
import java.text.*;

import nutrition.*;

public class MenuFinder {
	String menu_dir="./menu2/";
	MenuReader menureader;
	String[] srvs={"朝食","昼食","夕食"};
	LinkedList<String> slist;
	
	MenuFinder(MenuReader menureader){
		this.menureader=menureader;
		slist=new LinkedList<String>();
		for (int i=0; i<srvs.length; i++) slist.add(srvs[i]);
	}
	void find_menu(String str, TreeSet<Integer> fileset){
		int i=0;
		for (Integer I: fileset){
			String fstr=new DecimalFormat("0").format(I);
			String filename=I+".txt";
			LinkedList<MenuData> list=menureader.read_file(menu_dir+filename, false);
			boolean stat=search(str,fstr,list);
			if (stat) i++;
			if (i>3) break;
		}		
	}
	void list_menu(TreeSet<Integer> fileset){
		int i=0;
		for (Integer I: fileset){
			String fstr=new DecimalFormat("0").format(I);
			String filename=I+".txt";
			System.out.println(reformat_date(fstr));
			MenuData menus=menureader.read(menu_dir+filename);
			list(menus);
			i++;
			if (i>10) break;
		}
	}
	void list(MenuData menus){
		LinkedList<MenuData> menuitems=menus.getMenuitems();
		menuitems.forEach(V -> {
			String keywords=V.getKeywords();
			if (slist.contains(keywords)){
				LinkedHashSet<String> s=new LinkedHashSet<String>();
				LinkedList<MenuData> m=V.getMenuitems();
				m.forEach(v -> {
					String k=v.getKeywords();
					k=k.replaceAll(keywords,"");
					s.add(k);
				});
				System.out.print("　"+keywords+" [");
				s.forEach(x -> System.out.print(" "+x));
				System.out.println(" ]");
			}
		});
	}
	boolean search(String str, String fstr, LinkedList<MenuData> list){
		Pattern pat=Pattern.compile(str);
		boolean stat=false;
		list.forEach(V -> {
			String keywords=V.getKeywords();
			Matcher mat=pat.matcher(keywords);
			if (mat.find()) {
				System.out.println(reformat_date(fstr)+" "+keywords);
				LinkedList<MenuData> m=V.getMenuitems();
				if (m!=null){
					m.forEach(v -> {
						v.output();					
					});				
				}
			}
		});
		return stat;
	}
	String reformat_date(String str){
		return "20"+str.substring(0,2)+"年"+str.substring(2,4)+"月"+str.substring(4,6)+"日";
	}
}
