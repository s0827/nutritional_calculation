package menu;

import java.io.*;
import java.util.*;
import java.util.regex.*;

public class IdReader {													//	食品の名前（抽象的な名前）から食品成分表のIDへの参照をマップに取得する
	LinkedHashMap<String, String> IDmap;								//	IDがdictionaryに対応
	public IdReader(String idrefs_file){
		 IDmap=new LinkedHashMap<String, String>();
		 try {
			 BufferedReader in=new BufferedReader(new FileReader(idrefs_file));
			 while(true){
				 String line=in.readLine();
				 if (line==null) break;
				 
				 if (line.length()==0) continue;
				 if (line.substring(0,1).equals(":")) continue;			//	コロンでコメント行
				 String[] strs=line.split("[ \t]+");					//	スペースまたはタブ区切り
				 strs[0]=strs[0].replaceAll("\b","");					//	余計な\b記号を削除
				 IDmap.put(strs[0],strs[1]);							//	コメントの内容は保持されない
			 }
			 in.close();
		 }
		 catch(Exception e){
			 System.err.println(e);
		 }
	}
	public LinkedHashMap<String, String> getIDmap(){ return IDmap; }
}
