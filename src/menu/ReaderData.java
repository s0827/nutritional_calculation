package menu;

import java.util.*;

import nutrition.*;

public class ReaderData {
	String standard_file="mineral-vitamin.xml";				//	標準摂取量のデータ
	String idref_file="dictionary.txt";						//	食品成分表のIDを保存する。旧版と同じファイルを使う

	LinkedHashMap<String, Nutritions> foodproducts;			//	加工食品、外食データ
	Nutritions standard_nutrition;							//	標準栄養所要量
	LinkedHashMap<String, Nutritions> nutrition_table;		//	食品成分表のデータ	
	LinkedHashMap<String, String> IDmap;					//	食品名から食品成分表を参照するマップ
	Unit2 unit;												//	単位の換算
	
	NutritionSearch nutsearch;								//	検索機能。多分tentativeな実装
	
	ReaderData(){											//	外食データの読み込みのためにbrandsが必要
		unit=new Unit2();
		
		StandardNutritionsXML snreader=new StandardNutritionsXML(standard_file);		//	標準摂取量を読み込む
		LinkedHashSet<String> mkeys=snreader.get_mkeys();
		LinkedHashSet<String> vkeys=snreader.get_vkeys();
		standard_nutrition=(Nutritions)snreader;
		
		IdReader idreader=new IdReader(idref_file);					//	食品名から食品成分表を参照するマップの取得
		IDmap=idreader.getIDmap();

		NutritionReader nutrition_reader=new NutritionReader();		//	食品成分表を読み込む
		nutrition_table=nutrition_reader.getNutritionIdMap();		//	読み込むファイルはNutritionReader内に記述
	
		FoodProductReader foodproducts_reader=new FoodProductReader(nutrition_table,IDmap, mkeys,vkeys);	//	加工食品、外食のテーブルを読み込む
		foodproducts=foodproducts_reader.getFoodproducts();													//	古い実装もFoodProductREaderに移動
		nutsearch=foodproducts_reader.getNutsearch();
	}	
	LinkedHashMap<String, Nutritions> getFoodproducts(){ return foodproducts; }
	Nutritions getStandard_nutrition() { return standard_nutrition; }
	LinkedHashMap<String, Nutritions> getNutrition_table() { return nutrition_table; }
	LinkedHashMap<String, String> getIDmap(){ return IDmap; }
	Unit2 getUnit() { return unit; }
	
	NutritionSearch getNutsearch(){ return nutsearch; }
}
