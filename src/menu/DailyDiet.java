package menu;

import java.util.*;

import nutrition.*;

public class DailyDiet {					//	栄養成分を朝食、昼食、夕食に分けて出力する
	DailyDietItem Cal,Carb,Prot,Fat,Salt,Fiber;
	DailyDiet(MenuData data){
		Cal=new DailyDietItem();
		Carb=new DailyDietItem();
		Prot=new DailyDietItem();
		Fat=new DailyDietItem();
		Salt=new DailyDietItem();
		Fiber=new DailyDietItem();
		
		Cal.setTotal(data.getCalorie());
		Carb.setTotal(data.getCarbohydrate());
		Prot.setTotal(data.getProtein());
		Fat.setTotal(data.getFat());
		Salt.setTotal(data.getSalt());
		Fiber.setTotal(data.getFiber());
		
		LinkedList<MenuData> menuitems=data.getMenuitems();
		menuitems.forEach(V -> {
			String keywords=V.getKeywords();
			if (keywords.equals("朝食")) {
				Cal.setBreakfast(V.getCalorie());
				Carb.setBreakfast(V.getCarbohydrate());
				Prot.setBreakfast(V.getProtein());
				Fat.setBreakfast(V.getFat());
				Salt.setBreakfast(V.getSalt());
				Fiber.setBreakfast(V.getFiber());
			}
			else if (keywords.equals("昼食")){
				Cal.setLunch(V.getCalorie());
				Carb.setLunch(V.getCarbohydrate());
				Prot.setLunch(V.getProtein());
				Fat.setLunch(V.getFat());
				Salt.setLunch(V.getSalt());
				Fiber.setLunch(V.getFiber());
			}
			else if (keywords.equals("夕食")){
				Cal.setDinner(V.getCalorie());
				Carb.setDinner(V.getCarbohydrate());
				Prot.setDinner(V.getProtein());
				Fat.setDinner(V.getFat());
				Salt.setDinner(V.getSalt());
				Fiber.setDinner(V.getFiber());
			}
		});
	}
	DailyDietItem getCal(){ return Cal; }
	DailyDietItem getCarb(){ return Carb; }
	DailyDietItem getProt(){ return Prot; }
	DailyDietItem getFat(){ return Fat; }
	DailyDietItem getSalt(){ return Salt; }
	DailyDietItem getFiber(){ return Fiber; }
}
