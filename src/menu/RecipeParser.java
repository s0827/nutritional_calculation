package menu;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.*;

import nutrition.*;

public class RecipeParser extends Reader {				//	レシピ本の内容の読み込み
	BufferedReader in;
	String title,author;
	LinkedHashMap<String, String> substitute;
	LinkedHashSet<String> ignore;
		
	Pattern pat=Pattern.compile("(.+)\\{(.*)");
	Pattern qpat=Pattern.compile("([0-9\\.\\-]+)g");
	int num=0;											//	レシピ本の中のアイテム番号

	RecipeParser(String book, ReaderData reader_data) {
		super(reader_data);
		
		try {
			in=new BufferedReader(new FileReader(book));
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				String[] strs=line.split("[ \t]");
				if (strs[0].equals("title")) {
					title=strs[1];
				}
				else if (strs[0].equals("author")) {
					author=strs[1];
				}
				else if (strs[0].equals("substitute")) {
					substitute=get_map(in);
				} 
				else if (strs[0].equals("ignore")) {
					ignore=get_set(in);
					break;					//	このブロックを読んだらルールの読み込みは終了
				}
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	LinkedHashMap<String, String> get_map(BufferedReader in){
		LinkedHashMap<String, String> map=new LinkedHashMap<String, String>();
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				String[] strs=line.split("[ \t]+");
				if (strs[0].equals("end")) break;
				map.put(strs[1],strs[2]);
			}			
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return map;
	}
	LinkedHashSet<String> get_set(BufferedReader in){
		LinkedHashSet<String> set=new LinkedHashSet<String>();
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				String[] strs=line.split("[ \t]+");
				if (strs[0].equals("end")) break;
				set.add(strs[1]);
			}			
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return set;
	}
	MenuData find_recipe(String ikeyword) {
		MenuData M=null;
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;					//	空行は無視
				if (line.substring(0,1).equals(":")) continue;	//	: で始まる行はコメント(無視)
				if (line.equals("recipe")) {
					M=analyze_recipe(ikeyword);					//	読み込み処理本体
					break;
				}
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return M;
	}
	MenuData analyze_recipe(String ikeyword) {
		MenuData M=null;
		int num=0;
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;					//	空行は無視
				if (line.substring(0,1).equals(":")) continue;	//	: で始まる行はコメント(無視)
				if (line.equals("end")) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()) {
					MenuServing mserv=new MenuServing(mat.group(1));	//	食数の分析
					String keywords=mserv.getKeywords();
					if (keywords.equals(ikeyword)) {
						String Mid=get_Mid(num++);
						M=new MenuData(Mid,keywords,null,standard_nutrition,mserv,"");
						analyze_recipe_item(num,M);
					}
				}
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return M;
	}
	MenuData get_next_record() {
		MenuData M=null;
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;					//	空行は無視
				if (line.substring(0,1).equals(":")) continue;	//	: で始まる行はコメント(無視)
				if (line.equals("end")) break;
				Matcher mat=pat.matcher(line);
				if (mat.find()) {
					MenuServing mserv=new MenuServing(mat.group(1));	//	食数の分析
					String keywords=mserv.getKeywords();
					String Mid=get_Mid(num++);
					M=new MenuData(Mid,keywords,null,standard_nutrition,mserv,"");
					analyze_recipe_item(num,M);
					break;
				}
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return M;
	}
	void analyze_recipe_item(int num, MenuData M) {				//	レシピデータの解析、メニューデータと若干処理が異なる
		Pattern ipat=Pattern.compile("[\t ]*([^ ]+) +\\{");

		try {
			while(true){
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;				//	空行を無視
				String[] strs=split(line);
				if (strs[0].equals("}")) break ;
				if (strs[0].substring(0,1).equals(":")) continue ;
					
				Matcher imat=ipat.matcher(line);
				if (imat.find()) {
					String str=imat.group(1);
					if (imat.group(1).equals("レシピ")) {
						skip_block(in);							//	レシピ（料理の作り方は無視する）
					}
					else {
						num=process_block(in,M,num);			//	サブブロックの処理
					}
					continue;
				}
				
				line=substitute(line);						//	語の置換、無視する行の処理
				if (line==null) continue;
				
				MenuData Mitem=analyze_line(split(line), "", num++);
				if (Mitem!=null) {
					M.add_item(Mitem);
				}
			}		
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void skip_block(BufferedReader in) {			//	ブロックを読み飛ばす
		Pattern epat=Pattern.compile("\\}");
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				Matcher emat=epat.matcher(line);
				if (emat.find()) break;
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	int process_block(BufferedReader in, MenuData M, int num) {
		Pattern epat=Pattern.compile("\\}");
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				Matcher emat=epat.matcher(line);
				if (emat.find()) break;
				line=substitute(line);						//	語の置換、無視する行の処理
				if (line==null) continue;

				MenuData Mitem=analyze_line(split(line), "", num++);
				if (Mitem!=null) {
					M.add_item(Mitem);
				}
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return num;
	}
	String substitute(String line) {
		boolean ignore_line=false;
		for (String word: ignore) {
			if (line.contains(word)) {
				ignore_line=true;
				break;
			}
		}
		if (ignore_line) return null;									//	無視すべき語が入っている時

		Iterator<String> it=substitute.keySet().iterator();				//	特定の語の置き換え
		while(it.hasNext()) {
			String str=it.next();
			String rstr=substitute.get(str);
			line=line.replaceAll(str,rstr);
		}
		return line;
	}
	MenuData analyze_line(String[] strs, String selected, int num){
		MenuData Mitem=null;
		String keywords=strs[0].replaceAll("\b","");				//	異常な記号を除去
		String id=IDmap.get(keywords);								//	まず食品成分表と照合する
		if (id!=null){
			Nutritions N=nutrition_table.get(id);
			if (N!=null) {
				double quantity=get_quantity(strs);					//	strs[1]から値を取得する。ない場合は0を返す処理をget_quantityの中で行う
				String comment=get_comment(strs);
				String Mid=get_Mid(num++);
				Mitem=new MenuData(Mid,keywords,selected,N,quantity,comment);
			}
			else {
				System.out.println("   "+keywords+" "+id+"　食品成分データがない。idrefs.txtファイルの問題");
			}
		}
		else {
			Nutritions F=foodproducts.get(keywords);				//	加工食品のテーブルを検索する
			if (F!=null){											//	brandパラメータがあるとき一致を求める
				boolean stat=true;
				if (stat) {
					double quantity=get_quantity(strs, F.getWeight(), F.getWeight_f());
					String comment=get_comment(strs);
					String Mid=get_Mid(num++);
					Mitem=new MenuData(Mid,keywords,selected,F,quantity,comment);					
				}
			}
			else {													//	三大栄養素の直接入力とみてデータの解釈を試みる
				if (strs.length>1) {
					String[] dstr=strs[1].split("/");
					if (dstr.length>=5){							//	NutritionParserUtilを使うように直すべき
						double calorie=get_number(dstr[0]);			//	get_numberは親クラスに移動
						double carbohydrate=get_number(dstr[1]);
						double protein=get_number(dstr[2]);
						double fat=get_number(dstr[3]);
						double salt=get_number(dstr[4]);
						String comment=get_comment(strs);
						String Mid=get_Mid(num++);
						double fiber=0.0;
						if (dstr.length==6) fiber=get_number(dstr[5]); 
						Mitem=new MenuData(Mid,keywords,selected,standard_nutrition, comment,calorie,carbohydrate,protein,fat,salt,fiber);
					}
					else {
						if (output_error_f) System.out.println("   "+keywords+"　食品成分データがない (キーワードに該当する項目がない)");
					}
				}
				else {
					if (output_error_f) System.out.println("　　"+keywords+" 食品成分データがないか直接入力の栄養素データがない");
				}
			}
		}
		return Mitem;
	}
	void close() {
		try {
			in.close();			
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	String get_Mid(int num){
		return "000000"+new DecimalFormat("000").format(num++);
	}
}
