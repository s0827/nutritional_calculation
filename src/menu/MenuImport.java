package menu;

import java.util.regex.*;
import nutrition.*;

public class MenuImport {
	String fdatestring,ikeyword;
	MenuServing mserv;
	MenuImport(String instr,MenuDate md,String datestring){			//	import文字列の解析
		String[] strs=instr.split(" +");
		int ic=0;
		fdatestring="";
		String rdatestring=md.calculate_date(strs[0],datestring);	//	このファイルの日付を基準として料理データのある日付を取得
		if (rdatestring!=null) {
			fdatestring=rdatestring;
			ic++;
		}
		else {
			Pattern pat=Pattern.compile("[0-9]{6}");
			Matcher mat=pat.matcher(strs[0]);
			if (mat.find()){
				fdatestring=strs[0];
				ic++;
			}
			else {
				fdatestring=md.getDatestring();		//	指定ないときは当日
			}
		}
		ikeyword=strs[ic++];

		mserv=new MenuServing(instr);
	}
	double get_quantity(MenuServing M){
		return mserv.get_quantity(M);
	}
	String getFdatestring(){ return fdatestring; }
	String getIkeyword(){ return ikeyword; }
	MenuServing getMserv(){ return mserv; }
}
