package menu;

import java.io.*;
import java.util.*;

public class RecipeFileRules {			//	レシピデータ解釈のためのルールを保持する
	String title,author;
	LinkedHashMap<String, String> substitute;
	LinkedHashSet<String> ignore;
	RecipeFileRules(BufferedReader in){
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				String[] strs=line.split("[ \t]");
				if (strs[0].equals("title")) {
					title=strs[1];
				}
				else if (strs[0].equals("author")) {
					author=strs[1];
				}
				else if (strs[0].equals("substitute")) {
					substitute=get_map(in);
				} 
				else if (strs[0].equals("ignore")) {
					ignore=get_set(in);
					break;					//	このブロックを読んだらルールの読み込みは終了
				}
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	LinkedHashMap<String, String> get_map(BufferedReader in){
		LinkedHashMap<String, String> map=new LinkedHashMap<String, String>();
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				String[] strs=line.split("[ \t]+");
				if (strs[0].equals("end")) break;
				map.put(strs[1],strs[2]);
			}			
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return map;
	}
	LinkedHashSet<String> get_set(BufferedReader in){
		LinkedHashSet<String> set=new LinkedHashSet<String>();
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				String[] strs=line.split("[ \t]+");
				if (strs[0].equals("end")) break;
				set.add(strs[1]);
			}			
		}
		catch(Exception e) {
			System.err.println(e);
		}
		return set;
	}
	LinkedHashMap<String, String> getSubstitute() { return substitute; }
	LinkedHashSet<String> getIgnore() { return ignore; }

}
