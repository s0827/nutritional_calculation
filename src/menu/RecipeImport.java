package menu;

import nutrition.MenuServing;

public class RecipeImport {			//	レシピ項目を取り込み、料理した量に直す
	String ikeyword;
	MenuServing mserv;				//	わざわざ文字列をつなぎ直す。改良必要
	RecipeImport(String instr, String qstr) {
		ikeyword=instr;
		mserv = new MenuServing(instr+" "+qstr);
	}
	double get_quantity(MenuServing Mserv){
		return Mserv.get_quantity(mserv);
	}
	void update_serving(MenuServing Mserv) {
		Mserv.copy(mserv);			//	Mservの内容をmservにコピー
	}
	String getIkeyword(){ return ikeyword; }
	MenuServing getMserv(){ return mserv; }
}
