package menu;

import java.awt.*;

import nutrition.*;

public class MenuFrame extends Frame {
	int offset=20, margin=0;						//	メニューバーの高さのオフセットが必要？
	int framewidth=500,frameheight=500+offset;
	int panelwidth=500,panelheight=500;
	MenuPanel panel;
	
	public MenuFrame(){
		setSize(framewidth,frameheight);
		setVisible(true);
		setLayout(null);
		panel=new MenuPanel(panelwidth,panelheight);
		panel.setLocation(0,offset);
		add(panel);
	}
	public void put_data(MenuData data){ panel.setData(data); }
	public void put_standard_nutrition(Nutritions standard_nutrition){ panel.setStandard_nutrion(standard_nutrition); }
	public void put_title(String title){ panel.setTitle(title); }
	
	public void paint(Graphics gc){
		if (panel!=null) panel.repaint();
	}
}
