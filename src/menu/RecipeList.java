package menu;

import java.text.DecimalFormat;
import java.util.LinkedHashMap;

import nutrition.*;

public class RecipeList {							//	レシピ本の解析出力。Analyzerと共通のライブラリ（RecipeParser）を使って実装する。
	public static void main(String args[]) {		//	中間ファイルを作ることはやめる
		RecipeList rl=new RecipeList();
	}
	RecipeList(){
		ReaderData reader_data=new ReaderData();				//	メニュー読み込みのための各種データ

//		String book="makita-toshitsuofu-yasai.txt";
		String book="makita-zenburenchin.txt";
		
		RecipeParser rp=new RecipeParser(book, reader_data);
		System.out.println("メニュー           \tカロリー\tC\tP\tF\tfiber");
		while(true) {
			MenuData M=rp.get_next_record();
			if (M==null) break;
			M.summation();
			double serving=M.getMserv().getServing();									//	データは食数を想定
			M.mult(1.0/serving);
			System.out.print(fmt(M.getKeywords()));
			System.out.print("\t"+new DecimalFormat("0").format(M.getCalorie())+"kcal");
			System.out.print("\t"+new DecimalFormat("0.0").format(M.getCarbohydrate())+"g");
			System.out.print("\t"+new DecimalFormat("0.0").format(M.getProtein())+"g");
			System.out.print("\t"+new DecimalFormat("0.0").format(M.getFat())+"g");
			System.out.print("\t"+new DecimalFormat("0.0").format(M.getFiber())+"g");
			System.out.println();
		}
	}
	String fmt(String instr) {
		String outstr=instr;
		while(outstr.length()<15) outstr+=" ";
		return outstr;
	}

}
