package menu;

import java.text.DecimalFormat;
import java.util.*;

public class MenuDate {			//	日付情報の管理
	int year,month,day;
	String datestring,formatted_date;
	String directory;										//	ファイルパス名を取得するためのもの
	LinkedHashMap<String, String> datestrings;
	LinkedHashMap<String, Integer> date_refs;
	MenuDate(){
		Calendar calendar=Calendar.getInstance();			//	日付情報の取得。内部データ、ファイルの管理に用いるJava6の実装
		datestring=get_datestring(calendar);
		formatted_date=get_formatted_date(datestring);
	
		datestrings=get_file_dates();						//	当日を起点として前後の日を参照するためのマップを作成
		date_refs=get_date_refs();
	}
	String get_datestring(Calendar calendar){
		year=calendar.get(Calendar.YEAR)-2000;
		month=calendar.get(Calendar.MONTH)+1;
		day=calendar.get(Calendar.DAY_OF_MONTH);
		return new DecimalFormat("00").format(year)+new DecimalFormat("00").format(month)+new DecimalFormat("00").format(day);
	}
	String get_formatted_date(String str){
		return "20"+str.substring(0,2)+"年"+str.substring(2,4)+"月"+str.substring(4,6)+"日";		//	内部のデータは変えない
	}
	LinkedHashMap<String, String> get_file_dates(){
		LinkedHashMap<String, String> map=new LinkedHashMap<String, String>();
		Calendar calendar=Calendar.getInstance();												//	日付情報の取得。内部データ、ファイルの管理に用いるJava6の実装
		calendar.add(calendar.DATE,2);map.put("明後日",get_datestring(calendar));
		calendar.add(calendar.DATE,-1); map.put("明日",get_datestring(calendar));
		calendar.add(calendar.DATE,-1); map.put("今日",get_datestring(calendar));
		calendar.add(calendar.DATE,-1); map.put("昨日",get_datestring(calendar));
		calendar.add(calendar.DATE,-1); map.put("一昨日",get_datestring(calendar));
		return map;
	}
	LinkedHashMap<String, Integer> get_date_refs(){
		LinkedHashMap<String, Integer> map=new LinkedHashMap<String, Integer>();
		Calendar calendar=Calendar.getInstance();										//	日付情報の取得。内部データ、ファイルの管理に用いるJava6の実装
		map.put("今日",new Integer(0));
		calendar.add(calendar.DATE,-1); map.put("前日",new Integer(-1));
		calendar.add(calendar.DATE,-1); map.put("前々日",new Integer(-2));
		calendar.add(calendar.DATE,-1); map.put("前々々日",new Integer(-3));
		calendar.add(calendar.DATE,-1); map.put("前々々々日",new Integer(-4));
		return map;
	}
	String calculate_date(String str, String xdatestring){
		String fdatestring="";
		Integer diff=date_refs.get(str);
		if (diff==null) return null;
		
		int year=2000+Integer.parseInt(xdatestring.substring(0,2));			//	解析しているファイルの日付を起点として当日、前日...のように参照先ファイルの日付を見つける
		int month=Integer.parseInt(xdatestring.substring(2,4))-1;
		int day=Integer.parseInt(xdatestring.substring(4,6));
		Calendar xcalendar=Calendar.getInstance();
		xcalendar.set(year,month,day);
		
		int idiff=diff.intValue();
		xcalendar.add(xcalendar.DATE,idiff);
		
		return get_datestring(xcalendar);
	}
	String reformat_date(Integer I){													//	formatted_dateを与えられた日付から再取得する。内部のデータは変えない
		String str=new DecimalFormat("0").format(I.intValue());
		return get_formatted_date(str);	
	}
	String get_file(String str){ return datestrings.get(str); }	
	String get_default_menufile(){ return directory+datestring+".txt"; }

	void setDirectory(String directory){ this.directory=directory; }
	
	LinkedHashMap<String, String> getDatestrings(){ return datestrings; }
	String getDatestring(){ return datestring; }
	String getFormatted_date(){ return formatted_date; }
}
