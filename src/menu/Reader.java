package menu;

import java.text.DecimalFormat;
import java.util.*;
import java.util.regex.*;
import java.io.*;

import nutrition.FoodProductsXML;
import nutrition.MenuData;
import nutrition.Nutritions;
import nutrition.Unit2;

public class Reader {
	ReaderData reader_data;								//	これに以下の情報をまとめる
	LinkedHashMap<String, Nutritions> foodproducts;
	Nutritions standard_nutrition;
	LinkedHashMap<String, Nutritions> nutrition_table;
	LinkedHashMap<String, String> IDmap;
	Unit2 unit;
	
	LinkedHashMap<String, String> brands;
	String select_chars="#&@\\$";
	
	Pattern qpat=Pattern.compile("([0-9\\.\\-]+)g");
	Pattern npat=Pattern.compile("([0-9]+\\.{0,1}[0-9]*)");

	boolean output_error_f=true;
	String datestring;									//	YYMMDDの日付情報。
	
	Reader(ReaderData reader_data){						//	受け渡されたデータを使える形にする
		this.reader_data=reader_data;
		
		foodproducts=reader_data.getFoodproducts();
		standard_nutrition=reader_data.getStandard_nutrition();
		nutrition_table=reader_data.getNutrition_table();
		IDmap=reader_data.getIDmap();
		unit=reader_data.getUnit();
	}
	MenuData analyze_line(String line){
		String[] strs=split(line);
		return analyze_line(strs,null,"",0);				//	Analyzerから項目ごとに直接呼ばれる場合
	}
	MenuData parse(String[] strs, int num) {
		return analyze_line(strs,null,"",num);
	}
	MenuData analyze_line(String[] strs, LinkedHashMap<String,Nutritions> fp, String selected, int num){
		MenuData Mitem=null;
		String keywords=strs[0].replaceAll("\b","");				//	異常な記号を除去
		String id=IDmap.get(keywords);								//	まず食品成分表と照合する
		if (id!=null){
			Nutritions N=nutrition_table.get(id);
			if (N!=null) {
				double quantity=get_quantity(strs);					//	strs[1]から値を取得する。ない場合は0を返す処理をget_quantityの中で行う
				String comment=get_comment(strs);
				String Mid=get_Mid(num++);
				Mitem=new MenuData(Mid,keywords,selected,N,quantity,comment);
			}
			else {
				System.out.println("   "+keywords+" "+id+"　食品成分データがない。idrefs.txtファイルの問題");
			}
		}
		else {
			Nutritions F=fp.get(keywords);							//	加工食品のテーブルを検索する
			if (F!=null){											//	brandパラメータがあるとき一致を求める。これは今は冗長では？
				double quantity=get_quantity(strs, F.getWeight(), F.getWeight_f());
				String comment=get_comment(strs);
				String Mid=get_Mid(num++);
				Mitem=new MenuData(Mid,keywords,selected,F,quantity,comment);					
			}
			else {													//	三大栄養素の直接入力とみてデータの解釈を試みる
				if (strs.length>1) {
					String[] dstr=strs[1].split("/");
					if (dstr.length>=5){							//	NutritionParserUtilを使うように直すべき
						double calorie=get_number(dstr[0]);
						double carbohydrate=get_number(dstr[1]);
						double protein=get_number(dstr[2]);
						double fat=get_number(dstr[3]);
						double salt=get_number(dstr[4]);
						String comment=get_comment(strs);
						String Mid=get_Mid(num++);
						double fiber=0.0;
						if (dstr.length==6) fiber=get_number(dstr[5]); 
						Mitem=new MenuData(Mid,keywords,selected,standard_nutrition, comment,calorie,carbohydrate,protein,fat,salt,fiber);
					}
					else {
						if (output_error_f) System.out.println("   "+keywords+"　食品成分データがない (キーワードに該当する項目がない)");
					}
				}
				else {
					if (output_error_f) System.out.println("　　"+keywords+" 食品成分データがないか直接入力の栄養素データがない");
				}
			}
		}
		return Mitem;
	}
	
	double get_quantity(String[] strs, double weight, boolean weight_f) {	//	改良版、食品に単位となる重量が与えられている場合に対応する
		double quantity=0.0;
		if (strs.length>1) {
			quantity=unit.get_weight(strs,weight,weight_f);
		}
		else {
			quantity=weight;											//	数量が指定されていないときは1個である
		}
		return quantity;
	}
	double get_quantity(String[] strs){									//	食品の重量を得る
		double quantity=0.0;
		if (strs.length>1){												//	フィールドに値がある場合
			Matcher qmat=qpat.matcher(strs[1]);
			if (qmat.find()){
				quantity=Double.parseDouble(qmat.group(1));				//	これはunitの処理と重複では？ 重量の値を得る。食品成分表は廃棄率を考慮しているのでここで計算しなくてよいはず
			}
			else {
				quantity=unit.get_weight(strs[0],strs[1]);				//	単位の変換を行う。単位のデータベースを参照し、1本、1個などの表現を重量に直す
			}
		}
		return quantity;
	}
	double get_number(String str){
		double val=0.0;
		Matcher nmat=npat.matcher(str);
		if (nmat.find()){
			val=Double.parseDouble(nmat.group(1));
		}
		return val;
	}
	String get_comment(String[] strs){
		String comment="";					
		for (int i=2; i<strs.length; i++){
			if (i>2) comment+=" ";
			comment+=strs[i];
		}
		return comment;
	}
	String[] split(String instr){
		String[] strs=instr.split("[ \t]+");							//	語と語の間の余計なスペースは除去される
		LinkedList<String> list=new LinkedList<String>();
		for (int i=0; i<strs.length; i++){								//	行の頭の空レコードを除去する
			if (strs[i].length()>0) list.add(strs[i]);
		}
		String[] outstrs=new String[list.size()];
		int is=0;
		for (String s: list) outstrs[is++]=s;
		return outstrs;
	}
	String get_Mid(int num){
		return datestring+new DecimalFormat("000").format(num++);
	}
}
