package menu;

import java.util.*;
import java.io.*;

public class IndexReader {													//	ブランドのキーワードとファイル名を読み込む
	LinkedHashMap<String, String> map;
	public IndexReader(String brand_file){
		map=new LinkedHashMap<String, String>();
		try {
			BufferedReader in=new BufferedReader(new FileReader(brand_file));
			while(true) {
				 String line=in.readLine();
				 if (line==null) break;
				 
				 if (line.length()==0) continue;
				 if (line.substring(0,1).equals(":")) continue;			//	コロンでコメント行
				 String[] strs=line.split("[ \t]+");					//	スペースまたはタブ区切り
				 strs[0]=strs[0].replaceAll("\b","");					//	余計な\b記号を削除
				 map.put(strs[0],strs[1]);								//	コメントの内容は保持されない
			}
			in.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	public LinkedHashMap<String, String> getMap(){ return map; }
}
