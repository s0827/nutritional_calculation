package menu;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.*;

import nutrition.*;

public class Recipe {
	String brand_file="brands.txt";							//	外食のブランド名のデータ
	String foodproducts_xml="foodproducts.xml";				//	加工食品のデータ

	MenuDate menu_date;
	MenuReader menureader;									//	メニューファイル読み込みエンジン
	String select_chars="%";

	String filename="makita-zenburenchin.txt";
		
	public static void main(String args[]) {
		Recipe recipe=new Recipe();
	}
	Recipe(){
		initialize();
		convert();
		analyze();
	}
	void initialize() {
		menu_date=new MenuDate();														//	日付情報（ダミー）
		IndexReader aux_reader=new IndexReader(brand_file);
		LinkedHashMap<String, String> brands=aux_reader.getMap();
		
		ReaderData reader_data=new ReaderData();									//	食品の栄養情報はReaderDataにまとめられた
		
		menureader=new MenuReader(brands, null, menu_date, select_chars, reader_data);
	}
	void convert() {
		RecipeFileConverter converter=new RecipeFileConverter(filename);				//	レシピファイルの書式を汎用に変え、中間ファイルを生成
	}
	void analyze() {																	//	ここでメニューごとの集計が必要
		LinkedList<MenuData> list=menureader.read_file("recipe_temp.txt");
		LinkedList<MenuData> menus=new LinkedList<MenuData>();
		list.forEach(V ->{
			if (V.getMenuitems()!=null) {												//	メニュー項目ごとの栄養情報の集計
				V.summation();
				menus.add(V);
			}
		});
		System.out.println("メニュー           \tカロリー\tC\tP\tF\tfiber");
		menus.forEach(V -> {
			double serving=V.getMserv().getServing();									//	データは食数を想定
			V.mult(1.0/serving);
			System.out.print(fmt(V.getKeywords()));
			System.out.print("\t"+new DecimalFormat("0").format(V.getCalorie())+"kcal");
			System.out.print("\t"+new DecimalFormat("0.0").format(V.getCarbohydrate())+"g");
			System.out.print("\t"+new DecimalFormat("0.0").format(V.getProtein())+"g");
			System.out.print("\t"+new DecimalFormat("0.0").format(V.getFat())+"g");
			System.out.print("\t"+new DecimalFormat("0.0").format(V.getFiber())+"g");
			System.out.println();
		
//			V.output("%",2,0,1);
		});
	}
	String fmt(String instr) {
		String outstr=instr;
		while(outstr.length()<15) outstr+=" ";
		return outstr;
	}
}
