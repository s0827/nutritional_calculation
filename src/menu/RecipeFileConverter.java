package menu;

import java.util.*;
import java.util.regex.*;
import java.io.*;

public class RecipeFileConverter {					//	レシピファイルの中間ファイルの生成
	RecipeFileRules rules;
	String outfile="recipe_temp.txt";
	Pattern epat=Pattern.compile("\\}");

	RecipeFileConverter(String infile){
		try {
			BufferedReader in=new BufferedReader(new FileReader(infile));
			rules=new RecipeFileRules(in);
			create_temp_file(in);
			in.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void create_temp_file(BufferedReader in) {
		try {
			PrintWriter out=new PrintWriter(new BufferedWriter(new FileWriter(outfile)));
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				String[] strs=line.split("[ \t]");
				if (strs[0].equals("recipe")) process_recipe(in,out);		//	メニュー項目の処理
			}
			out.close();
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void process_recipe(BufferedReader in, PrintWriter out) {
		Pattern pat=Pattern.compile("(.*)\\{"); 
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				Matcher mat=pat.matcher(line);
				if (mat.find()) {
					String[] strs=mat.group(1).split("[ \t]+");
					out.println("%"+strs[0]+" "+strs[1]+" {");
					process_menu(in,out);
				}
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void process_menu(BufferedReader in, PrintWriter out) {
		Pattern ipat=Pattern.compile("[A-Z0-9] +\\{");
		Pattern rpat=Pattern.compile("レシピ");
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				Matcher emat=epat.matcher(line);
				if (emat.find()) {
					out.println(line);
					break;
				}
				Matcher imat=ipat.matcher(line);
				if (imat.find()) {
					process_block(true,in,out);
					continue;
				}
				Matcher rmat=rpat.matcher(line);
				if (rmat.find()) {
					process_block(false,in,out);
					continue;
				}
				line=process_line(line);		//	語の置き換え、コメントアウト処理
				out.println(line);
			}
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	void process_block(boolean stat, BufferedReader in, PrintWriter out) {
		try {
			while(true) {
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				Matcher emat=epat.matcher(line);
				if (emat.find()) {
					break;
				}
				else {
					if (stat) {
						line=process_line(line);
						line=truncate(line);		//	タブ1つ分短縮する
						out.println(line);
					}
				}
			}		
		}
		catch(Exception e) {
			System.err.println(e);
		}
	}
	String process_line(String instr) {
		LinkedHashMap<String, String> substitute=rules.getSubstitute();
		LinkedHashSet<String> ignore=rules.getIgnore();

		String outstr=instr;
		Iterator<String> it=substitute.keySet().iterator();
		while(it.hasNext()) {
			String keyword=it.next();
			String replacement=substitute.get(keyword);
//			if (outstr.contains(keyword)) {
				outstr=outstr.replace(keyword,replacement);
//			}
		}
		for (String keyword: ignore) {
			if (outstr.contains(keyword)) outstr=":"+outstr;
		}
		return outstr;
	}
	String truncate(String instr) {
		String outstr=instr.replace("\t\t","\t");
		return outstr;
	}
}
