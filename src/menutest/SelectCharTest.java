package menutest;

import java.io.*;
import java.util.regex.*;

public class SelectCharTest {
	public static void main(String args[]){
		SelectCharTest sct=new SelectCharTest();
	}
	SelectCharTest(){
		Pattern pat=Pattern.compile("([#%&@]*)");
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			while(true){
				String line=in.readLine();
				Matcher mat=pat.matcher(line);
				if (mat.find()){
					System.out.println("("+mat.group(1)+")");
				}
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
}
