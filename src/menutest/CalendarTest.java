package menutest;

import java.text.DecimalFormat;
import java.util.Calendar;

public class CalendarTest {
	public static void main(String args[]){
		CalendarTest ct=new CalendarTest();
	}
	CalendarTest(){
		Calendar calendar=Calendar.getInstance();			//	日付情報の取得。内部データ、ファイルの管理に用いるJava6の実装
		for (int i=0; i<10; i++){
			calendar.add(calendar.DATE,1);
			int year=calendar.get(Calendar.YEAR)-2000;
			int month=calendar.get(Calendar.MONTH)+1;
			int day=calendar.get(Calendar.DAY_OF_MONTH);
			String datestring=new DecimalFormat("00").format(year)+new DecimalFormat("00").format(month)+new DecimalFormat("00").format(day);
			String formatted_date="20"+new DecimalFormat("00").format(year)+"年"+new DecimalFormat("0").format(month)+"月"+new DecimalFormat("0").format(day)+"日";
			System.out.println(formatted_date);
		}
	}
}
