package menutest;

import java.io.*;
import java.util.regex.*;

public class RegularExpressionTest {
	public static void main(String args[]){
		RegularExpressionTest ret=new RegularExpressionTest();
	}
	RegularExpressionTest(){
		Pattern pat=Pattern.compile("^(玉|タマ|たま)(ネギ|ねぎ)$");		// 要素はカッコでくくり、始まりと終わりの指定で語が完結することを指定
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			while(true){
				System.out.print("> ");
				String line=in.readLine();
				Matcher mat=pat.matcher(line);
				if (mat.find()) System.out.println("matched");
			}			
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
}
