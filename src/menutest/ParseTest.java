package menutest;

import java.io.*;
import java.util.regex.*;
import java.util.*;

public class ParseTest {
	String select_chars="#%&@\\$";
	public static void main(String args[]){
		ParseTest pt=new ParseTest();
	}
	ParseTest(){
//		Pattern pat=Pattern.compile("(["+select_chars+"]+)(.+)( *[0-9]{0,6} ^)( *.+ ^)( *[0-9\\.]*).*;.*");
		Pattern pat=Pattern.compile("(["+select_chars+"]+)([^ ]+)( *([0-9]{0,6}))( *([^ ]+))( *([0-9\\.]*)).*;.*");
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			while(true){
				System.out.print("> ");
				String line=in.readLine();
				Matcher mat=pat.matcher(line);
				if (mat.find()){
					int groupcount=mat.groupCount();
					for (int i=0; i<=groupcount; i++){
						System.out.println(i+"\t"+mat.group(i));
					}
				}
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
}
