package pricer;

public class Shop {
	String name;
	int id;
	boolean tax_included=true;
	Shop(String name, int id, Boolean tax_included){
		this.name=name;
		this.id=id;
		this.tax_included=tax_included;
	}
	int getId(){ return id; }
	String getName(){ return name; }
	Boolean isTax_included(){ return tax_included; }
}
