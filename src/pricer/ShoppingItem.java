package pricer;

import java.util.*;
import java.util.regex.*;

public class ShoppingItem {					//	商品ごとの情報
	Shop shop;
	Calendar cal;							//	日付
	String datestring, shopstring;
	int id;
	String name;							//	商品情報
	double weight,price,tprice;				//	重量、価格、税込み価格
	double nprice;							//	100グラムあたり価格、税抜き
	double tax_rate=0.08;					//	消費税率
	
	ShoppingItem(String datestring, String shopstring, Shop shop){
		this.datestring=datestring;
		this.shopstring=shopstring;
		this.shop=shop;
		set_date();							//	日付の情報をカレンダー型変数にセット
	}
	ShoppingItem(ShoppingItem item){		//	コピーコンストラクタ
		this.datestring=item.datestring;
		this.shopstring=item.shopstring;
		this.shop=item.shop;
		set_date();
	}
	void set_date(){
		Pattern pat=Pattern.compile("([0-9]{4})年([0-9]{1,2})月([0-9]{1,2})日");
		cal=Calendar.getInstance();
		Matcher mat=pat.matcher(datestring);
		if (mat.find()){
			int year=Integer.parseInt(mat.group(1));
			int month=Integer.parseInt(mat.group(2))-1;
			int day=Integer.parseInt(mat.group(3));
			cal.set(year,month,day);
		}
		else {
			System.out.println("日付のフォーマットのエラー "+datestring);
			System.exit(0);
		}
	}
	void put_data(int id, String name, double weight, double price){
		this.id=id;
		this.name=name;
		this.weight=weight;
		if(shop.isTax_included()){
			this.tprice=price;
			this.price=price/(1.0+tax_rate);
		}
		else {
			this.price=price;
			this.tprice=price*(1.0+tax_rate);
		}
		nprice=price/weight*100.0;
	}
	Calendar getCal(){ return cal; }
	String getName(){ return name; }
	double getPrice(){ return price; }
	double getNprice(){ return nprice; }
	double getWeight(){ return weight; }
	String getDatestring(){ return datestring; }
	String getShopstring(){ return shopstring; }
}
