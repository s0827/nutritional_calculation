package pricer;

import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.*;

public class Pricer {
	String filename="shopping.txt";
	TreeMap<String, Shop> shops;
	public static void main(String args[]){
		Pricer pricer=new Pricer();
	}
	Pricer(){
		TreeMap<Integer, ShoppingItem> items=new TreeMap<Integer, ShoppingItem>();
		try {
			BufferedReader in=new BufferedReader(new FileReader(filename));
			in.readLine();
			read_shop_map(in);											//	店情報を登録
			int lastID=0;
			while(true){
				String line=in.readLine();
				if (line==null) break;
				if (line.length()==0) continue;
				ShoppingItem master_item=get_master_item(line);
				
				items.putAll(get_items(in,lastID,master_item));			//	シリアル番号の処理
				lastID=items.size();
			}
			in.close();
			
			disp_summary(items);										//	今日の買い物内容についての出力
			disp_interactive(items);									//	結果の解析、検索出力
			
			System.out.println(lastID+" items read");
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	void read_shop_map(BufferedReader in){
		String ikey="内税";		//	内税、外税の違いを識別するキーワード
		String xkey="外税";
		shops=new TreeMap<String, Shop>();
		try {
			int id=0;
			while(true){
				String line=in.readLine();
				StringTokenizer tokens=new StringTokenizer(line," \t");
				if (!tokens.hasMoreTokens()) break;
				String word=tokens.nextToken();
				boolean tax_included=false;
				while(tokens.hasMoreTokens()){
					if (word.equals(ikey)) tax_included=true;						//	内税対象の店
					Shop shop=new Shop(tokens.nextToken(),id,tax_included);
					shops.put(shop.getName(),shop);
					id++;					
				}
			}
/*			Iterator<String> it=shops.keySet().iterator();
			while(it.hasNext()){
				String name=it.next();
				Shop shop=map.get(name);
				System.out.println(shop.getId()+"\t"+name+"\t"+shop.isTax_included());
			}*/
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	ShoppingItem get_master_item(String line){
		StringTokenizer tokens=new StringTokenizer(line," \t");
		String datestring=tokens.nextToken();
		String shopstring=tokens.nextToken();
		Shop shop=shops.get(shopstring);
		if (shop==null) {
			System.out.println("店情報が登録されていない: "+shopstring);
			System.exit(0);
		}
		ShoppingItem master_item=new ShoppingItem(datestring, shopstring, shop);
		return master_item;
	}
	TreeMap<Integer, ShoppingItem> get_items(BufferedReader in, int lastID, ShoppingItem master_item){
		Pattern pat=Pattern.compile("([^ \t]+)[ \t]+([0-9]+)g[ \t]+([0-9]+)円");
		TreeMap<Integer, ShoppingItem> map=new TreeMap<Integer, ShoppingItem>();
		try {
			while(true){
				String line=in.readLine();
				if (line==null) break;
				Matcher mat=pat.matcher(line);
				if (!mat.find()) break;										//	空行で読み取り終了
				String name=mat.group(1);
				double weight=Double.parseDouble(mat.group(2));
				double price=Double.parseDouble(mat.group(3));
				ShoppingItem item=new ShoppingItem(master_item);			//	店情報をマスターからコピー
				item.put_data(lastID,name,weight,price);
				map.put(new Integer(lastID++),item);
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
		return map;
	}
	void disp_summary(TreeMap<Integer, ShoppingItem> items){				//	最近の買い物の内容を出力する
		TreeMap<Calendar, TreeSet<String>> map=new TreeMap<Calendar, TreeSet<String>>();
		Iterator<Integer> it=items.keySet().iterator();
		while(it.hasNext()){
			ShoppingItem I=items.get(it.next());
			Calendar cal=I.getCal();
			String name=I.getName();
			TreeSet<String> set=map.get(cal);
			if (set==null) {
				set=new TreeSet<String>();
				map.put(cal,set);
			}
			set.add(name);
		}	
/*		Iterator<Calendar> ic=map.keySet().iterator();						//	日毎のデータ
		while(ic.hasNext()) {
			Calendar cal=ic.next();
			TreeSet<String> set=map.get(cal);
			System.out.println(cal.get(Calendar.YEAR)+" "+cal.get(Calendar.MONTH)+" "+cal.get(Calendar.DATE)+" "+set);	
		}*/
		TreeSet<String> set=map.get(map.lastKey());
		System.out.println(get_datestring(map.lastKey())+"の買物");
		for (String s: set) {
			System.out.println(s);
			TreeMap<Double, ShoppingItem> results=search(items,s);
			display(results);
		}
	}
	String get_datestring(Calendar cal){
		int year=cal.get(Calendar.YEAR);
		int month=cal.get(Calendar.MONTH)+1;
		int day=cal.get(Calendar.DATE);
		String s=year+"年"+month+"月"+day+"日";
		return s;
	}
	void disp_interactive(TreeMap<Integer, ShoppingItem> items){			//	検索機能
		try {
			BufferedReader in=new BufferedReader(new InputStreamReader(System.in));
			System.out.println("品目名を入力");
			while(true){
				System.out.print("> ");
				String line=in.readLine();
				TreeMap<Double, ShoppingItem> results=search(items,line);
				display(results);
			}
		}
		catch(Exception e){
			System.err.println(e);
		}
	}
	TreeMap<Double, ShoppingItem> search(TreeMap<Integer, ShoppingItem> items, String line){
		TreeMap<Double, ShoppingItem> results=new TreeMap<Double, ShoppingItem>();
		
		Iterator<Integer> it=items.keySet().iterator();
		while(it.hasNext()){
			ShoppingItem item=items.get(it.next());
			if (line.equals(item.getName())) {
				results.put(new Double(item.getNprice()),item);
			}
		}
		return results;
	}
	void display(TreeMap<Double, ShoppingItem> results){
		Iterator<Double> it=results.keySet().iterator();
		while(it.hasNext()){
			ShoppingItem item=results.get(it.next());
			System.out.println(new DecimalFormat("0").format(item.getNprice())+"円"
					+"\t["+new DecimalFormat("0").format(item.getPrice())+"円/"
					+new DecimalFormat("0").format(item.getWeight())+"g]"
					+"\t"+item.getShopstring()
					+"\t("+item.getDatestring()+")"
					);
		}
		System.out.println("税抜き値段100gあたり");
	}
}
